export const environment = {
  production: true,
  apiBaseUrl: 'https://surgecloud.bluesurge.com/',
  googleApiKey: 'AIzaSyCbJoTbmaZ_k8tTQ0ZBazr2O7YP1vEPnm0',
  defaultImage: 'assets/images/org_default.svg',
  adal: {
    tenant: 'bsdevad.onmicrosoft.com',
    clientId: '8df10808-cc89-4ba8-b4e5-23e7dcdfc64b' // AD
    // clientId: '379f6d12-7fea-4e23-907e-355887fe136d' // AD B2C
  },
  socket: {
    // googleMap:'ws://localhost:3006'
    googleMap: 'ws://40.71.192.34:3006',
    alarm: 'ws://40.71.192.34:3001'
  }
};
