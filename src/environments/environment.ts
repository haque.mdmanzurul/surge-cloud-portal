// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiBaseUrl: 'https://surgecloud.azurewebsites.net/',
  googleApiKey: 'AIzaSyDz6SjR_p_mXWyV1l-hMinwloCpUKHHtCs',
  defaultImage: 'assets/images/org_default.svg',
  adal: {
    tenant: 'bsdevad.onmicrosoft.com',
    // tenant: 'bluesurgead.onmicrosoft.com',
    // clientId: '379f6d12-7fea-4e23-907e-355887fe136d' // AD B2C Staging
    // clientId: '9f860bb8-0593-4484-82cc-a0be72c214a8' // AD B2C Local
    clientId: 'e533e6cc-fe9b-491a-a6c4-2413ea5be932' // AD Local
    // clientId: '8df10808-cc89-4ba8-b4e5-23e7dcdfc64b' // AD Staging
 },
  socket: {
    // googleMap:'ws://localhost:3006'
    googleMap: 'ws://40.71.192.34:3006',
    alarm: 'ws://40.71.192.34:3000'
    // alarm: 'http://localhost:3000'
  }
};
