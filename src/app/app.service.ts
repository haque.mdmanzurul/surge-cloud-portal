import { Injectable } from '@angular/core';
import { Company, Features } from './app';
import { NotificationsService } from 'angular2-notifications';
import { environment } from '../environments/environment';
import { Subject } from 'rxjs/Subject';
import * as _ from 'underscore';
import { AdalService, HttpService } from './shared-module';
import { Observable } from 'rxjs/Observable';
import { AppStartService } from './app-start.service';


@Injectable()
export class AppService {

  private _companies: Company[] = [];
  private _activeCompany: Company;
  public activeCompanyInfo = new Subject<Company>();
  public availableFeatures: Features[] = [];
  public feature: Subject<any> = new Subject();
  public _user: any;
  public companyList = new Subject<Company[]>();
  constructor(private _notificationsService: NotificationsService,
    private adalService: AdalService, public httpService: HttpService,
    public appStartService: AppStartService) {
  }

  set companies(companies: Company[]) {
    this._companies = [];
    companies.forEach(company => {
      this._companies.push(new Company(company));
    });
    this.companyList.next(this._companies);
  }

  get companies(): Company[] {
    return this._companies;
  }

  getActiveCompany(): Company {
    return this._activeCompany;
  }

  updateCompany(data: Company, companyId: string) {
    let index = _.indexOf(this._companies, _.findWhere(this._companies, { companyId: companyId }));
    this._companies[index] = data;
    this.companyList.next(this._companies);
  }

  addCompany(data: Company){
    this._companies.push(data);
    this.companyList.next(this._companies);
  }

  getDefaultImage() {
    return 'assets/images/defaulticon.png';
  }

  showErrorMessage(title: string, message: string) {
    this._notificationsService.error(
      title,
      message,
      {
        timeOut: 5000,
        showProgressBar: true,
        pauseOnHover: true,
        clickToClose: false,
        maxLength: 100
      }
    );
  }

  showSuccessMessage(title: string, message: string) {
    this._notificationsService.success(
      title,
      message,
      {
        timeOut: 5000,
        showProgressBar: true,
        pauseOnHover: true,
        clickToClose: false,
        maxLength: 100
      }
    );
  }

  set activeCompany(activeCompany: Company) {
    this._activeCompany = activeCompany;
    this.activeCompanyInfo.next(activeCompany);
  }

  getUserId(): string {
    return this.adalService.userInfo.profile.oid;
  }

  getPermissions() {
    this.httpService.get(`users/api/roles/GetUserRoleData/${this.getUserId()}`)
      .map((res) => res.json())
      .subscribe(
      (data) => {
        if (data && data.Features) {
          data.Features.forEach((feature) => {
            const model = new Features(
              feature.CanAccess,
              feature.CanAdd,
              feature.CanDelete,
              feature.CanUpdate,
              feature.FeatureId,
              feature.RoleId
            )
            this.availableFeatures.push(model);
          });
          this.feature.next();
        }
      }
      );
  }

  getAccessibleSideNavigation(availableSidebar: any[]): any[] {
    let accessible = _.pluck(this.availableFeatures, 'featureId');
    let features = [];
    availableSidebar.forEach((sidebar, key) => {
      if (accessible.indexOf(sidebar.featureId) > -1) {
        features.push(sidebar);
      }
    });
    return features;
  }

  getAccessibleFeatures(availableFeatures: any[]): any[] {
    let accessible = _.pluck(this.availableFeatures, 'featureId');
    let features = [];
    availableFeatures.forEach((sidebar, key) => {
      if (accessible.indexOf(sidebar.featureId) > -1) {
        features.push(sidebar);
      }
    });
    return features;
  }

  getLandingPage(): string {
    return 'admin/feature';
  }

  isAccessible(url: string): boolean {
    let available = this.appStartService.features;
    let feature = null;
    if (url.startsWith('/')) {
      url = url.replace('/', '');
    }
    feature = _.findWhere(available, { Url: url });
    return this.checkFeatureAccessible(feature && feature.FeatureId ? feature.FeatureId : null);
  }

  checkFeatureAccessible(featureId: string = null) {
    let accessible = _.pluck(this.availableFeatures, 'featureId');
    if (accessible.indexOf(featureId) > -1) {
      return true;
    } else {
      return false;
    }
  }

  set user(data: any) {
    this._user = data;
  }

  getCompanyNameByUser() {
    return this._user && this._user.CompanyName ? this._user.CompanyName : null;
  }

}
