import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AdalService } from './shared-module';

@Injectable()
export class AuthenticationGuard implements CanActivate {
  constructor(private router: Router, private adalService: AdalService) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'redirectUrl': next.url }
    };
    if (this.adalService.userInfo) {
      return true;
    } else {
      this.router.navigate(['/login'], navigationExtras);
    }
  }
}
