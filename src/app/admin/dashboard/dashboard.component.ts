import { Component } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { AppService } from '../../app.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  constructor(public appService:AppService, private route: ActivatedRoute) { 
  }
}
