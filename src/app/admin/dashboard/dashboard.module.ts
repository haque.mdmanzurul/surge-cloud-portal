import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { MaterialModule } from '@angular/material';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';
import { GoogleMapComponent } from './google-map/google-map.component';
import { RouterModule } from '@angular/router';
// import { AgmCoreModule } from 'angular2-google-maps/core';
import { AgmCoreModule } from '@agm/core';

import { FeatureComponent } from './feature/feature.component';
import { SharedModule } from '../../shared-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AgmCoreModule,
    // MaterialModule,
    SharedModule
  ],
  declarations: [
    DashboardComponent,
    GoogleMapComponent,
    FeatureComponent
  ],
  exports: [
    DashboardComponent,
    GoogleMapComponent,
    FeatureComponent
  ],
  providers: [DashboardService]
})
export class DashboardModule { }
