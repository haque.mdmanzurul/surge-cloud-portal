import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

// import { DashboardService } from '../dashboard.service';
import { INavigation } from '../../../app';
import { AppService } from '../../../app.service';
import { AppStartService } from '../../../';
import * as _ from 'underscore';

@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.scss']
})
export class FeatureComponent implements OnInit {

  private features: INavigation[] = [];
  private cardColors: string[] = [
    'light-green',
    'light-sea-blue',
    'blue',
    'grey',
    'light-red',
    'violet',
    'light-yellow',
    'sea-blue',
    'green',
    // 'yellow',
  ];
  private svgIcons: string[] = [
    // 'icon_AdvancedAanalytics.svg#Vector_Smart_Object_copy',
    'LandingPageIcons/BasicAnalytics',
    'LandingPageIcons/GeofenceTracking',
    'LandingPageIcons/Events&Alarms',
    'LandingPageIcons/ConnectedDiognostics',
    'LandingPageIcons/RoutingDispatch',
    'LandingPageIcons/Reports',
    'LandingPageIcons/ConnectedDiognostics',
    'LandingPageIcons/ConnectedCalibration',
    'LandingPageIcons/SafetyCompliance',
    'LandingPageIcons/ServiceManagement',

  ];
  // over:boolean[];

  public currentIcon: string;
  public showError: Boolean = false;

  constructor(public appStartService: AppStartService, private route: ActivatedRoute,
    private router: Router, private sanitizer: DomSanitizer, public appService: AppService) {

  }

  ngOnInit() {
    let i = 0, tempData = _.sortBy(this.appStartService.feature, 'sortOrder');
    let availableFeatures = [];
    for (let feature of tempData) {
      feature.imageIcon = this.sanitizer.bypassSecurityTrustResourceUrl(`${feature.imageIcon}${this.svgIcons[i]}_01.svg#Vector_Smart_Object`);
      feature.imageHoverIcon = this.sanitizer.bypassSecurityTrustResourceUrl(`${feature.imageHoverIcon}${this.svgIcons[i]}_02.svg#Vector_Smart_Object`);
      feature.color = this.cardColors[i++];
      feature.enabled = false; // hover
      availableFeatures.push(feature);
    }
    this.features = this.appService.getAccessibleFeatures(availableFeatures);
    this.appService.feature.subscribe(
      (res) => {
        this.features = this.appService.getAccessibleFeatures(availableFeatures);
        this.showError = true;
      });
  }

  goto(feature: INavigation) {
    this.router.navigate([feature.url]);
  }

  
}
