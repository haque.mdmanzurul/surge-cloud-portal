import { Injectable } from '@angular/core';
import { WebsocketService } from '../../../shared-module';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../../../environments/environment';


@Injectable()
export class GoogleMapService {
  public markers: Subject<any> = new Subject<any>();

  constructor(public wsService: WebsocketService) {

    this.markers = <Subject<any>>this.wsService
      .connect(environment.socket.googleMap)
      .map((response: MessageEvent): any => {
        console.log(response.data);
        return JSON.parse(response.data);
      });
  }




}
