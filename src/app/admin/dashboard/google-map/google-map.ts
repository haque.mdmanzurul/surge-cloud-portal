import * as _ from 'underscore';

export class Marker {
    public latitude: number;
    public longitude: number;
    public status: number;
    public icon: any;
    public assetID: string;
    public gw_SNO: string;
    public type: string;
    public id: string;
    public timestamp?: Date;
    constructor(public data: any) {
        let keys = Object.keys(data);
        let found = _.findWhere(data[keys[0]][0].param, { "name": "Location" });
        let latlng = found.val.split(',');
        this.latitude = Number(latlng[0]);
        this.longitude = Number(latlng[1]);
        this.status = found.sts || 'Empty status';
        this.icon = data.icon || '';
        this.type = keys[0].toLowerCase();
        this.assetID = this.data[keys[0]][0].assetID;
        this.gw_SNO = this.data[keys[0]][0].gw_SNO;
        this.id = `${this.assetID}${this.gw_SNO}`;
        this.timestamp = found.TS;

    }
}


