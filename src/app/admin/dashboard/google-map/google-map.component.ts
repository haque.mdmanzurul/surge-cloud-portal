import { Component, OnInit } from '@angular/core';
import { Marker } from './google-map';
import { GoogleMapService } from './google-map.service';
import * as _ from 'underscore';

declare const google: any;
@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss'],
  providers: [GoogleMapService]
})
export class GoogleMapComponent implements OnInit {
  public lat: number = 51.678418;
  public lng: number = 7.809007;
  public markers: Marker[] = [];
  public availableMarkers: Marker[] = []; 

  constructor(private mapService: GoogleMapService) {
    this.mapService.markers.subscribe(
      (data) => {
          let markerData = new Marker(data);
          this.markers = _.without(this.markers,  _.findWhere(this.markers, { id:`${markerData.assetID}${markerData.gw_SNO}`}));
          markerData.icon = this.getIcon(markerData.type);
          if(this.availableMarkers.length > 0){
            this.availableMarkers =  [ markerData,...this.availableMarkers];
          }else{
            this.availableMarkers.push(markerData);
          }
          
          this.markers.push(markerData);
      }
    )
  }

  ngOnInit() {

  }

  getIcon(type: string) {
    let concernIcon = new google.maps.MarkerImage("assets/images/sprite_mapicon.png", new google.maps.Size(38, 36), new google.maps.Point(4, 2));
    let inactiveIcon = new google.maps.MarkerImage("assets/images/sprite_mapicon.png", new google.maps.Size(38, 36), new google.maps.Point(4, 46));
    let icon = new google.maps.MarkerImage("assets/images/sprite_mapicon.png", new google.maps.Size(38, 36), new google.maps.Point(4, 90));
    return type == 'alarm' ? concernIcon : icon;
  }

}
