import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent, CompanyResolverService, AccessGuard } from '../app-common';
import { DashboardComponent, GoogleMapComponent, FeatureComponent } from './dashboard';
import { AuthenticationGuard } from '../authentication.guard';
import { CustomerOnboardingComponent } from './customer-onboarding';
import { TenantManagementComponent } from './tenant-management/tenant-management.component';
import { SubscriptionManagementComponent } from './subscription-management/subscription-management.component';
import { AssetOnboardingComponent } from './asset-onboarding';

import { DeviceManagementComponent } from './device-management/device-management.component';

import { GeoFencingComponent } from '../operation/geo-fencing';
import { AssetDashboardComponent } from '../operation/asset-dashboard';
import { AlarmConsoleComponent } from '../operation/alarm-console';


const routes: Routes = [
  {
    path: ':type',
    component: HomeComponent,
    resolve: {
      company: CompanyResolverService
    },
    canActivate: [AuthenticationGuard],
    children: [
      {
        path: '',
        component: DashboardComponent,
        children: [
          {
            path: 'feature',
            component: FeatureComponent
          },
          {
            path: 'map',
            component: GoogleMapComponent
          },
          {
            path: 'customer-onboarding',
            component: CustomerOnboardingComponent,
            canActivate: [AccessGuard]
          },
          {
            path: 'asset-onboarding',
            component: AssetOnboardingComponent,
            canActivate: [AccessGuard]
          }
        ],
      },
      {
        path: 'tenant-management',
        component: TenantManagementComponent,
        canActivate: [AccessGuard]
      },
      {
        path: 'subscription-management',
        component: SubscriptionManagementComponent,
        canActivate: [AccessGuard]
      },
      {
        path: 'device-management',
        component: DeviceManagementComponent,
        canActivate: [AccessGuard]
      },
      {
        path: 'geo-fencing',
        component: GeoFencingComponent,
        canActivate: [AccessGuard]
      },
      {
        path: 'asset-dashboard/:assetId',
        component: AssetDashboardComponent,
        canActivate: [AccessGuard]
      },
      {
        path: 'asset-dashboard',
        component: AssetDashboardComponent,
        canActivate: [AccessGuard]
      },
      {
        path: 'alarm',
        component: AlarmConsoleComponent,
        canActivate: [AccessGuard]
      },
      {
        path: 'alarm/:assetId/:gwSNO',
        component: AlarmConsoleComponent,
        canActivate: [AccessGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
