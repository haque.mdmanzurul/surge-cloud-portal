import { Component, OnInit, ViewChild } from '@angular/core';
import { IWizardItem } from '../../shared-module';
import { AssetOnboardingService } from '../asset-onboarding';



@Component({
  selector: 'app-tenant-management',
  templateUrl: './tenant-management.component.html',
  styleUrls: ['./tenant-management.component.scss']
})
export class TenantManagementComponent implements OnInit {
	public wizardItems: IWizardItem[] = [
    {
      name: 'Organization Overview',
      icon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateOrg.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateOrgWhite.svg#Vector_Smart_Object',
      success: false,
      active: true,
      statusIcon: ''
    },
    {
      name: 'Facility Overview',
      icon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Facility.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Facility_White.svg#Vector_Smart_Object',
      success: false,
      active: false,
      statusIcon: ''
    },
    {
      name: 'Asset Overview',
      icon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Asset.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Asset_White.svg#Vector_Smart_Object',
      success: false,
      active: false,
      statusIcon: ''
    }
  ];
   currentIndex = 0;
  constructor(public assetOnboardingService:AssetOnboardingService) { }

  ngOnInit() {
    this.assetOnboardingService.onboardingDataForEdit = { index: null, asset: null };    
  }

  beforeChange($event){
  	// console.log($event);
  	switch ($event.nextId) {
      case 'organization_overview':
      		this.wizardItems[this.currentIndex].active = false;
        	this.wizardItems[0].active = true;
        	this.currentIndex = 0;
        break;
      case 'facility_overview':
        	this.wizardItems[this.currentIndex].active = false;
        	this.wizardItems[1].active = true;
        	this.currentIndex = 1;
        break;
      case 'asset_overview':
        this.wizardItems[this.currentIndex].active = false;
        	this.wizardItems[2].active = true;
        	this.currentIndex = 2;

        break;
    }
  }

}
