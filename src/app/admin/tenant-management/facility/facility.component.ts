import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FacilityService } from './facility.service';
import { Facility } from './facility';
import { AppService } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { FacilityTableComponent } from './';
import { FacilityImageComponent } from './';
import { FacilityDirective } from './facility.directive';
import { FacilityItem } from './facility-item';
import { IWizardItem } from '../../../shared-module';
import { AssetOnboardingService }  from '../../asset-onboarding';
import * as _ from 'underscore';
import { PieChartComponent, SingleChartData } from '../../../core';

@Component({
  selector: 'app-facility',
  templateUrl: './facility.component.html',
  styleUrls: ['./facility.component.scss']
})

export class FacilityComponent implements OnInit {

  facility: Facility[] = [];

  ArrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();

  facilityCount: { active: number, inActive: number } = { active: 0, inActive: 0 };

  facilityItem: FacilityItem[] = [
    new FacilityItem(FacilityTableComponent, {}),
    new FacilityItem(FacilityImageComponent, {})];

  currentIndex = 0;

  public wizardItems: IWizardItem[] = [
    {
      name: 'Facility Table',
      icon: 'glyphicon-th-list',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    },
    {
      name: 'Facility Image',
      icon: 'glyphicon-th',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    },
    {
      name: 'Facility Add',
      icon: 'glyphicon-plus',
      disabled: false,
      index: 2,
      success: false,
      error: false,
      active: false
    }
  ];

  @ViewChild(FacilityDirective) facilityHost: FacilityDirective;
  filterObject : any = null;
  private lastUpdated : Date[] = [];
  public chartData: SingleChartData[] = [];
  filterStatus: boolean = this.filterObject === null ? false : true;
  constructor(private facilityService: FacilityService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private appService: AppService,
    private router: Router,
    private assetOnboardingService : AssetOnboardingService) {
  }

  ngOnInit() {
    this.getFacility(this.appService.getActiveCompany().companyId);

    this.appService.activeCompanyInfo.subscribe((company)=>{
      if(company){
        this.getFacility(company.companyId);
      }
    })
  }

  getFacility(companyId : string) : void {
    this.facilityService.getFacility(companyId).subscribe(
      (data) => {
        this.facility = [];
        data.forEach(fac =>{
          fac.address = fac.address ? fac.address : {};
          this.facility.push(new Facility(fac));
        })
        this.chartData = [];
        if(this.facility.length > 0){
          // let dates: Date[] = [];
          // dates = _.map(this.facility, function(i) { return i.lastUpdatedDate });
          // this.lastUpdated = _.sortBy(dates, function(i) { return i });
          let activeCount = (Math.ceil((this.ArrayFilterPipe.transform(this.facility, { status: 1 }).length / this.facility.length) * 100))
          + (Math.ceil((this.ArrayFilterPipe.transform(this.facility, { status: true }).length / this.facility.length) * 100));
          let inactiveCount = (Math.floor((this.ArrayFilterPipe.transform(this.facility, { status: 0 }).length / this.facility.length) * 100))+
          (Math.floor((this.ArrayFilterPipe.transform(this.facility, { status: false }).length / this.facility.length) * 100));
        // this.facilityCount = {
        //   active: activeCount,
        //   inActive: inactiveCount
        // };
        
        this.chartData.push({ name: 'Active Facilities', value: activeCount },
        { name: 'Inactive Facilities', value: inactiveCount });
        }
        this.loadComponent();
      }
    );
  }

  loadComponent() {
    const boardingItem = this.facilityItem[this.currentIndex];
    boardingItem.data = {facilityList : this.facility,filterObject:this.filterObject};

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.facilityHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ data: any }>componentRef.instance).data = boardingItem.data;

  }

  changeComponent(index) {
    console.log(index);
    if (index <= 1) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    }
    else if(index == 2){
      // this.assetOnboardingService.set
      this.assetOnboardingService.onboardingDataForEdit = { index: 1, facility: null};
      this.router.navigate(['/admin/asset-onboarding']);
    }

  }
  filterInfo(data){
    
    if (data) {
      let status: number = data.name === 'Active Facilities' ? 1 : 0;
      this.filterObject = { status: status };
    } else {
      this.filterObject = null;
    }
    this.filterStatus = this.filterObject === null ? false : true;
    this.loadComponent();
  }

}
