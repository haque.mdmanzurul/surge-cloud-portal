import { Component, OnInit, Input } from '@angular/core';
import { Facility } from '../facility';
import { TenantManagementService } from '../../tenant-management.service';
import { Router } from '@angular/router';
import { AssetOnboardingService, AssetOnboardingStatus } from '../../../asset-onboarding';
import { ArrayFilterPipe } from '../../../../shared-module';

@Component({
  selector: 'app-facility-table',
  templateUrl: './facility-table.component.html',
  styleUrls: ['./facility-table.component.scss']
})
export class FacilityTableComponent implements OnInit {

  @Input() data :{ facilityList: Facility[], filterObject: any};

  private paginationOrgInfo = {
    facilitysortOrder: 'asc', facilitysortBy: 'name',
    numberRowPerPage: 5
  }
  private statusSvg = ["assets/icons/Management/TenantStatus_inactive.svg#TenantStatus_inactive",
  "assets/icons/Management/TenantStatus_active.svg#TenantStatus_active"];
  facility : Facility[];
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  constructor(private tenantManagementService: TenantManagementService, private router:Router,
  private onboardingService:AssetOnboardingService) {
    this.paginationOrgInfo.numberRowPerPage = this.tenantManagementService.numberOfRecordPerPage;
  }

  ngOnInit() {
    if (this.data.filterObject && this.data.facilityList.length >0) {
        let filter;
          if (typeof this.data.facilityList[0].status === "number") {
            filter = this.data.filterObject;
          }else{
            filter = {status : this.data.filterObject.status === 0 ? false : true };
          }
          this.facility = this.arrayFilterPipe.transform(this.data.facilityList,filter);
    }
    else {
      this.facility = this.data.facilityList;
    }

  }

  editFacility(facility: Facility) {
    this.onboardingService.onboardingDataForEdit = { index: 1, facility: facility};
    this.router.navigate(['admin/asset-onboarding']);
  }
}
