import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared-module';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FacilityService {

  constructor(public httpService: HttpService) { }

  getFacility(data?: string): Observable<any> {

    return this.httpService.get(`facility/api/facility/get/${data}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }
}
