import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FacilityComponent } from './facility.component';
import { FacilityTableComponent } from './facility-table/facility-table.component';
import { FacilityImageComponent } from './facility-image/facility-image.component';
import { FacilityChartComponent } from './facility-chart/facility-chart.component';
import { FacilityDirective } from './facility.directive';
import { FacilityService } from './facility.service';
import { DataTableModule } from 'angular2-datatable';
import { CoreModule } from '../../../core';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    CoreModule
  ],
  declarations: [FacilityComponent,
   FacilityTableComponent,
   FacilityImageComponent,
   FacilityChartComponent,
   FacilityDirective
   ],
   exports: [FacilityComponent,
   FacilityTableComponent,
   FacilityImageComponent,
   FacilityChartComponent
   ],
   providers: [
   FacilityService
   ],
   entryComponents: [
   FacilityTableComponent,
   FacilityImageComponent
   ],
})

export class FacilityModule { }
