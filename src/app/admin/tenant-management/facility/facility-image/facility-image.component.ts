import { Component, OnInit, Input } from '@angular/core';
import { Facility } from '../facility';
import { Router } from '@angular/router';
import { AssetOnboardingService, AssetOnboardingStatus } from '../../../asset-onboarding';
import { ArrayFilterPipe } from '../../../../shared-module';
@Component({
  selector: 'app-facility-image',
  templateUrl: './facility-image.component.html',
  styleUrls: ['./facility-image.component.scss']
})
export class FacilityImageComponent implements OnInit {

  @Input() data: { facilityList: Facility[], filterObject: any };

  facility: Facility[];
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  constructor(private router: Router,
    private onboardingService: AssetOnboardingService) { }

  ngOnInit() {
    if (this.data.filterObject && this.data.facilityList.length > 0) {
      let filter;
      if (typeof this.data.facilityList[0].status === "number") {
        filter = this.data.filterObject;
      } else {
        filter = { status: this.data.filterObject.status === 0 ? false : true };
      }
      this.facility = this.arrayFilterPipe.transform(this.data.facilityList, filter);
    }
    else {
      this.facility = this.data.facilityList;
    }
  }

  editFacility(facility: Facility) {
    this.onboardingService.onboardingDataForEdit = { index: 1, facility: facility };
    this.router.navigate(['admin/asset-onboarding']);
  }

}
