import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityImageComponent } from './facility-image.component';

describe('FacilityImageComponent', () => {
  let component: FacilityImageComponent;
  let fixture: ComponentFixture<FacilityImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
