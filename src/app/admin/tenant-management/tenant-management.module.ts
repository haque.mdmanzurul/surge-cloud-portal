import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { FormsModule } from '@angular/forms';
import { TenantManagementComponent } from './tenant-management.component';
import { TenantManagementService } from './tenant-management.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { OrganizationModule, OrganizationComponent } from './organization';
import { FacilityModule, FacilityComponent } from './facility';
import { AssetModule, AssetComponent } from './asset';

@NgModule({
  imports: [
    CommonModule,
    SimpleNotificationsModule,
    FormsModule,
    NgbModule,
    OrganizationModule,
    FacilityModule,
    AssetModule
  ],
  declarations: [
    TenantManagementComponent,
  ],
  exports: [
    TenantManagementComponent
  ],
  providers: [
    TenantManagementService
  ],
  entryComponents : [
    OrganizationComponent,
    FacilityComponent,
    AssetComponent
  ]
})

export class TenantManagementModule { }
