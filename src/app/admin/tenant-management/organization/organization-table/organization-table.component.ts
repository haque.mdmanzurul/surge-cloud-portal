import { Component, OnInit, Input } from '@angular/core';
import { Company } from '../../../../../app';
import { TenantManagementService } from '../../tenant-management.service';
import { Organization } from '../../../../app-common';
import { CustomerOnboardingService } from '../../../customer-onboarding';
import { Router } from '@angular/router';
import { ArrayFilterPipe } from '../../../../shared-module';
@Component({
  selector: 'app-organization-table',
  templateUrl: './organization-table.component.html',
  styleUrls: ['./organization-table.component.scss']
})
export class OrganizationTableComponent implements OnInit {

  @Input() data: { organizationList: Company[], filterObject: any };
  private paginationOrgInfo = {
    organizationsortOrder: 'asc', organizationsortBy: 'name',
    numberRowPerPage: 5
  };
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  private organization: Company[];
  private statusSvg = ["assets/icons/Management/TenantStatus_inactive.svg#TenantStatus_inactive",
    "assets/icons/Management/TenantStatus_active.svg#TenantStatus_active"];
  
  constructor(private tenantManagementService: TenantManagementService,
    private customerOnboardingService: CustomerOnboardingService, private router: Router) {
    this.paginationOrgInfo.numberRowPerPage = this.tenantManagementService.numberOfRecordPerPage;
  }

  ngOnInit() {
    // console.log(this.data.organizationList);
    if (this.data.filterObject && this.data.organizationList.length >0) {
        let filter;
          if (typeof this.data.organizationList[0].status === "number") {
            filter = this.data.filterObject;
          }else{
            filter = {status : this.data.filterObject.status === 0 ? false : true };
          }
          this.organization = this.arrayFilterPipe.transform(this.data.organizationList,filter);
    }
    else {
      this.organization = this.data.organizationList;
    }
    
  }


  editOrganization(organization: any) {
    this.customerOnboardingService.onboardEdit = { organizationId: organization.companyId, userId: null, for: 1 };
    this.router.navigate(['/admin/customer-onboarding']);
  }
}
