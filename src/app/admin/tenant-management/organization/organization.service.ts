import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared-module';
import { Observable } from 'rxjs/Observable';
import { Company } from '../../../app';

@Injectable()
export class OrganizationService {

  constructor(public httpService: HttpService) { }

  getOrganization(): Observable<any> {
    return this.httpService.get('company/api/company/get')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }
}
