import { Type } from '@angular/core';

export class OrganizationItem {
    constructor(public component: Type<any>, public data: any) { }
};
