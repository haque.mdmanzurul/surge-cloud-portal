import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { OrganizationService } from './organization.service';
import { Company } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { OrganizationTableComponent } from './organization-table/organization-table.component';
import { OrganizationImageComponent } from './organization-image/organization-image.component';
import { OrganizationDirective } from './organization.directive';
import { OrganizationItem } from './organization-item';
import { IWizardItem } from '../../../shared-module';
import { AppService } from '../../../app.service';
import { Router } from '@angular/router';
import { CustomerOnboardingService } from '../../customer-onboarding';
import * as _ from 'underscore';
import { PieChartComponent, SingleChartData } from '../../../core';
@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {

  organizationList: Company[] = [];

  ArrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();

  userCount: { activeUser: number, inActiveUser: number } = { activeUser: 0, inActiveUser: 0 };

  organizationItem: OrganizationItem[] = [
    new OrganizationItem(OrganizationTableComponent, {}),
    new OrganizationItem(OrganizationImageComponent, {})];

  currentIndex = 0;
  public wizardItems: IWizardItem[] = [
    {
      name: 'Organization Table',
      icon: 'glyphicon-th-list',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    },
    {
      name: 'Organization Image',
      icon: 'glyphicon-th',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    },
    {
      name: 'Organization Add',
      icon: 'glyphicon-plus',
      disabled: false,
      index: 2,
      success: false,
      error: false,
      active: true
    }
  ];
  filterObject = null;
  @ViewChild(OrganizationDirective) organizationHost: OrganizationDirective;
  chartData: SingleChartData[] = [];
  filterStatus: boolean = this.filterObject === null ? false : true;
  lastUpdated: Date[] = [];
  pieChartInfo : any = {valueAppend:'%'};
  constructor(private organizationService: OrganizationService,
    private componentFactoryResolver: ComponentFactoryResolver, public customerOnboardingService: CustomerOnboardingService,
    private appService: AppService,
    private router: Router) {
    this.customerOnboardingService.onboardEdit = { organizationId: null, userId: null, for: null };
  }

  ngOnInit() {

    this.getOrganizationList();

  }

  getOrganizationList() {

    this.organizationList = this.appService.companies;
    this.chartData = [];  
    if (this.organizationList.length > 0) {
      
      let dates: Date[] = [];
      dates = _.map(this.organizationList, function(i) { return i.lastUpdatedDate });
      this.lastUpdated = _.sortBy(dates, function(i) { return i });
      
      let activeCount = (Math.ceil((this.ArrayFilterPipe.transform(this.organizationList, { status: 1 }).length /
        this.organizationList.length) * 100)) + (Math.ceil((this.ArrayFilterPipe.transform(this.organizationList, { status: true }).length /
          this.organizationList.length) * 100));
      let inactiveCount = (Math.floor((this.ArrayFilterPipe.transform(this.organizationList, { status: 0 }).length /
        this.organizationList.length) * 100)) + (Math.floor((this.ArrayFilterPipe.transform(this.organizationList, { status: false }).length /
          this.organizationList.length) * 100))
      
      this.chartData.push({ name: 'Active Organizations', value: activeCount },
        { name: 'Inactive Organizations', value: inactiveCount });
    }
    this.loadComponent();

  }
  loadComponent() {

    const boardingItem = this.organizationItem[this.currentIndex];
    boardingItem.data = { organizationList: this.organizationList, filterObject: this.filterObject };

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.organizationHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ data: any }>componentRef.instance).data = boardingItem.data;
    // const componentRef = viewContainerRef.createComponent(componentFactory);
    // (<{ facility: any }>componentRef.instance).facility = boardingItem.facility;
  }
  changeComponent(index) {

    if (index <= 1) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    }
    else if (index == 2) {
      this.customerOnboardingService.onboardEdit = { organizationId: null, userId: null, for: null, roleGroup: null };
      this.router.navigate(['/admin/customer-onboarding']);
    }

  }

  filterInfo(data: SingleChartData) {
    if (data) {
      let status: number = data.name === 'Active Organizations' ? 1 : 0;
      this.filterObject = { status: status };
    } else {
      this.filterObject = null;
    }
    this.filterStatus = this.filterObject === null ? false : true;
    this.loadComponent();
  }

  

}
