import { Component, OnInit, Input, Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-organization-chart',
  templateUrl: './organization-chart.component.html',
  styleUrls: ['./organization-chart.component.scss']
})
export class OrganizationChartComponent implements OnInit {

  @Input() userCount : {activeUser: number,inActiveUser : number};

  @Output() clickedChartInfo = new EventEmitter<number>();

  activeStatus : {active:boolean, inActive:boolean}= {active : false, inActive : false};
  constructor() { }

  ngOnInit() {
  }
  filterData(status:number){
  	// console.log(status);
    this.activeStatus = status ===1 ? {active : true, inActive : false} : {active : false, inActive : true};
    this.clickedChartInfo.next(status);
  }

}
