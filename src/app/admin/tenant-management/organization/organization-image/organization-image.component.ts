import { Component, OnInit, Input } from '@angular/core';
import { Company } from '../../../../../app';
import { AppService } from '../../../../app.service';
import { CustomerOnboardingService } from '../../../customer-onboarding';
import { Router } from '@angular/router';
import { ArrayFilterPipe } from '../../../../shared-module';
@Component({
  selector: 'app-organization-image',
  templateUrl: './organization-image.component.html',
  styleUrls: ['./organization-image.component.scss']
})
export class OrganizationImageComponent implements OnInit {

  @Input() data: { organizationList: Company[], filterObject: any };

  private organization: Company[] = [];
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  private statusSvg = ["assets/icons/Management/TenantStatus_inactive.svg#TenantStatus_inactive",
    "assets/icons/Management/TenantStatus_active.svg#TenantStatus_active"];
  
  constructor(private appService: AppService, private customerOnboardingService: CustomerOnboardingService, private router: Router) {
    
  }

  ngOnInit() {
    if (this.data.filterObject && this.data.organizationList.length > 0) {
      let filter;
      if (typeof this.data.organizationList[0].status === "number") {
        filter = this.data.filterObject;
      } else {
        filter = { status: this.data.filterObject.status === 0 ? false : true };
      }
      this.organization = this.arrayFilterPipe.transform(this.data.organizationList, filter);
    }
    else {
      this.organization = this.data.organizationList;
    }
  }

  editOrganization(organization: any) {
    this.customerOnboardingService.onboardEdit = { organizationId: organization.companyId, userId: null, for: 1 };
    this.router.navigate(['/admin/customer-onboarding']);
  }

}
