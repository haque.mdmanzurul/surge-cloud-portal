import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared-module';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AssetService {

  constructor(public httpService: HttpService) { }

  getAsset(data?: string): Observable<any> {

    return this.httpService.get(`device/api/iot/get/${data}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }
}
