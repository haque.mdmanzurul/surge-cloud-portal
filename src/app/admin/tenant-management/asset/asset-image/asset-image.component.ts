import { Component, OnInit, Input } from '@angular/core';
import { Asset } from '../asset';
import { Router } from '@angular/router';
import { AssetOnboardingService } from '../../../asset-onboarding';
import { ArrayFilterPipe } from '../../../../shared-module';

@Component({
  selector: 'app-asset-image',
  templateUrl: './asset-image.component.html',
  styleUrls: ['./asset-image.component.scss']
})
export class AssetImageComponent implements OnInit {

  @Input() data : {assetList : Asset[], filterObject : any};
  asset: Asset[];
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  constructor(private router: Router,
    private assetOnboardingService: AssetOnboardingService) { }

  ngOnInit() {
    if (this.data.filterObject && this.data.assetList.length >0) {
        let filter;
          if (typeof this.data.assetList[0].status === "number") {
            filter = this.data.filterObject;
          }else{
            filter = {status : this.data.filterObject.status === 0 ? false : true };
          }
          this.asset = this.arrayFilterPipe.transform(this.data.assetList,filter);
    }
    else {
      this.asset = this.data.assetList;
    }
  }

  editAsset(asset) {
    this.assetOnboardingService.onboardingDataForEdit = { index: 0, asset: asset };
    this.router.navigate(['/admin/asset-onboarding']);
  }

}
