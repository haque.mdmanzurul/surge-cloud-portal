import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetComponent } from './asset.component';
import { AssetTableComponent } from './asset-table/asset-table.component';
import { AssetImageComponent } from './asset-image/asset-image.component';
import { AssetChartComponent } from './asset-chart/asset-chart.component';
import { AssetDirective } from './asset.directive';
import { AssetService } from './asset.service';
import { DataTableModule } from 'angular2-datatable';
import { CoreModule } from '../../../core';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    CoreModule
  ],
  declarations: [AssetComponent,
   AssetTableComponent,
   AssetImageComponent,
   AssetChartComponent,
   AssetDirective
   ],
   exports: [AssetComponent,
   AssetTableComponent,
   AssetImageComponent,
   AssetChartComponent
   ],
   providers: [
   AssetService
   ],
   entryComponents: [
   AssetTableComponent,
   AssetImageComponent
   ],
})

export class AssetModule { }
