import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AssetService } from './asset.service';
import { Asset } from './asset';
import { AppService } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { AssetTableComponent } from './';
import { AssetImageComponent } from './';
import { AssetDirective } from './asset.directive';
import { AssetItem } from './asset-item';
import { IWizardItem } from '../../../shared-module';
import { AssetOnboardingService } from '../../asset-onboarding';
import * as _ from 'underscore';
import { PieChartComponent, SingleChartData } from '../../../core';


@Component({
  selector: 'app-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.scss']
})

export class AssetComponent implements OnInit {

  asset: Asset[] = [];

  ArrayFilterPipe = new ArrayFilterPipe();

  assetCount: { active: number, inActive: number } = { active: 0, inActive: 0 };


  assetItem: AssetItem[] = [
    new AssetItem(AssetTableComponent, {}),
    new AssetItem(AssetImageComponent, {})];

  currentIndex = 0;

  public wizardItems: IWizardItem[] = [
    {
      name: 'Asset Table',
      icon: 'glyphicon-th-list',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    },
    {
      name: 'Asset Image',
      icon: 'glyphicon-th',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    },
    {
      name: 'Asset Add',
      icon: 'glyphicon-plus',
      disabled: false,
      index: 2,
      success: false,
      error: false,
      active: false
    }
  ];

  @ViewChild(AssetDirective) assetHost: AssetDirective;
  filterObject: any = null;
  private lastUpdated : Date[] = [];
  public chartData: SingleChartData[] = [];
  public filterStatus: boolean = this.filterObject === null ? false : true;
  constructor(private assetService: AssetService, public assetOnboardingService: AssetOnboardingService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private appService: AppService, private router: Router) {
  }

  ngOnInit() {
    this.getAsset(this.appService.getActiveCompany().companyId);
    this.appService.activeCompanyInfo.subscribe((company) => {
      if (company) {
        this.getAsset(company.companyId);
      }
    })
  }

  getAsset(companyId: string) {
    this.assetService.getAsset(companyId).subscribe(
      (data) => {
        this.asset = [];
        this.chartData = [];
        data.forEach(assetInfo => {
          this.asset.push(new Asset(assetInfo));
        });
        if (this.asset.length > 0) {
          let dates: Date[] = [];
          dates = _.map(this.asset, function(i) { return i.lastUpdatedDate });
          this.lastUpdated = _.sortBy(dates, function(i) { return i });
          let activeCount = (Math.ceil((this.ArrayFilterPipe.transform(this.asset, { status: 1 }).length / this.asset.length) * 100)) +
            (Math.ceil((this.ArrayFilterPipe.transform(this.asset, { status: true }).length / this.asset.length) * 100));
          let inactiveCount = (Math.floor((this.ArrayFilterPipe.transform(this.asset, { status: 0 }).length / this.asset.length) * 100)) +
            (Math.floor((this.ArrayFilterPipe.transform(this.asset, { status: false }).length / this.asset.length) * 100));
          // this.assetCount = {
          //   active: activeCount,
          //   inActive: inactiveCount
          // };
          this.chartData.push({ name: 'Active Assets', value: activeCount },
          { name: 'Inactive Assets', value: inactiveCount });
        }

        this.loadComponent();
      }
    );
  }

  loadComponent() {
    const boardingItem = this.assetItem[this.currentIndex];
    boardingItem.data = { assetList: this.asset, filterObject: this.filterObject };

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.assetHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ data: any }>componentRef.instance).data = boardingItem.data;

  }

  changeComponent(index) {

    if (index <= 1) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    }
    else if (index == 2) {
      this.assetOnboardingService.onboardingDataForEdit = { index: 0, asset: null };
      this.router.navigate(['/admin/asset-onboarding']);
    }

  }
  filterInfo(data) {
    // this.filterObject = { status: $event };
    if (data) {
      let status: number = data.name === 'Active Assets' ? 1 : 0;
      this.filterObject = { status: status };
    } else {
      this.filterObject = null;
    }
    this.filterStatus = this.filterObject === null ? false : true;
    this.loadComponent();
  }

}
