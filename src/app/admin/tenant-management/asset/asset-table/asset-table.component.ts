import { Component, OnInit, Input } from '@angular/core';
import { Asset } from '../asset';
import { TenantManagementService } from '../../tenant-management.service';
import { Router } from '@angular/router';
import { AssetOnboardingService } from '../../../asset-onboarding';
import { ArrayFilterPipe } from '../../../../shared-module';
@Component({
  selector: 'app-asset-table',
  templateUrl: './asset-table.component.html',
  styleUrls: ['./asset-table.component.scss']
})
export class AssetTableComponent implements OnInit {

  @Input() data: {assetList : Asset[], filterObject : any};

  asset: Asset[];

  private paginationOrgInfo = { assetsortOrder: 'asc', assetsortBy : 'assetId',
            numberRowPerPage : 5}
  private statusSvg = ['assets/icons/Management/TenantStatus_inactive.svg#TenantStatus_inactive',
  'assets/icons/Management/TenantStatus_active.svg#TenantStatus_active'];
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();

  constructor(private tenantManagementService: TenantManagementService,
    private router: Router,
    private assetOnboardingService: AssetOnboardingService) {
    this.paginationOrgInfo.numberRowPerPage = this.tenantManagementService.numberOfRecordPerPage;

  }

  ngOnInit() {
    console.log(this.data.assetList);
    if (this.data.filterObject && this.data.assetList.length >0) {
        let filter;
          if (typeof this.data.assetList[0].status === "number") {
            filter = this.data.filterObject;
          }else{
            filter = {status : this.data.filterObject.status === 0 ? false : true };
          }
          this.asset = this.arrayFilterPipe.transform(this.data.assetList,filter);
    }
    else {
      this.asset = this.data.assetList;
    }

  }
  editAsset(asset) {
    this.assetOnboardingService.onboardingDataForEdit = { index: 0, asset: asset};
    this.router.navigate(['/admin/asset-onboarding']);
  }

  
}
