import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-asset-chart',
  templateUrl: './asset-chart.component.html',
  styleUrls: ['./asset-chart.component.scss']
})
export class AssetChartComponent implements OnInit {

  @Input() assetCount: {active: number, inActive: number};
  @Output() clickedChartInfo : EventEmitter<number> = new EventEmitter<number>();
  activeStatus : {active:boolean, inActive:boolean}= {active : false, inActive : false};
  constructor() { }

  ngOnInit() {
  }

  filterData(status:number){
  	this.activeStatus = status ===1 ? {active : true, inActive : false} : {active : false, inActive : true};
  	this.clickedChartInfo.next(status);
  }

}
