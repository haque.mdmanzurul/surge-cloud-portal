import { TestBed, inject } from '@angular/core/testing';

import { TenantManagementService } from './tenant-management.service';

describe('TenantManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TenantManagementService]
    });
  });

  it('should ...', inject([TenantManagementService], (service: TenantManagementService) => {
    expect(service).toBeTruthy();
  }));
});
