import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { MaterialModule } from '@angular/material';
import { AdminRoutingModule } from './admin-routing.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { SharedModule } from '../shared-module';
import { CustomerOnboardingModule } from './customer-onboarding';
import { TenantManagementModule } from './tenant-management';
import { SubscriptionManagementModule } from './subscription-management';
import { AssetOnboardingModule } from './asset-onboarding';

import { DeviceManagementModule } from './device-management';

@NgModule({
  imports: [
    CommonModule,
    DashboardModule,
    // MaterialModule,
    SharedModule,
    CustomerOnboardingModule,
    AdminRoutingModule,
    TenantManagementModule,
    SubscriptionManagementModule,
    AssetOnboardingModule,
    DeviceManagementModule
  ],
  declarations: [],
  exports: [],
})
export class AdminModule { }
