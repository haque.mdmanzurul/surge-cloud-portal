import { Component, OnInit, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { IWizardItem, TabsComponent } from '../../shared-module';
import { AssetOnboardingDirective } from './asset-onboarding.directive';
import { OnboardingItem } from '../onboarding-item';
import { CreateAssetComponent } from './create-asset';
import { CreateDeviceComponent } from './create-device';
import { CreateFacilityComponent } from './create-facility';
import { AssetOnboardingService } from './asset-onboarding.service';
import { Asset, Device, Facility, AssetOnboardingStatus } from './asset-onboarding';
import { NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-asset-onboarding',
  templateUrl: './asset-onboarding.component.html',
  styleUrls: ['./asset-onboarding.component.scss']
})
export class AssetOnboardingComponent implements OnInit {

  private currentAddIndex: number = 0;
  private asset: Asset;
  public wizardItems: IWizardItem[] = [
    {
      name: 'Create Asset',
      icon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Asset.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Asset_White.svg#Vector_Smart_Object',
      disabled: true,
      index: 0,
      success: false,
      error: false,
      active: true,
      tabId: 'assetTab'
    },
    {
      name: 'Create Facility',
      icon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Facility.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Facility_White.svg#Vector_Smart_Object',
      disabled: true,
      index: 1,
      success: false,
      error: false,
      active: false,
      tabId: 'facilityTab'
    },
    {
      name: 'Create Device',
      icon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Device.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Device_White.svg#Vector_Smart_Object',
      disabled: true,
      index: 2,
      success: false,
      error: false,
      active: false,
      tabId: 'deviceTab'
    }
  ];

  private statusIcons = [
    'assets/icons/common/TabStatus_Error.svg#_',
    'assets/icons/common/TabStatus_Success.svg#Vector_Smart_Object_copy_13'
  ];


  public notificationOptions = {
    timeOut: 5000,
    position: ['right']
  };
  private isForDevice: Boolean = false;
  private newDevice: Device;
  private newFacility: Facility;

  private isForDeviceOrFacility: Boolean = false;

  @ViewChild('assetBoardingTab') private tab: NgbTabset;

  public data: AssetOnboardingStatus = new AssetOnboardingStatus();

  constructor(private _componentFactoryResolver: ComponentFactoryResolver,
    private onboardingService: AssetOnboardingService) { }

  ngOnInit() {
    setTimeout(
      () => {
        const data = this.onboardingService.onboardingDataForEdit;
        if (data && data.index == 1) {
          this.isForDeviceOrFacility = true;
          this.tab.select('facilityTab');
        }

        if (data && data.index == 2) {
          this.isForDeviceOrFacility = true;
          this.tab.select('deviceTab');
        }
      }, 10);

  }


  createDeviceOrFacility($event: { asset, selectedDevice, selectedFacility, isForDevice , isEditMode }) {
    this.data.asset = $event.asset;
    this.data.device = $event.selectedDevice;
    this.data.facility = $event.selectedFacility;
    this.data.isEditMode = $event.isEditMode;
    this.isForDeviceOrFacility = true;
    this.onboardingService.onboardingData = this.data;
    this.tab.select($event.isForDevice ? 'deviceTab' : 'facilityTab');
  }

  deviceOrFacilityCreated($event: { data: any, isForDevice: Boolean, cancel: Boolean }) {
    if (!$event.cancel) {
      if ($event.isForDevice) {
        this.data.device = $event.data;
        this.wizardItems[2].statusIcon = this.statusIcons[1];
      } else {
        this.data.facility = $event.data;
        this.wizardItems[1].statusIcon = this.statusIcons[1];
      }
      this.onboardingService.onboardingData = this.data;
    }
    this.isForDeviceOrFacility = false;
    this.tab.select('assetTab');
  }


  beforeChange($event) {
    switch ($event.nextId) {
      case 'assetTab':
        if (this.isForDeviceOrFacility) {
          $event.preventDefault();
        } else {
          this.makeActiveTab($event.nextId);
        }
        break;
      case 'facilityTab':
        if (!this.isForDeviceOrFacility) {
          $event.preventDefault();
        } else {
          this.makeActiveTab($event.nextId);
        }

        break;
      case 'deviceTab':
        if (!this.isForDeviceOrFacility) {
          $event.preventDefault();
        } else {
          this.makeActiveTab($event.nextId);
        }
        break;
    }

  }

  gotoPreviousTab(tabId: string) {
    this.tab.select(tabId);
  }

  makeActiveTab(tabId: string) {
    this.wizardItems.forEach(
      (wizard) => {
        if (wizard.tabId == tabId) {
          wizard.active = true;
        } else {
          wizard.active = false;
        }
      }
    )
  }

}
