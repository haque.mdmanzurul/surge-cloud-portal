import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { CreateAssetComponent } from './create-asset/create-asset.component';
import { CreateFacilityComponent } from './create-facility/create-facility.component';
import { CreateDeviceComponent } from './create-device/create-device.component';
import { FormsModule } from '@angular/forms';
import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications';
import { SharedModule } from '../../shared-module';
import { AssetOnboardingDirective } from './asset-onboarding.directive';
import { AssetOnboardingService } from './asset-onboarding.service';
import { CoreModule } from '../../core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';
import { AppService } from '../../app.service';
import { RouterModule, Router } from '@angular/router';

import { AssetOnboardingComponent } from './asset-onboarding.component';

const AssetOnboardingServiceStub = {
  changeStatusSource: {},
  changeStatus: {},
  _onboardingData: {},
  _onboardingDataForEdit: {},
  getAssetType: () => { }
};

describe('AssetOnboardingComponent', () => {
  let component: AssetOnboardingComponent;
  let fixture: ComponentFixture<AssetOnboardingComponent>;
  let assetOnboardingService: AssetOnboardingService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        SimpleNotificationsModule,
        SharedModule,
        CoreModule,
        NgbModule,
        RouterModule
      ],
      declarations: [
        CreateAssetComponent,
        CreateFacilityComponent,
        CreateDeviceComponent,
        AssetOnboardingDirective,
        AssetOnboardingComponent
      ],
      providers: [
        NgbTabsetConfig,
        NotificationsService,
        { provide: AssetOnboardingService, useValue: AssetOnboardingServiceStub },
        { provide: AppService, useValue: {} },
        { provide: Router, useValue: {}}
      ],
      schemas: [NO_ERRORS_SCHEMA]

    }).compileComponents();
    fixture = TestBed.createComponent(AssetOnboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    assetOnboardingService = TestBed.get(AssetOnboardingService);
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });



});
