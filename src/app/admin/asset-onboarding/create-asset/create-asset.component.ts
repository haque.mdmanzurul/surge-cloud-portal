import { Component, OnInit, Input, Output } from '@angular/core';
import { Asset, AssetType, Facility, Device, MapDeviceAsset, MapDeviceFacility } from '../asset-onboarding';
import { AssetOnboardingService } from '../asset-onboarding.service';
import { Subject } from 'rxjs/Subject';
import { NotificationsService } from 'angular2-notifications';
import { AppService } from '../../../app.service';
import { Router } from '@angular/router';
import * as _ from 'underscore';

@Component({
  selector: 'app-create-asset',
  templateUrl: './create-asset.component.html',
  styleUrls: ['./create-asset.component.scss']
})
export class CreateAssetComponent implements OnInit {

  public asset: Asset = new Asset({ Status: true });
  private avilableaAssetType: AssetType[] = [];
  @Output() public createDeviceOrFacility: Subject<{ asset: Asset, selectedDevice: Device, selectedFacility: Facility, isForDevice: Boolean, isEditMode: Boolean }> = new Subject();
  private availableFacility: Facility[] = [];
  private availableDevice: Device[] = [];
  public selectedDevice: Device;
  public selectedFacility: Facility;
  private logoFile: any;
  private isEditMode: Boolean = false;
  private availableAsset: Asset[] = [];
  private assetIdIndex: Asset;
  private existingAssest: any;

  constructor(private assetOnboardingService: AssetOnboardingService, private appService: AppService,
    private _notificationService: NotificationsService, private router: Router) { }

  ngOnInit() {
    const data = this.assetOnboardingService.onboardingData;
    if (data) {
      if (data.asset) {
        this.asset = data.asset;
      }
      if (data.facility) {
        this.selectedFacility = data.facility;
      }
      if (data.device) {
        this.selectedDevice = data.device;
      }
      if (data.isEditMode) {
        this.isEditMode = true;
        this.existingAssest = this.asset;
        this.getAssetAvailable();
      }
    }

    const dataForEdit = this.assetOnboardingService.onboardingDataForEdit;
    if (dataForEdit && dataForEdit.asset) {
      this.isEditMode = true;
      this.existingAssest = dataForEdit.asset;
      this.getAssetAvailable();
    }

    this.getAssetType();
    this.getAvailableFacility();
    this.getAvailableDevice();
  }

  getAssetType() {
    this.assetOnboardingService.getAssetType().subscribe(
      (data) => {
        if (data) {
          data.forEach(assettype => {
            let model = new AssetType(assettype.Name, assettype.Status);
            this.avilableaAssetType.push(model);
          });
        }
      }
    );
  }

  getAvailableFacility() {
    this.assetOnboardingService.getFacility(this.appService.getActiveCompany().companyId).subscribe(
      (data) => {
        data.forEach(facility => {
          let model = new Facility(facility.logo, facility.name, facility.status, facility.address, facility.timezone, facility.facilityType, facility.facilityId);
          this.availableFacility.push(model);
        });
        if (this.selectedFacility) {
          const indexForFacility = _.indexOf(this.availableFacility, _.findWhere(this.availableFacility, { facilityId: this.selectedFacility.facilityId }));
          this.selectedFacility = this.availableFacility[indexForFacility];
        }
      }
    );
  }

  getAvailableDevice() {
    this.assetOnboardingService.getDevice(this.appService.getActiveCompany().companyId).subscribe(
      (data) => {
        data.forEach(device => {
          let model = new Device(this.appService.getActiveCompany().companyId,
            this.appService.getActiveCompany().name, device.logo, device.gwSNO,
            device.Status, device.IotHubName, device.pVer, device.gwOSBuild, device.gwAppBuild,
            device.gwDCAver, device.gwSIMICCID, device.gwIMEI, device.gwRSSI, device.gwWANIP,
            device.gwTimezone, device.Latitude, device.Longitude, device.Altitude, device.gwDescription, device.ControllerId);
          this.availableDevice.push(model);
        });

        if (this.selectedDevice) {
          const indexFordevice = _.indexOf(this.availableDevice, _.findWhere(this.availableDevice,
            { controllerId: this.selectedDevice.controllerId }));
          this.selectedDevice = this.availableDevice[indexFordevice];
        }
      }
    );

  }

  getNewDeviceOrFacility(isForDevice = false) {
    this.createDeviceOrFacility.next({ asset: this.asset, selectedDevice: this.selectedDevice, selectedFacility: this.selectedFacility, isForDevice: isForDevice, isEditMode: this.isEditMode });
  }


  changeImage(image: { file, data }) {
    const pattern = new RegExp("image/");
    if (image.file && pattern.test(image.file.type)) {
      this.asset.logo = image.data;
      this.logoFile = image.file;
    }
  }

  createAsset(isValid: boolean) {
    if (isValid) {
      let logoName = '';
      let splittedName = (this.asset.assetId.replace(/[^\w\s]/gi, '')).split(' ');
      let randomNumber: any = Math.random().toFixed(2);
      splittedName.push((randomNumber * 100).toString());

      if (this.logoFile) {
        logoName = `${splittedName.join('_')}.${this.logoFile.name.split('.').pop()}`;
      }

      let data = {
        aId: this.asset.aId,
        assetId: this.asset.assetId,
        companyId: this.appService.getActiveCompany().companyId,
        companyName: this.appService.getActiveCompany().name,
        assetType: this.asset.assetType,
        logo: this.isEditMode ? this.asset.logo : logoName,
        make: this.asset.make,
        model: this.asset.model,
        serialNumber: this.asset.serialNumber,
        unitNumber: this.asset.unitNumber,
        vin: this.asset.vin,
        softwareId: this.asset.softwareId,
        calibrationId: this.asset.calibrationId,
        customerEquipmentGroup: this.asset.customerEquipmentGroup,
        customerEquipmentId: this.asset.customerEquipmentId,
        status: this.asset.status ? 'active' : 'Inactive'
      };
      this.checkExistingDevice((isValid) => {
        if (isValid) {
          if (this.logoFile) {
            this.uploadLogo(data, logoName);
          } else {
            this.saveAsset(data);
          }
        }
      });
    }
  }
  uploadLogo(asset, logoName) {
    if (this.logoFile) {
      this.assetOnboardingService.uploadLogo(this.logoFile, logoName)
        .subscribe(
        (data) => {
          console.log(data);
          asset.logo = data._body;
          this.saveAsset(asset)
        },
        (error) => {
          console.log(error);
        },
        () => {
          console.log('image uploaded');
        }
        );
    }
  }

  saveAsset(asset) {
    this.assetOnboardingService.createAsset(asset, this.isEditMode).subscribe(
      (res) => {
        this.appService.showSuccessMessage(
          'Success',
          `Asset is ${!this.isEditMode ? 'created' : 'updated'} and redirecting to Dashboard`
        );
        const savedAsset = new Asset(res);
        const mapDeviceModel = new MapDeviceAsset(
          this.selectedDevice.controllerId,
          this.selectedDevice.gatewaySno,
          savedAsset.aId,
          savedAsset.assetId,
          this.appService.getActiveCompany().companyId,
          this.appService.getActiveCompany().name
        );
        const mapFacilityModel = new MapDeviceFacility(
          this.selectedDevice.controllerId,
          this.selectedDevice.gatewaySno,
          this.selectedFacility.facilityId,
          this.selectedFacility.facilityName,
          this.appService.getActiveCompany().companyId,
          this.appService.getActiveCompany().name
        );
        this.mapDeviceAsset([mapDeviceModel]);
        this.mapFacilityAsset([mapFacilityModel]);
        this.resetForm();
        this.selectedFacility = null;
        this.selectedDevice = null;
        if(this.assetOnboardingService.onboardingDataForEdit && this.assetOnboardingService.onboardingDataForEdit.asset){
          this.router.navigate(['admin/tenant-management']);
        }else{
          this.router.navigate([this.appService.getLandingPage()]);
        }
        
      },
      (error) => {
        this.appService.showErrorMessage('Error', (error.json() && error.json().message || error.json().ErrorMessage));
      }
    );
  }


  mapDeviceAsset(data: MapDeviceAsset[]) {
    this.assetOnboardingService.mapDeviceAsset(data).subscribe(
      (res) => {
        console.log('asset mapped with device');
      });
  }

  mapFacilityAsset(data: MapDeviceFacility[]) {
    this.assetOnboardingService.mapFacilityAsset(data).subscribe(
      (res) => {
        console.log('facility mapped with asset');
      });
  }

  resetForm() {
    this.asset = new Asset({ Status: true });
    this.selectedDevice = null;
    this.selectedFacility = null;
  }

  getAssetAvailable() {
    this.assetOnboardingService.getAsset(this.appService.getActiveCompany().companyId).subscribe(
      (data) => {
        this.availableAsset = [];
        data.forEach((assetData) => {
          let model = new Asset(assetData);
          this.availableAsset.push(model);
        });
        if (this.existingAssest) {
          this.assetIdIndex = _.findWhere(this.availableAsset, { assetId: this.existingAssest.AssetId });
          this.getSelectedAsset(this.assetIdIndex);
        }
      }
    )
  };

  getSelectedAsset(selectedAsset: Asset) {
    this.asset = selectedAsset;
    this.getDeviceAndFacilityByAsset(selectedAsset.assetId);
  }

  cancel() {
    this.router.navigate(['/']);
  }

  getDeviceAndFacilityByAsset(assetId: string) {
    this.assetOnboardingService.getDeviceByAsset(this.appService.getActiveCompany().companyId, assetId).subscribe(
      (res) => {
        if (res) {
          if (res[0] && res[0].FacilityId) {
            const indexForFacility = _.indexOf(this.availableFacility, _.findWhere(this.availableFacility, { facilityId: res[0].FacilityId }));
            this.selectedFacility = this.availableFacility[indexForFacility];
          }
          if (res[0] && res[0].ControllerId) {
            const indexFordevice = _.indexOf(this.availableDevice, _.findWhere(this.availableDevice, { controllerId: res[0].ControllerId }));
            this.selectedDevice = this.availableDevice[indexFordevice];
          }
        }
      }
    );
  }

  checkExistingDevice(callback) {
    this.assetOnboardingService.checkAsset(this.appService.getActiveCompany().companyId, this.selectedDevice.gatewaySno, this.asset.assetId).subscribe(
      (res) => {
        callback(res);
      }, (error) => {
        this.appService.showErrorMessage('Error', (error.json() && error.json().message || error.json().ErrorMessage));
      }
    );
  }

}
