import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../../../core';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { AssetOnboardingService } from '../asset-onboarding.service';
import { NotificationsService } from 'angular2-notifications';
import { AppService } from '../../../app.service';
import { Router } from '@angular/router';




import { CreateAssetComponent } from './create-asset.component';

describe('CreateAssetComponent', () => {
  let component: CreateAssetComponent;
  let fixture: ComponentFixture<CreateAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      declarations: [CreateAssetComponent],
      providers: [
        { provide: AssetOnboardingService, useValue: {} },
        { provide: AppService, useValue: {} },
        { provide: NotificationsService, useValue: {} },
        { provide: Router, useValue: {} }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents().then(
      () => {
        fixture = TestBed.createComponent(CreateAssetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      }
      );

  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(CreateAssetComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
