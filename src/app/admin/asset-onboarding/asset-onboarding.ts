export class Asset {
  public aId: string;
  public assetId: string;
  public companyId: string;
  public companyName: string;
  public assetType: string;
  public logo: string;
  public make: string;
  public model: string;
  public serialNumber: string;
  public unitNumber: string;
  public vin: string;
  public softwareId: string;
  public calibrationId: string;
  public customerReference: string;
  public customerFacilityGroup: string;
  public customerEquipmentGroup: string;
  public customerEquipmentId: string;
  public tags: string[];
  public status: boolean;
  constructor(data) {
    this.aId = data.AId || '';
    this.assetId = data.AssetId || '';
    this.companyId = data.CompanyId || '';
    this.companyName = data.CompanyName || '';
    this.assetType = data.AssetType || '';
    this.logo = data.Logo || '';
    this.make = data.Make || '';
    this.model = data.Model || '';
    this.serialNumber = data.SerialNumber || '';
    this.unitNumber = data.UnitNumber || '';
    this.vin = data.Vin || '';
    this.softwareId = data.SoftwareId || '';
    this.calibrationId = data.CalibrationId || '';
    this.customerReference = data.CustomerReference || '';
    this.customerFacilityGroup = data.CustomerFacilityGroup || '';
    this.customerEquipmentGroup = data.CustomerEquipmentGroup || '';
    this.customerEquipmentId = data.CustomerEquipmentId || '';
    this.tags = data.Tags || '';
    this.status = data.Status ? true : false;
  }
}

export class AssetType {
  constructor(
    public name: string,
    public status: number
  ) {
    this.name = name;
    this.status = status;
  }
}

export class Timezone {
  constructor(
    public id: string,
    public displayName: string,
    public daylightName: string,
    public standardName: string,
    public supportsDaylightSavingTime: boolean
  ) {
    this.id = id;
    this.displayName = displayName;
    this.daylightName = daylightName;
    this.standardName = standardName;
    this.supportsDaylightSavingTime = supportsDaylightSavingTime;
  }
}

export class Facility {
  constructor(
    public logo: string,
    public facilityName: string,
    public status: boolean,
    public address: any,
    public timezone: string,
    public facilityType: string,
    public facilityId?: string
  ) {
    this.logo = logo;
    this.facilityName = facilityName;
    this.status = status;
    this.timezone = timezone;
    this.facilityType = facilityType;
    this.address = new Address(address || {});
    this.facilityId = facilityId;
  }
}


export class Address {
  public name: string;
  public street1: string;
  public street2: string;
  public city: string;
  public stateCode?: string;
  public postcode: string;
  public countryCode: string;
  public latitude: number;
  public longitude: number;
  constructor(private address: any = {}) {
    this.name = address.name || '';
    this.street1 = address.street1 || '';
    this.street2 = address.street2 || '';
    this.city = address.city || '';
    this.stateCode = address.stateCode || '';
    this.postcode = address.postCode || '';
    this.countryCode = address.countryCode || '';
    this.latitude = address.latitude || 0;
    this.longitude = address.longitude || 0;
  }
}

export class Device {
  constructor(
    public companyId: string,
    public companyName: string,
    public logo: string,
    public gatewaySno: string,
    public status: boolean,
    public iotHub: any,
    public productVersion: string,
    public osBuild: string,
    public appBuild: string,
    public dcaVersion: string,
    public simiccid: string,
    public imei: string,
    public rssi: string,
    public wanip: string,
    public timezone: string,
    public latitude: string,
    public longitude: string,
    public altitude: string,
    public description: string,
    public controllerId: string
  ) {
    this.logo = logo;
    this.companyId = companyId;
    this.companyName = companyName;
    this.gatewaySno = gatewaySno;
    this.status = status ? status : true;
    this.iotHub = iotHub;
    this.productVersion = productVersion;
    this.osBuild = osBuild;
    this.appBuild = appBuild;
    this.dcaVersion = dcaVersion;
    this.simiccid = simiccid;
    this.imei = imei;
    this.rssi = rssi;
    this.wanip = wanip;
    this.timezone = timezone;
    this.latitude = latitude;
    this.longitude = longitude;
    this.altitude = altitude;
    this.description = description;
    this.controllerId = controllerId;
  }
}

export class IotHub {
  constructor(
    public iotHubUri: string,
    public name: string,
    public status: boolean
  ) {
    this.iotHubUri = iotHubUri;
    this.name = name;
    this.status = status ? true : false;
  }
}

export class MapDeviceAsset {
  constructor(
    public ControllerId: string,
    public gwSNO: string,
    public AId: string,
    public AssetId: string,
    public CompanyId: string,
    public CompanyName: string
  ) {
    this.ControllerId = ControllerId;
    this.gwSNO = gwSNO;
    this.AId = AId;
    this.AssetId = AssetId;
    this.CompanyId = CompanyId;
    this.CompanyName = CompanyName;
  }
}

export class MapDeviceFacility {
  constructor(
    public ControllerId: string,
    public gwSNO: string,
    public FacilityId: string,
    public FacilityName: string,
    public CompanyId: string,
    public CompanyName: string
  ) {
    this.ControllerId = ControllerId;
    this.gwSNO = gwSNO;
    this.FacilityId = FacilityId;
    this.FacilityName = FacilityName;
    this.CompanyId = CompanyId;
    this.CompanyName = CompanyName;
  }
}

export class AssetOnboardingStatus {
  public index: number;
  public success: Boolean = false;
  public asset: Asset;
  public device: Device;
  public facility: Facility;
  public message: any;
  public isEditMode: Boolean = false;
}