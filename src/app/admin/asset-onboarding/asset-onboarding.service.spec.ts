import { TestBed, inject } from '@angular/core/testing';

import { AssetOnboardingService } from './asset-onboarding.service';
import { HttpService } from '../../shared-module';


describe('AssetOnboardingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssetOnboardingService, {provide:HttpService, useValue:{}}]
    });
  });

  it('should ...', inject([AssetOnboardingService], (service: AssetOnboardingService) => {
    expect(service).toBeTruthy();
  }));
});
