import { Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appAssetOnboarding]'
})
export class AssetOnboardingDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
