import { Component, OnInit, ElementRef, Output, EventEmitter } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { Device, IotHub, Timezone, Asset } from '../asset-onboarding';
import { AssetOnboardingService } from '../asset-onboarding.service';
import { Subject } from 'rxjs/Subject';
import { AppService } from '../../../app.service';
import * as _ from 'underscore';
import { Router } from '@angular/router';

declare const google: any;
@Component({
  selector: 'app-create-device',
  templateUrl: './create-device.component.html',
  styleUrls: ['./create-device.component.scss']
})
export class CreateDeviceComponent implements OnInit {

  private address: any;
  private device: Device = new Device('', '', '', '', true, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
  private iotHub: IotHub[] = [];
  private timezone: Timezone[] = [];
  private elevator: any;
  @Output() public deviceOrFacilityCreated: Subject<{ data: Device, isForDevice: Boolean, cancel: Boolean }> = new Subject();
  @Output() public back: EventEmitter<string> = new EventEmitter();
  public availbleDevice: Device[] = [];
  public geoCoder: any;
  private addressname: string;
  private logoFile: any;
  private isEditMode: Boolean = false;
  private existingDeviceEdit: any;
  private deviceSelected: Device;
  constructor(private assetOnboardingService: AssetOnboardingService, private elementRef: ElementRef,
    private mapsAPILoader: MapsAPILoader, private appService: AppService, private router: Router) { }

  ngOnInit() {
    this.getIotHub();
    this.getTimezone();

    this.mapsAPILoader.load().then(() => {
      const autocomplete: any = new google.maps.places.Autocomplete(this.elementRef.nativeElement.querySelector('input[name=addressName]'), {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        const place: any = autocomplete.getPlace();
        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.setAddress(place);
      });
      if (!this.elevator) {
        this.elevator = new google.maps.ElevationService;
      }
      if (!this.geoCoder) {
        this.geoCoder = new google.maps.Geocoder;
      }
    });

    const data = this.assetOnboardingService.onboardingDataForEdit;
    if (data && data.index == 2 && data.device) {
      this.isEditMode = true;
      this.existingDeviceEdit = data.device;
      this.getAvailableDevice();
    }


  }

  getIotHub() {
    this.assetOnboardingService.getIotHub().subscribe(
      (data) => {
        data.forEach((iothub) => {
          this.iotHub.push(new IotHub(iothub.IotHubUri, iothub.Name, iothub.status));
        });
      }
    );
  }

  getTimezone() {
    this.assetOnboardingService.getTimezone().subscribe(
      (data) => {
        data.forEach((timezone: Timezone) => {
          this.timezone.push(new Timezone(timezone.id, timezone.displayName, timezone.daylightName, timezone.standardName, timezone.supportsDaylightSavingTime));
        });
      }
    );
  }

  setAddress(place: any) {
    this.address = {
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
      altitude: 0
    };
    this.getAltitude(place.geometry.location.lat(), place.geometry.location.lng());
  }

  getAltitude(lat, lng) {
    this.elevator.getElevationForLocations({
      'locations': [{ lat: lat, lng: lng }]
    }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.address.altitude = results[0].elevation;
        } else {
          console.log('No result found');
        }
      } else {
        console.log('Elevation service failed due to: ' + status);
      }
    });
  }

  changeImage(image: { file, data }) {
    const pattern = new RegExp("image/");
    if (image.file && pattern.test(image.file.type)) {
      this.device.logo = image.data;
      this.logoFile = image.file;
    }
  }

  saveDevice(isValid: Boolean): void {
    if (isValid) {
      let logoName = '';
      let splittedName = (this.device.gatewaySno.replace(/[^\w\s]/gi, '')).split(' ');
      let randomNumber: any = Math.random().toFixed(2);
      splittedName.push((randomNumber * 100).toString());

      if (this.logoFile) {
        logoName = `${splittedName.join('_')}.${this.logoFile.name.split('.').pop()}`;
      }

      let data = {
        companyId: this.appService.getActiveCompany().companyId,
        companyName: this.appService.getActiveCompany().name,
        IotHubName: this.device.iotHub,
        pVer: this.device.productVersion,
        gwSNO: this.device.gatewaySno,
        gwDescription: this.device.description,
        gwOSBuild: this.device.osBuild,
        gwAppBuild: this.device.appBuild,
        gwDCAver: this.device.dcaVersion,
        gwSIMICCID: this.device.simiccid,
        gwIMEI: this.device.imei,
        gwRSSI: this.device.rssi,
        gwTimezone: this.device.timezone,
        gwWANIP: this.device.wanip,
        Latitude: this.address && this.address.latitude ? this.address.latitude : null,
        Longitude: this.address && this.address.longitude ? this.address.longitude : null,
        Altitude: this.address && this.address.altitude ? this.address.altitude : null,
        Logo: this.isEditMode ? this.device.logo : null,
        status: this.device.status ? 1 : 0
      };

      if (this.logoFile) {
        this.uploadLogo(data, logoName)
      } else {
        this.createDeviceAPI(data);
      }
    }

  }

  uploadLogo(data, logoName) {
    if (this.logoFile) {
      this.assetOnboardingService.uploadLogo(this.logoFile, logoName)
        .subscribe(
        (res) => {
          data.Logo = res._body;
          this.createDeviceAPI(data);
        },
        (error) => {
          console.log(error);
        },
        () => {
          console.log('image uploaded');
        }
        );
    }
  }

  createDeviceAPI(data) {
    this.assetOnboardingService.createDevice(data, this.isEditMode).subscribe(
      (response) => {
        console.log('device update or crated')
        const model = new Device(
          this.appService.getActiveCompany().companyId,
          this.appService.getActiveCompany().name,
          response.Logo,
          response.gwSNO,
          response.Status,
          response.IotHubName,
          response.pVer,
          response.gwOSBuild,
          response.gwAppBuild,
          response.gwDCAver,
          response.gwSIMICCID,
          response.gwIMEI,
          response.gwRSSI,
          response.gwWANIP,
          response.gwTimezone,
          response.Latitude,
          response.Longitude,
          response.Altitude,
          response.gwDescription,
          response.ControllerId);
        if (this.assetOnboardingService.onboardingDataForEdit && this.assetOnboardingService.onboardingDataForEdit.index == 2) {
          this.router.navigate(['admin/device-management']);
        } else {
          this.deviceOrFacilityCreated.next({ data: model, isForDevice: true, cancel: false });
        }
      },
      (error) => {
        this.appService.showErrorMessage('Error', (error.json() && error.json().message || error.json().ErrorMessage));
      }
    );
  }

  getAvailableDevice() {
    this.assetOnboardingService.getDevice(this.appService.getActiveCompany().companyId)
      .subscribe(
      (response) => {
        this.availbleDevice = [];
        response.forEach(
          (deviceAvailable) => {
            const model = new Device(
              deviceAvailable.CompanyId,
              deviceAvailable.CompanyName,
              deviceAvailable.Logo,
              deviceAvailable.gwSNO,
              deviceAvailable.Status,
              deviceAvailable.IotHubName,
              deviceAvailable.pVer,
              deviceAvailable.gwOSBuild,
              deviceAvailable.gwAppBuild,
              deviceAvailable.gwDCAver,
              deviceAvailable.gwSIMICCID,
              deviceAvailable.gwIMEI,
              deviceAvailable.gwRSSI,
              deviceAvailable.gwWANIP,
              deviceAvailable.gwTimezone,
              deviceAvailable.Latitude,
              deviceAvailable.Longitude,
              deviceAvailable.Altitude,
              deviceAvailable.gwDescription,
              deviceAvailable.ControllerId);
            this.availbleDevice.push(model);
          }
        );
        if (this.existingDeviceEdit) {
          this.deviceSelected = _.findWhere(this.availbleDevice, { gatewaySno: this.existingDeviceEdit.gwSNO });
          this.setSelectedDeive(this.deviceSelected);
        }
      }
      );
  }

  setSelectedDeive(selected: Device) {
    this.device = selected;
    this.address = {
      latitude: selected.latitude,
      longitude: selected.longitude,
      altitude: selected.altitude
    };
    this.getAddress(parseFloat(this.device.latitude), parseFloat(this.device.longitude));
  }

  getAddress(lat: any, lng: any) {
    const latlng = { lat: parseFloat(lat), lng: parseFloat(lng) };
    this.geoCoder.geocode({ 'location': latlng }, (results, status) => {
      if (status === 'OK') {
        if (results[1]) {
          this.addressname = results[1].formatted_address;
        }
      }
    });
  }

  resetForm() {
    this.device = new Device('', '', '', '', true, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
    this.addressname = '';
  }

  cancel() {
    this.deviceOrFacilityCreated.next({ data: null, isForDevice: true, cancel: true });
  }

}
