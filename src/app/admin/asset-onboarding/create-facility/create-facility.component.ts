import { Component, OnInit, ElementRef, Output, EventEmitter } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { AssetOnboardingService } from '../asset-onboarding.service';
import { Timezone, Facility, Asset } from '../asset-onboarding';
import { AppService } from '../../../app.service';
import { Subject } from 'rxjs/Subject';
import * as _ from 'underscore';
import { Router } from '@angular/router';
declare const google: any;

@Component({
  selector: 'app-create-facility',
  templateUrl: './create-facility.component.html',
  styleUrls: ['./create-facility.component.scss']
})
export class CreateFacilityComponent implements OnInit {
  private address: any;
  private timezone: Timezone[] = [];
  private facility: Facility = new Facility('', '', true, {}, '', '');
  private logoFile: any;
  @Output() private deviceOrFacilityCreated: Subject<{ data: Facility, isForDevice: Boolean, cancel: Boolean }> = new Subject();
  @Output() public back: EventEmitter<string> = new EventEmitter();
  private isEditMode: Boolean = false;
  private availableFacility: Facility[] = [];
  private addressname: string;
  private facilityIndex: string;

  private existingFacilityEdit: any;
  constructor(private mapsAPILoader: MapsAPILoader, private elementRef: ElementRef,
    private assetOnboardingService: AssetOnboardingService, private appService: AppService,
    public router: Router) { }

  ngOnInit() {
    const data = this.assetOnboardingService.onboardingDataForEdit;
    if (data && data.index == 1 && data.facility) {
      this.isEditMode = true;
      this.existingFacilityEdit = data.facility;
      this.getAvailableFacility();
    }

    this.mapsAPILoader.load().then(() => {
      const autocomplete: any = new google.maps.places.Autocomplete(this.elementRef.nativeElement.querySelector('input[name=addressName]'), {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        const place: any = autocomplete.getPlace();
        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.setAddress(place);
      });
    });

    this.getTimezone();
  }

  setAddress(place: any) {
    let street = this.getAddrComponent(place, { street_number: 'short_name' });
    if (street) {
      street = `, ${this.getAddrComponent(place, { route: 'long_name' })}`;
    } else {
      street = this.getAddrComponent(place, { route: 'long_name' });
    }

    this.address = {
      name: place.name,
      street1: street,
      street2: this.getAddrComponent(place, { neighborhood: "long_name" }),
      city: this.getAddrComponent(place, { locality: 'long_name' }),
      stateCode: this.getAddrComponent(place, { administrative_area_level_1: 'long_name' }),
      postcode: this.getAddrComponent(place, { postal_code: 'long_name' }),
      countryCode: this.getAddrComponent(place, { country: 'short_name' }),
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng()
    };
  }

  getAddrComponent(place: any, componentTemplate: any) {
    for (let i = 0; i < place.address_components.length; i++) {
      const addressType = place.address_components[i].types[0];
      if (componentTemplate[addressType]) {
        return place.address_components[i][componentTemplate[addressType]];
      }
    }
    return '';
  }

  getTimezone() {
    this.assetOnboardingService.getTimezone().subscribe(
      (data) => {
        data.forEach((timezone: Timezone) => {
          this.timezone.push(new Timezone(timezone.id, timezone.displayName, timezone.daylightName, timezone.standardName, timezone.supportsDaylightSavingTime));
        });
      }
    );
  }

  changeImage(image: { file, data }) {
    const pattern = new RegExp("image/");
    if (image.file && pattern.test(image.file.type)) {
      this.facility.logo = image.data;
      this.logoFile = image.file;
    }
  }

  saveFacility(isValid: Boolean) {
    if (isValid) {
      let logoName = '';
      let splittedName = (this.facility.facilityName.replace(/[^\w\s]/gi, '')).split(' ');
      let randomNumber: any = Math.random().toFixed(2);
      splittedName.push((randomNumber * 100).toString());

      if (this.logoFile) {
        logoName = `${splittedName.join('_')}.${this.logoFile.name.split('.').pop()}`;
      }
      let data = {
        facilityId: this.facility.facilityId,
        name: this.facility.facilityName,
        companyId: this.appService.getActiveCompany().companyId,
        logo: this.isEditMode ? this.facility.logo : null,
        address: this.address,
        timezone: this.facility.timezone,
        facilityType: this.facility.facilityType,
        status: this.facility.status ? 'active' : 'Inactive'
      }
      if (this.logoFile) {
        this.uploadLogo(data, logoName);
      } else {
        this.createFacilityAPI(data);
      }
    }
  }

  resetForm() {
    this.facility = new Facility('', '', true, {}, '', '');
  }

  getAvailableFacility() {
    this.assetOnboardingService.getFacility(this.appService.getActiveCompany().companyId).subscribe(
      (data) => {
        data.forEach((facilityData) => {
          let model = new Facility(facilityData.logo, facilityData.name, facilityData.status, facilityData.address, facilityData.timezone, facilityData.facilityType, facilityData.facilityId);
          this.availableFacility.push(model);
        });
        if (this.existingFacilityEdit) {
          this.facilityIndex = _.findWhere(this.availableFacility, { facilityId: this.existingFacilityEdit.facilityId });
          this.getSelectedFacility(this.facilityIndex);
        }
      }
    );
  }

  uploadLogo(fac, logoName) {
    if (this.logoFile) {
      this.assetOnboardingService.uploadLogo(this.logoFile, logoName)
        .subscribe(
        (data) => {
          fac.logo = data._body;
          this.createFacilityAPI(fac)
        },
        (error) => {
          console.log(error);
        },
        () => {
          console.log('image uploaded');
        }
        );
    }
  }

  createFacilityAPI(fac) {
    this.assetOnboardingService.saveFacility(fac, this.isEditMode).subscribe(
      (data) => {
        const model = new Facility(data.logo, data.name, data.status, data.address, data.timezone, data.facilityType, data.facilityId);
        if (this.assetOnboardingService.onboardingDataForEdit && this.assetOnboardingService.onboardingDataForEdit.index == 1) {
          this.router.navigate(['admin/tenant-management']);
        }else {
          this.deviceOrFacilityCreated.next({ data: model, isForDevice: false, cancel: false });
        }
      },
      (error) => {
        this.appService.showErrorMessage('Error', (error.json() && error.json().message || error.json().ErrorMessage));
      }
    );

  }
  getSelectedFacility(exfac) {
    this.facility = exfac;
    this.address = exfac.address;
    this.addressname = `${this.address.name}, ${this.address.city},  ${this.address.stateCode},  ${this.address.countryCode} - ${this.address.postcode}`;
  }

  cancel() {
    this.deviceOrFacilityCreated.next({ data: null, isForDevice: false, cancel: true });
  }
}
