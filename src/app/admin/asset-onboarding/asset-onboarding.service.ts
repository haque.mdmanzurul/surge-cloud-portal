import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpService } from '../../shared-module';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class AssetOnboardingService {
  public changeStatusSource: Subject<any> = new Subject();
  public changeStatus = this.changeStatusSource.asObservable();
  private _onboardingData: any;
  private _onboardingDataForEdit: any;

  constructor(private httpService: HttpService) { }

  getAssetType(): Observable<any> {
    return this.httpService.get('device/api/iot/getassettype')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  changeWizardStatus(status: Boolean, data: any, index: number) {
    if (status) {
      this.changeStatusSource.next({ data, index });
    } else {
      data.index = index;
      this.changeStatusSource.error(data);
    }
  }

  getTimezone(): Observable<any> {
    return this.httpService.get('common/api/timezone')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  saveFacility(data, isEditMode: Boolean): Observable<any> {
    let url = isEditMode ? 'facility/api/Facility/update' : 'facility/api/Facility/Post';
    return this.httpService.post(url, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getIotHub(): Observable<any> {
    return this.httpService.get('device/api/iot/GetIot')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  createDevice(data, isEditMode: Boolean): Observable<any> {
    let url = isEditMode ? 'device/api/iot/updatedevice' : 'device/api/iot/adddevice';
    return this.httpService.post(url, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getFacility(companyId): Observable<any> {
    return this.httpService.get(`facility/api/facility/get/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getDevice(companyId): Observable<any> {
    return this.httpService.get(`device/api/iot/getdevicebycompany/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  createAsset(data, isEditMode: Boolean): Observable<any> {
    let url = isEditMode ? 'device/api/iot/update' : 'device/api/iot/post';
    return this.httpService.post(url, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  mapDeviceAsset(request): Observable<any> {
    return this.httpService.post('device/api/iot/mapdeviceasset', request)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  mapFacilityAsset(request): Observable<any> {
    return this.httpService.post('device/api/iot/mapdevicefacility', request)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  uploadLogo(logo: File, fileName: string): Observable<any> {
    let formData = new FormData();
    formData.append('', logo);
    formData.append('fname', fileName);
    return this.httpService.saveFile('common/api/image', formData)
      // .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getAsset(companyId: string): Observable<any> {
    return this.httpService.get(`device/api/iot/Get/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getDeviceByAsset(companyId: string, assetId: string) {
    return this.httpService.get(`device/api/Iot/GetByAsset?companyId=${companyId}&assetId=${assetId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  set onboardingData(data: any) {
    this._onboardingData = data;
  }

  get onboardingData() {
    return this._onboardingData;
  }

  set onboardingDataForEdit(data: any) {
    this._onboardingDataForEdit = data;
  }

  get onboardingDataForEdit() {
    return this._onboardingDataForEdit;
  }

  checkAsset(companyId: string, gwSNO: string, assetId: string) {
    return this.httpService.get(`device/api/Iot/CheckExistingDeviceMap?id=${companyId}&gwSNO=${gwSNO}&assetId=${assetId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

}
