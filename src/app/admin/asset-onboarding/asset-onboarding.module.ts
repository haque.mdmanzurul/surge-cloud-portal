import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetOnboardingComponent } from './asset-onboarding.component';
import { CreateAssetComponent } from './create-asset/create-asset.component';
import { CreateFacilityComponent } from './create-facility/create-facility.component';
import { CreateDeviceComponent } from './create-device/create-device.component';
import { FormsModule } from '@angular/forms';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { SharedModule } from '../../shared-module';
import { AssetOnboardingDirective } from './asset-onboarding.directive';
import { AssetOnboardingService } from './asset-onboarding.service';
import { CoreModule } from '../../core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SimpleNotificationsModule,
    SharedModule,
    CoreModule,
    NgbModule
  ],
  declarations: [
    AssetOnboardingComponent,
    CreateAssetComponent,
    CreateFacilityComponent,
    CreateDeviceComponent,
    AssetOnboardingDirective,
    CreateAssetComponent,
    CreateFacilityComponent,
    CreateDeviceComponent
  ],
  entryComponents: [],
  exports: [
    AssetOnboardingComponent
  ],
  providers: [
    AssetOnboardingService
  ]

})
export class AssetOnboardingModule { }
