import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { FormsModule } from '@angular/forms';
import { SubscriptionManagementComponent } from './subscription-management.component';
import { SubscriptionManagementService } from './subscription-management.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { UserModule, UserComponent } from './user';
import { RoleGroupModule, RoleGroupComponent } from './role-group';

@NgModule({
  imports: [
    CommonModule,
    SimpleNotificationsModule,
    FormsModule,
    NgbModule,
    UserModule,
    RoleGroupModule
  ],
  declarations: [
    SubscriptionManagementComponent,
  ],
  exports: [
    SubscriptionManagementComponent
  ],
  providers: [
    SubscriptionManagementService
  ],
  entryComponents : [
    UserComponent,
    RoleGroupComponent
  ]
})

export class SubscriptionManagementModule { }
