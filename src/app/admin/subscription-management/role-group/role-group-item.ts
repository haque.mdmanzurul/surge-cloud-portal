import { Type } from '@angular/core';
import { RoleGroup } from './role-group';
export class RoleGroupItem {
    constructor(public component: Type<any>, public roleGroup: RoleGroup[]) { }
};
