import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appRoleGroup]'
})
export class RoleGroupDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
