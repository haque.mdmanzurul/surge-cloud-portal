import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleGroupImageComponent } from './role-group-image.component';

describe('RoleGroupImageComponent', () => {
  let component: RoleGroupImageComponent;
  let fixture: ComponentFixture<RoleGroupImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleGroupImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleGroupImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
