import { Component, OnInit, Input , EventEmitter} from '@angular/core';
import { RoleGroup } from '../role-group';
import { SubscriptionManagementService } from '../../subscription-management.service';
import { CustomerOnboardingService } from '../../../customer-onboarding';
import { Router } from '@angular/router';
import { User } from '../../user/user';
import { AppService } from '../../../../../app';
import { RoleGroupService } from '../role-group.service';

@Component({
  selector: 'app-role-group-table',
  templateUrl: './role-group-table.component.html',
  styleUrls: ['./role-group-table.component.scss']
})
export class RoleGroupTableComponent implements OnInit {

  @Input() roleGroup: RoleGroup[];  
  
  userView : boolean = false;

  user: User[] = [];

  groupId : string;

  private paginationOrgInfo = {
    roleGroupsortOrder: 'asc', roleGroupsortBy: 'Name',
    numberRowPerPage: 5
  };
  private paginationUserInfo = {
    usersortOrder: 'asc', usersortBy: 'DisplayName',
    numberRowPerPage: 10
  };

  constructor(private subscriptionManagementService: SubscriptionManagementService,
    public customerOnboardingService: CustomerOnboardingService, private router: Router,
    private appService : AppService, private roleGroupService : RoleGroupService
    ) {
    this.paginationOrgInfo.numberRowPerPage = this.subscriptionManagementService.numberOfRecordPerPage;

  }

  ngOnInit() {
    this.groupId = this.appService.getActiveCompany().groupId;
    this.appService.activeCompanyInfo.subscribe((company) => {
      if(company){
        this.userView = false;
        this.groupId = company.groupId;
      }
    });  
  }
  editRole(role) { // redirect to role page
    this.customerOnboardingService.onboardEdit = { organizationId: null, userId: null, for: 3, roleGroup: role };
    this.router.navigate(['/admin/customer-onboarding']);
  }
  gotoUser(roleId:string){
     if(roleId){

       this.userView = true;
       this.roleGroupService.getUser(this.groupId,roleId).subscribe(
       (data) => {
          this.user = data;
      });
     }
     else{
       this.user = [];
     }
  }
}
