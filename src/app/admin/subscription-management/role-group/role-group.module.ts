import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleGroupComponent } from './role-group.component';
import { RoleGroupTableComponent } from './role-group-table/role-group-table.component';
import { RoleGroupImageComponent } from './role-group-image/role-group-image.component';
import { RoleGroupChartComponent } from './role-group-chart/role-group-chart.component';
import { RoleGroupDirective } from './role-group.directive';
import { RoleGroupService } from './role-group.service';
import { DataTableModule } from 'angular2-datatable';
import { CoreModule} from '../../../core';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    CoreModule
  ],
  declarations: [RoleGroupComponent,
   RoleGroupTableComponent,
   RoleGroupImageComponent,
   RoleGroupChartComponent,
   RoleGroupDirective,
   ],
   exports: [RoleGroupComponent,
   RoleGroupTableComponent,
   RoleGroupImageComponent,
   RoleGroupChartComponent
   ],
   providers: [
   RoleGroupService
   ],
   entryComponents: [
   RoleGroupTableComponent,
   RoleGroupImageComponent
   ],
})

export class RoleGroupModule { }
