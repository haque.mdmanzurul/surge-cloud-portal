import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared-module';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RoleGroupService {

  constructor(public httpService: HttpService) { }

  getRoleGroup(data?: string): Observable<any> {

    return this.httpService.get(`users/api/roles/GetRoleUserCount/${data}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getUser(groupId: string,roleId:string): Observable<any> {

    return this.httpService.get(`users/api/roles/getmembers/${groupId}?roleId=${roleId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }
}
