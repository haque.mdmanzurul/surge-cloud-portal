import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-role-group-chart',
	templateUrl: './role-group-chart.component.html',
	styleUrls: ['./role-group-chart.component.scss']
})
export class RoleGroupChartComponent implements OnInit {

	@Input() roleGroupCount: { active: number, inActive: number };

	// single: any[] = [
	// 	{
	// 		"name": "Germany",
	// 		"value": 8940000
	// 	},
	// 	{
	// 		"name": "USA",
	// 		"value": 5000000
	// 	},
	// 	{
	// 		"name": "France",
	// 		"value": 7200000
	// 	}
	// ];
	// multi: any[] = [
	// 	{
	// 		"name": "Germany",
	// 		"series": [
	// 			{
	// 				"name": "2010",
	// 				"value": 7300000
	// 			},
	// 			{
	// 				"name": "2011",
	// 				"value": 8940000
	// 			}
	// 		]
	// 	},

	// 	{
	// 		"name": "USA",
	// 		"series": [
	// 			{
	// 				"name": "2010",
	// 				"value": 7870000
	// 			},
	// 			{
	// 				"name": "2011",
	// 				"value": 8270000
	// 			}
	// 		]
	// 	},

	// 	{
	// 		"name": "France",
	// 		"series": [
	// 			{
	// 				"name": "2010",
	// 				"value": 5000002
	// 			},
	// 			{
	// 				"name": "2011",
	// 				"value": 5800000
	// 			}
	// 		]
	// 	}
	// ];

	// view: any[] = [250, 250];

	// // options
	// showLegend = true;

	// colorScheme = {
	// 	domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
	// };

	// pie
	// showLabels = false;
	// explodeSlices = false;
	// doughnut = false;

	constructor() { }

	ngOnInit() {
	}

	onSelect(event) {
    console.log(event);
  }

}
