import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleGroupChartComponent } from './role-group-chart.component';

describe('RoleGroupChartComponent', () => {
  let component: RoleGroupChartComponent;
  let fixture: ComponentFixture<RoleGroupChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleGroupChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleGroupChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
