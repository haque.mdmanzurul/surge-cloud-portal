import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { RoleGroupService } from './role-group.service';
import { RoleGroup } from './role-group';
import { AppService } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { RoleGroupTableComponent } from './';
import { RoleGroupImageComponent } from './';
import { RoleGroupDirective } from './role-group.directive';
import { RoleGroupItem } from './role-group-item';
import { IWizardItem } from '../../../shared-module';
import { PieChartComponent, SingleChartData } from '../../../core';
import { CustomerOnboardingService } from '../../customer-onboarding';
import { Router } from '@angular/router';

@Component({
  selector: 'app-role-group',
  templateUrl: './role-group.component.html',
  styleUrls: ['./role-group.component.scss']
})

export class RoleGroupComponent implements OnInit {

  roleGroup: RoleGroup[] = [];

  ArrayFilterPipe: ArrayFilterPipe  = new ArrayFilterPipe();

  roleGroupItem: RoleGroupItem[] = [
    new RoleGroupItem(RoleGroupTableComponent, [])];
    // new RoleGroupItem(RoleGroupImageComponent, [])];

  currentIndex = 0;

    public wizardItems: IWizardItem[] = [
    {
      name: 'RoleGroup Table',
      icon: 'glyphicon-th-list',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    },
    {
      name: 'Asset Add',
      icon: 'glyphicon-plus',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    }
  ];
  roleChartData: SingleChartData[] = [];

  @ViewChild(RoleGroupDirective) roleGroupHost: RoleGroupDirective;

  constructor(private roleGroupService: RoleGroupService, public customerOnboardingService: CustomerOnboardingService,
    private componentFactoryResolver: ComponentFactoryResolver, private appService: AppService, private router: Router) { }

  ngOnInit() {
    this.getRoleGroup(this.appService.getActiveCompany().companyId);
    this.appService.activeCompanyInfo.subscribe((company) => {
      if (company) {
        this.getRoleGroup(company.companyId);
      }
    })
  }

  getRoleGroup(companyId: string) {
    this.roleGroupService.getRoleGroup(companyId).subscribe(
      (data) => {

        this.roleGroup = data ? data : [];
       if (this.roleGroup.length > 0 ) {
       this.roleChartData = this.roleGroup.map(role => {
         let newObj = Object.assign({});
         newObj = {'name' : role.Name, 'value': role.Count };
         return newObj;
       });
       // this.roleChartData.push({name:'user',value:4});
       }
        this.loadComponent();
      }
    );
  }

  loadComponent() {
    const boardingItem = this.roleGroupItem[this.currentIndex];
    boardingItem.roleGroup = this.roleGroup;

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.roleGroupHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ roleGroup: any }>componentRef.instance).roleGroup = boardingItem.roleGroup;

  }

  changeComponent(index) {
    // console.log(index);
    if (index === 0) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    } else if (index === 1) { // redirect add role page
      this.customerOnboardingService.onboardNew = { organizationId: null, userId: null, for: 3 , roleGroup: null};
      this.router.navigate(['/admin/customer-onboarding']);
    }
  }

}
