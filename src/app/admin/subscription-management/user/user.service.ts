import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared-module';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

  constructor(public httpService: HttpService) { }

  getUser(data?: string): Observable<any> {

    return this.httpService.get(`users/api/roles/getmembers/${data}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }
}
