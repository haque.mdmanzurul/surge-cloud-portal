import { Component, OnInit, Input } from '@angular/core';
import { User } from '../user';
import { SubscriptionManagementService } from '../../subscription-management.service';
import { CustomerOnboardingService } from '../../../customer-onboarding';
import { Router } from '@angular/router';
import { ArrayFilterPipe } from '../../../../shared-module';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {

  @Input() data: { userList: User[], filterObject: any};
  user: User[];
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  private paginationOrgInfo = {
    usersortOrder: 'asc', usersortBy: 'DisplayName',
    numberRowPerPage: 5
  };

  private statusSvg = ['assets/icons/Management/TenantStatus_inactive.svg#TenantStatus_inactive',
  'assets/icons/Management/TenantStatus_active.svg#TenantStatus_active'];

  constructor(private subscriptionManagementService: SubscriptionManagementService,
    public customerOnboardingService: CustomerOnboardingService, private router: Router) {
    this.paginationOrgInfo.numberRowPerPage = this.subscriptionManagementService.numberOfRecordPerPage;
  }

  ngOnInit() {

    if (this.data.filterObject && this.data.userList.length > 0) {
        let filter;
          if (typeof this.data.userList[0].IsActive === 'number') {
            filter = this.data.filterObject;
          } else {
            filter = {IsActive : this.data.filterObject.IsActive === 0 ? false : true };
          }

          this.user = this.arrayFilterPipe.transform(this.data.userList, filter);
    } else {
      this.user = this.data.userList;
    }

  }

  editUser(user: any) {
    this.customerOnboardingService.onboardEdit = { organizationId: null, userId: user.UserId, for: 2 };
    this.router.navigate(['/admin/customer-onboarding']);
  }
}
