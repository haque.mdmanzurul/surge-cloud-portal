import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { UserService } from './user.service';
import { User } from './user';
import { AppService } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { UserTableComponent } from './';
import { UserImageComponent } from './';
import { UserDirective } from './user.directive';
import { UserItem } from './user-item';
import { IWizardItem } from '../../../shared-module';
import { CustomerOnboardingService } from '../../customer-onboarding';
import { Router } from '@angular/router';
import * as _ from 'underscore';
import { PieChartComponent, SingleChartData } from '../../../core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {

  user: User[] = [];

  ArrayFilterPipe = new ArrayFilterPipe();

  userCount: { active: number, inActive: number } = { active: 0, inActive: 0 };

  userItem: UserItem[] = [
    new UserItem(UserTableComponent, {}),
    new UserItem(UserImageComponent, {})];

  currentIndex = 0;

  public wizardItems: IWizardItem[] = [
    {
      name: 'User Table',
      icon: 'glyphicon-th-list',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    },
    {
      name: 'User Image',
      icon: 'glyphicon-th',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    },
    {
      name: 'Asset Add',
      icon: 'glyphicon-plus',
      disabled: false,
      index: 2,
      success: false,
      error: false,
      active: false
    }
  ];

  @ViewChild(UserDirective) userHost: UserDirective;
  filterObject : any = null;
  private lastUpdated : Date[] = [];
  public chartData: SingleChartData[] = [];
  public filterStatus: boolean = this.filterObject === null ? false : true;
  constructor(private userService: UserService, public customerOnboardingService: CustomerOnboardingService,
    private componentFactoryResolver: ComponentFactoryResolver, private appService: AppService,
    private router: Router) { }

  ngOnInit() {
    this.getUser(this.appService.getActiveCompany().groupId);
    this.appService.activeCompanyInfo.subscribe((company) => {
      if(company){
        this.getUser(company.groupId);
      }
    });  
  }

  getUser(groupId : string){
    this.user = [];

     this.userService.getUser(groupId).subscribe(
      (data) => {
        this.user = data;
        this.chartData = [];
        if (this.user.length > 0) {
          // let dates: Date[] = [];
          // dates = _.map(this.asset, function(i) { return i.lastUpdatedDate });
          // this.lastUpdated = _.sortBy(dates, function(i) { return i });
          let activeCount = (Math.ceil((this.ArrayFilterPipe.transform(this.user, { IsActive: 1 }).length / this.user.length) * 100) +
            (Math.ceil((this.ArrayFilterPipe.transform(this.user, { IsActive: true }).length / this.user.length) * 100)));
          let inactiveCount = (Math.ceil((this.ArrayFilterPipe.transform(this.user, { IsActive: 0 }).length / this.user.length) * 100)) +
            (Math.ceil((this.ArrayFilterPipe.transform(this.user, { IsActive: false }).length / this.user.length) * 100));

          // this.userCount = {
          //   active: activeCount,
          //   inActive: inactiveCount
          // };
          this.chartData.push({ name: 'Active Users', value: activeCount },
          { name: 'Inactive Users', value: inactiveCount });
        }

        this.loadComponent();
      }
    );    
  }

  loadComponent() {
    const boardingItem = this.userItem[this.currentIndex];
    boardingItem.data = {userList : this.user,filterObject:this.filterObject};

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.userHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ data: any }>componentRef.instance).data = boardingItem.data;

  }

  changeComponent(index) {
    console.log(index);
    if (index <= 1) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    }
    else if (index == 2) { // redirect to create user page
      this.customerOnboardingService.onboardNew = { organizationId: null, userId: null, for: 2 , roleGroup: null};
      this.router.navigate(['/admin/customer-onboarding']);
    }

  }
  filterInfo(data){
    // this.filterObject = {IsActive : $event};
    if (data) {
      let status: number = data.name === 'Active Users' ? 1 : 0;
      this.filterObject = { IsActive: status };
    } else {
      this.filterObject = null;
    }
    this.filterStatus = this.filterObject === null ? false : true;
    this.loadComponent();
  }

}
