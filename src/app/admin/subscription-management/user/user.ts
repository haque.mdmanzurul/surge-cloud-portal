export class User {
    public Id: string;
    public emailId: string;
    public password: string;
    public displayName: string;
    public givenName: string;
    public surname: string;
    public jobTitle: string;
    public companyId: string;
    public companyName: string;
    public facilityId: string;
    public facilityName: string;
    public drivingLicenseNumber: string;
    public driverId: string;
    public expirationDate: any;
    public landingPage: string;
    public logo: string;
    public IsActive: Boolean | number;
    public roleGroup: string;
    public phoneNumber: string;
    constructor(public data: any = {}) {
        this.Id = data.UserId || null;
        this.emailId = data.EmailId || '';
        this.password = data.Password || '';
        this.displayName = data.DisplayName || '';
        this.givenName = data.GivenName || '';
        this.surname = data.Surname || '';
        this.jobTitle = data.JobTitle || '';
        this.companyId = data.CompanyId || null;
        this.companyName = data.CompanyName || '';
        this.facilityId = data.FacilityId || null;
        this.facilityName = data.FacilityName || '';
        this.drivingLicenseNumber = data.DrivingLicenceNumber || '';
        this.driverId = data.DriverId || null;
        this.expirationDate = data.ExpirationDate || '';
        this.landingPage = data.LandingPage || '';
        this.logo = data.logo || '';
        this.IsActive =  data.IsActive || 0;
        this.roleGroup = data.roleGroup || '';
        this.phoneNumber = data.PhoneNumber || '';
    }
}
