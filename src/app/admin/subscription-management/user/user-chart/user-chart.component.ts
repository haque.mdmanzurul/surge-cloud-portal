import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-user-chart',
  templateUrl: './user-chart.component.html',
  styleUrls: ['./user-chart.component.scss']
})
export class UserChartComponent implements OnInit {

  @Input() userCount: {active: number, inActive: number};
  @Output() clickedChartInfo : EventEmitter<number> = new EventEmitter<number>();
  activeStatus : {active:boolean, inActive:boolean}= {active : false, inActive : false};
  constructor() { }

  ngOnInit() {
  }

  filterData(status:number){
  	this.activeStatus = status ===1 ? {active : true, inActive : false} : {active : false, inActive : true};
  	this.clickedChartInfo.next(status);
  }

}
