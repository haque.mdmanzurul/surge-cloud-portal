import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { UserTableComponent } from './user-table/user-table.component';
import { UserImageComponent } from './user-image/user-image.component';
import { UserChartComponent } from './user-chart/user-chart.component';
import { UserDirective } from './user.directive';
import { UserService } from './user.service';
import { DataTableModule } from 'angular2-datatable';
import { CoreModule} from '../../../core';
@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    CoreModule
  ],
  declarations: [UserComponent,
   UserTableComponent,
   UserImageComponent,
   UserChartComponent,
   UserDirective
   ],
   exports: [UserComponent,
   UserTableComponent,
   UserImageComponent,
   UserChartComponent
   ],
   providers: [
   UserService
   ],
   entryComponents: [
   UserTableComponent,
   UserImageComponent
   ],
})
export class UserModule { }
