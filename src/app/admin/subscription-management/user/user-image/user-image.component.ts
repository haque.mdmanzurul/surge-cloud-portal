import { Component, OnInit, Input } from '@angular/core';
import { User } from '../user';
import { CustomerOnboardingService } from '../../../customer-onboarding';
import { Router } from '@angular/router';
import { ArrayFilterPipe } from '../../../../shared-module';
@Component({
  selector: 'app-user-image',
  templateUrl: './user-image.component.html',
  styleUrls: ['./user-image.component.scss']
})
export class UserImageComponent implements OnInit {

  @Input() data: {userList: User[], filterObject: any};

  user: User[];
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
  constructor(public customerOnboardingService: CustomerOnboardingService, public router: Router) { }

  ngOnInit() {
    if (this.data.filterObject && this.data.userList.length > 0) {
        let filter;
          if (typeof this.data.userList[0].IsActive === 'number') {
            filter = this.data.filterObject;
          } else {
            filter = {IsActive : this.data.filterObject.IsActive === 0 ? false : true };
          }
          this.user = this.arrayFilterPipe.transform(this.data.userList, filter);
    } else {
      this.user = this.data.userList;
    }
  }

  editUser(user: any) {
    this.customerOnboardingService.onboardEdit = { organizationId: null, userId: user.UserId, for: 2 };
    this.router.navigate(['/admin/customer-onboarding']);
  }

}
