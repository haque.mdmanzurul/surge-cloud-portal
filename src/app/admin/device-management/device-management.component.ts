import { Component, OnInit, ViewChild } from '@angular/core';
import { IWizardItem } from '../../shared-module';
import { AssetOnboardingService } from '../asset-onboarding';



@Component({
  selector: 'app-device-management',
  templateUrl: './device-management.component.html',
  styleUrls: ['./device-management.component.scss']
})
export class DeviceManagementComponent implements OnInit {
  public wizardItems: IWizardItem[] = [
    {
      name: 'Device Overview',
      icon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Device.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/AssetOnboarding/icon_Assetonboarding_Device_White.svg#Vector_Smart_Object',
      success: false,
      active: true,
      statusIcon: ''
    },
    {
      name: 'IotHub Overview',
      icon: 'assets/icons/AssetOnboarding/IOT_HUB.svg#IOT_HUB',
      activeIcon: 'assets/icons/AssetOnboarding/IOT_HUB_White.svg#IOT_HUB_white',
      success: false,
      active: false,
      statusIcon: ''
    }

  ];
  currentIndex = 0;
  
  constructor(public onboardingService:AssetOnboardingService) { }

  ngOnInit() {
    this.onboardingService.onboardingDataForEdit = { index: null, device: null };    
  }

  beforeChange($event) {
    // console.log($event);
    switch ($event.nextId) {
      case 'device_overview':
        this.wizardItems[this.currentIndex].active = false;
        this.wizardItems[0].active = true;
        this.currentIndex = 0;
        break;
      case 'iothub_overview':
        this.wizardItems[this.currentIndex].active = false;
        this.wizardItems[1].active = true;
        this.currentIndex = 1;
        break;
    }
  }

}
