import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeviceComponent } from './device.component';
import { DeviceTableComponent } from './device-table/device-table.component';
import { DeviceImageComponent } from './device-image/device-image.component';
import { DeviceChartComponent } from './device-chart/device-chart.component';
import { DeviceDirective } from './device.directive';
import { DeviceService } from './device.service';
import { DataTableModule } from 'angular2-datatable';
import { CoreModule } from '../../../core';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    CoreModule
  ],
  declarations: [DeviceComponent,
   DeviceTableComponent,
   DeviceImageComponent,
   DeviceChartComponent,
   DeviceDirective
   ],
   exports: [DeviceComponent,
   DeviceTableComponent,
   DeviceImageComponent,
   DeviceChartComponent
   ],
   providers: [
   DeviceService
   ],
   entryComponents: [
   DeviceTableComponent,
   DeviceImageComponent
   ],
})

export class DeviceModule { }
