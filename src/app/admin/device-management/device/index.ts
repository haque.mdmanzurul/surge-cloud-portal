export * from './device.module';
export * from './device-table/device-table.component';
export * from './device-image/device-image.component';
export * from './device.component';
