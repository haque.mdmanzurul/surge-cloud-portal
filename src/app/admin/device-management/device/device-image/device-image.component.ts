import { Component, OnInit, Input } from '@angular/core';
import { Device } from '../device';
import { Router } from '@angular/router';
import { AssetOnboardingService} from '../../../asset-onboarding';
import { ArrayFilterPipe } from '../../../../shared-module';
@Component({
  selector: 'app-device-image',
  templateUrl: './device-image.component.html',
  styleUrls: ['./device-image.component.scss']
})
export class DeviceImageComponent implements OnInit {

  @Input() data: { deviceList: Device[], filterObject: any};

  device: Device[];
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();

  constructor(private router: Router,
    private onboardingSerive: AssetOnboardingService) { }

  ngOnInit() {
    if (this.data.filterObject && this.data.deviceList.length > 0) {
        let filter;
          if (typeof this.data.deviceList[0].Status === 'number') {
            filter = this.data.filterObject;
          } else {
            filter = {status : this.data.filterObject.Status === 0 ? false : true };
          }
          this.device = this.arrayFilterPipe.transform(this.data.deviceList, filter);
    } else {
      this.device = this.data.deviceList;
    }

  }

  editDevice(device: any) {
    this.onboardingSerive.onboardingDataForEdit = { index: 2, device: device };
    this.router.navigate(['admin/asset-onboarding'])
  }
}
