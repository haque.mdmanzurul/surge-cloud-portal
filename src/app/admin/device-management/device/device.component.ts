import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { DeviceService } from './device.service';
import { Device } from './device';
import { AppService } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { DeviceTableComponent } from './';
import { DeviceImageComponent } from './';
import { DeviceDirective } from './device.directive';
import { DeviceItem } from './device-item';
import { IWizardItem } from '../../../shared-module';
import { AssetOnboardingService } from '../../asset-onboarding';
import { Router } from '@angular/router';
import * as _ from 'underscore';
import { PieChartComponent, SingleChartData } from '../../../core';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss']
})

export class DeviceComponent implements OnInit {

  device: Device[] = [];

  ArrayFilterPipe = new ArrayFilterPipe();

  deviceCount: { active: number, inActive: number } = { active: 0, inActive: 0 };

  deviceItem: DeviceItem[] = [
    new DeviceItem(DeviceTableComponent, {}),
    new DeviceItem(DeviceImageComponent, {})];

  currentIndex = 0;

  public wizardItems: IWizardItem[] = [
    {
      name: 'Device Table',
      icon: 'glyphicon-th-list',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    },
    {
      name: 'Device Image',
      icon: 'glyphicon-th',
      disabled: false,
      index: 1,
      success: false,
      error: false,
      active: false
    },
    {
      name: 'Device Add',
      icon: 'glyphicon-plus',
      disabled: false,
      index: 2,
      success: false,
      error: false,
      active: false
    }
  ];
  filterObject: any = null;
  @ViewChild(DeviceDirective) deviceHost: DeviceDirective;
  chartData: SingleChartData[] = [];
  filterStatus: boolean = this.filterObject === null ? false : true;
  lastUpdated: Date[] = [];
  constructor(private deviceService: DeviceService, public onboardingService: AssetOnboardingService,
    private componentFactoryResolver: ComponentFactoryResolver, private appService: AppService,
    public router: Router) {
  }

  ngOnInit() {
    this.getDevice(this.appService.getActiveCompany().companyId);
    this.appService.activeCompanyInfo.subscribe((company) => {
      if (company) {
        this.getDevice(company.companyId);
      }
    });

  }
  getDevice(companyId: string) {
    this.deviceService.getDevice(companyId).subscribe(
      (data) => {
        this.device = [];
        this.chartData = [];  
        if (data) {
          data.forEach((dev) => {
            this.device.push(new Device(dev));
          })
        }
        
        if (this.device.length > 0) {
          let dates: Date[] = [];
      dates = _.map(this.device, function(i) { return i.lastActivetime });
      this.lastUpdated = _.sortBy(dates, function(i) { return i });
          let activeCount = Math.ceil((this.ArrayFilterPipe.transform(this.device, { Status: 1 }).length / this.device.length) * 100) +
            Math.ceil((this.ArrayFilterPipe.transform(this.device, { Status: true }).length / this.device.length) * 100);
          let inactiveCount = Math.ceil((this.ArrayFilterPipe.transform(this.device, { Status: 0 }).length / this.device.length) * 100) +
            Math.ceil((this.ArrayFilterPipe.transform(this.device, { Status: false }).length / this.device.length) * 100);
          this.deviceCount = {
            active: activeCount,
            inActive: inactiveCount
          };
          this.chartData.push({ name: 'Active Devices', value: activeCount },
        { name: 'Inactive Devices', value: inactiveCount });
        }
        this.loadComponent();
      }
    );
  }

  loadComponent() {
    const boardingItem = this.deviceItem[this.currentIndex];
    boardingItem.data = { deviceList: this.device, filterObject: this.filterObject };

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.deviceHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ data: any }>componentRef.instance).data = boardingItem.data;

  }

  changeComponent(index) {
    console.log(index);
    if (index <= 1) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    }

    if (index == 2) {
      this.onboardingService.onboardingDataForEdit = { index: 2, device: null };
      this.router.navigate(['/admin/asset-onboarding']);
    }

  }
  filterInfo(data) {
    if (data) {
      let status: number = data.name === 'Active Devices' ? 1 : 0;
      this.filterObject = { Status: status };
    } else {
      this.filterObject = null;
    }
    this.filterStatus = this.filterObject === null ? false : true;
    this.loadComponent();
  }

}
