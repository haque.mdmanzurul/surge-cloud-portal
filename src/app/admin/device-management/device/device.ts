export class Device {
    public gwSNO: string;
    public AssetId: string;
    public provitionedDate: Date;
    public lastActivetime: Date;
    public Status: number | boolean;
    public gwWANIP: string;
    public gwRSSI: number;
    public AssetLastUpdateDate: Date;
    public LatestSyncDate: Date;
    public Logo: number;
    constructor(private d: any = {}) {
        this.gwSNO = d.gwSNO || '';
        this.AssetId = d.AssetId || '';
        this.provitionedDate = d.provitionedDate ? new Date(d.provitionedDate) : null;
        this.lastActivetime = d.lastActivetime ? new Date(d.lastActivetime) : null;
        this.LatestSyncDate = d.LatestSyncDate ? new Date(d.LatestSyncDate) : null;
        this.AssetLastUpdateDate = d.AssetLastUpdateDate ? new Date(d.AssetLastUpdateDate) : null;
        this.Status = d.Status || 0;
        this.gwWANIP = d.gwWANIP || '';
        this.gwRSSI = d.gwRSSI || '';
        this.Logo = d.Logo;
    }
}
