import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-device-chart',
  templateUrl: './device-chart.component.html',
  styleUrls: ['./device-chart.component.scss']
})
export class DeviceChartComponent implements OnInit {

  @Input() deviceCount: { active: number, inActive: number };
  @Output() clickedChartInfo: EventEmitter<number> = new EventEmitter<number>();
  activeStatus: { active: boolean, inActive: boolean } = { active: false, inActive: false };
  constructor() { }

  ngOnInit() {
  }
  filterData(status: number) {
    this.activeStatus = status ===1 ? {active : true, inActive : false} : {active : false, inActive : true};
    this.clickedChartInfo.next(status);
  }

}
