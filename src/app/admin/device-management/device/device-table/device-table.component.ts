import { Component, OnInit, Input } from '@angular/core';
import { Device } from '../device';
import { DeviceManagementService } from '../../device-management.service';
import { Router } from '@angular/router';
import { AssetOnboardingService} from '../../../asset-onboarding';
import { ArrayFilterPipe } from '../../../../shared-module';

@Component({
  selector: 'app-device-table',
  templateUrl: './device-table.component.html',
  styleUrls: ['./device-table.component.scss']
})
export class DeviceTableComponent implements OnInit {

  @Input() data :{ deviceList: Device[], filterObject: any};

  device: Device[];

  private paginationOrgInfo = { devicesortOrder: 'asc', devicesortBy : 'gwSNO',
            numberRowPerPage : 5};
  private statusSvg = ["assets/icons/Management/TenantStatus_inactive.svg#TenantStatus_inactive",
  "assets/icons/Management/TenantStatus_active.svg#TenantStatus_active"];
  private arrayFilterPipe: ArrayFilterPipe = new ArrayFilterPipe();
               
    constructor(private deviceManagementService: DeviceManagementService, private router: Router,
    private onboardingSerive: AssetOnboardingService) {
    this.paginationOrgInfo.numberRowPerPage = this.deviceManagementService.numberOfRecordPerPage;
  }

  ngOnInit() {
     if (this.data.filterObject && this.data.deviceList.length >0) {
        let filter;
          if (typeof this.data.deviceList[0].Status === "number") {
            filter = this.data.filterObject;
          }else{
            filter = {status : this.data.filterObject.Status === 0 ? false : true };
          }
          this.device = this.arrayFilterPipe.transform(this.data.deviceList,filter);
    }
    else {
      this.device = this.data.deviceList;
    }


  }

  editDevice(device: any){
    this.onboardingSerive.onboardingDataForEdit = {index: 2, device: device};
    this.router.navigate(['admin/asset-onboarding'])
  }
}
