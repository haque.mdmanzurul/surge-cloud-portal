import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appDevice]'
})
export class DeviceDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
