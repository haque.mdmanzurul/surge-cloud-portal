import { Component, OnInit, Input } from '@angular/core';
import { IotHub } from '../iothub';

@Component({
  selector: 'app-iothub-image',
  templateUrl: './iothub-image.component.html',
  styleUrls: ['./iothub-image.component.scss']
})
export class IotHubImageComponent implements OnInit {

  @Input() iothub: IotHub[];

  constructor() { }

  ngOnInit() {
  }

}
