import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IotHubImageComponent } from './iothub-image.component';

describe('IotHubImageComponent', () => {
  let component: IotHubImageComponent;
  let fixture: ComponentFixture<IotHubImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IotHubImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IotHubImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
