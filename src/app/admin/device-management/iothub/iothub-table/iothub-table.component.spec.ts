import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IotHubTableComponent } from './iothub-table.component';

describe('IotHubTableComponent', () => {
  let component: IotHubTableComponent;
  let fixture: ComponentFixture<IotHubTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IotHubTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IotHubTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
