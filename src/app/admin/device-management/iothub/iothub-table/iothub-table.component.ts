import { Component, OnInit, Input } from '@angular/core';
import { IotHub } from '../iothub';
import { DeviceManagementService } from '../../device-management.service';
import { IotHubService } from '../iothub.service';
import { Device } from '../../device/device';
import { AppService } from '../../../../../app';

@Component({
  selector: 'app-iothub-table',
  templateUrl: './iothub-table.component.html',
  styleUrls: ['./iothub-table.component.scss']
})
export class IotHubTableComponent implements OnInit {

  @Input() iothub: IotHub[];

  private paginationOrgInfo = { iothubsortOrder: 'asc', iothubsortBy : 'IotHubName',
            numberRowPerPage : 5};
  deviceView : boolean = false;
  device: Device[] =[];
  companyId : string;
  private paginationDevInfo = { devicesortOrder: 'asc', devicesortBy : 'gwSNO',
            numberRowPerPage : 10};
    constructor(private deviceManagementService: DeviceManagementService,
      private appService : AppService, private iotHubService : IotHubService) {
    this.paginationOrgInfo.numberRowPerPage = this.deviceManagementService.numberOfRecordPerPage;
  }

  ngOnInit() {
    this.companyId = this.appService.getActiveCompany().companyId;
    this.appService.activeCompanyInfo.subscribe((company)=>{
      if(company){
        this.companyId = company.companyId;
        this.deviceView = false;
      }
    });

  }

  showDevice(iothubname:string){
    if(iothubname){
      this.deviceView = true;
      this.iotHubService.getDevice(this.companyId,iothubname).subscribe(
      (data) => {
        this.device = [];
        if(data){
          data.forEach((dev)=>{
            this.device.push(new Device(dev));
          })
        }
      });
    }
    else{
      this.device = [];
    }
  }


}
