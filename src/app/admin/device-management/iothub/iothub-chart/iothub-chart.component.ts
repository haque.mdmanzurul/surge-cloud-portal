import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-iothub-chart',
  templateUrl: './iothub-chart.component.html',
  styleUrls: ['./iothub-chart.component.scss']
})
export class IotHubChartComponent implements OnInit {

  @Input() iothubCount: {active: number, inActive: number};

  constructor() { }

  ngOnInit() {
  }

}
