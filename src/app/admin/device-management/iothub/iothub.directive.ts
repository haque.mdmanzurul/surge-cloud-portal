import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appIotHub]'
})
export class IotHubDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
