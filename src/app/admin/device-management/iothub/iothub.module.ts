import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IotHubComponent } from './iothub.component';
import { IotHubTableComponent } from './iothub-table/iothub-table.component';
import { IotHubImageComponent } from './iothub-image/iothub-image.component';
import { IotHubChartComponent } from './iothub-chart/iothub-chart.component';
import { IotHubDirective } from './iothub.directive';
import { IotHubService } from './iothub.service';
import { DataTableModule } from 'angular2-datatable';
import { CoreModule} from '../../../core';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    CoreModule
  ],
  declarations: [IotHubComponent,
   IotHubTableComponent,
   IotHubImageComponent,
   IotHubChartComponent,
   IotHubDirective
   ],
   exports: [IotHubComponent,
   IotHubTableComponent,
   IotHubImageComponent,
   IotHubChartComponent
   ],
   providers: [
   IotHubService
   ],
   entryComponents: [
   IotHubTableComponent,
   IotHubImageComponent
   ],
})

export class IotHubModule { }
