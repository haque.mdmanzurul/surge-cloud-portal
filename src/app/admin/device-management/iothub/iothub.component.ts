import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { IotHubService } from './iothub.service';
import { IotHub } from './iothub';
import { AppService } from '../../../../app';
import { ArrayFilterPipe } from '../../../shared-module';
import { IotHubTableComponent } from './';
import { IotHubImageComponent } from './';
import { IotHubDirective } from './iothub.directive';
import { IotHubItem } from './iothub-item';
import { IWizardItem } from '../../../shared-module';
import { PieChartComponent, SingleChartData } from '../../../core';

@Component({
  selector: 'app-iothub',
  templateUrl: './iothub.component.html',
  styleUrls: ['./iothub.component.scss']
})

export class IotHubComponent implements OnInit {

  iothub: IotHub[];

  ArrayFilterPipe = new ArrayFilterPipe();

  iothubItem: IotHubItem[] = [
    new IotHubItem(IotHubTableComponent, []),
    new IotHubItem(IotHubImageComponent, [])];

  currentIndex = 0;

    public wizardItems: IWizardItem[] = [
    {
      name: 'IotHub Table',
      icon: 'glyphicon-th-list',
      disabled: false,
      index: 0,
      success: false,
      error: false,
      active: true
    }
  ];
  iotHubChartData: SingleChartData[] = [];

  @ViewChild(IotHubDirective) iothubHost: IotHubDirective;

  constructor(private iothubService: IotHubService,
    private componentFactoryResolver: ComponentFactoryResolver, private appService: AppService) { }

  ngOnInit() {
    this.getIotHub(this.appService.getActiveCompany().companyId);
    this.appService.activeCompanyInfo.subscribe((company)=>{
      if(company){
        this.getIotHub(company.companyId);
      }
    })
  }

  getIotHub(companyId : string){
    this.iothubService.getIotHub(companyId).subscribe(
      (data) => {
        this.iothub = data ? data : [];
       this.iotHubChartData = this.iothub.map(hub => {
         let newObj = Object.assign({});
         newObj = {'name' : hub.IotHubName, 'value': hub.DeviceCount };
         return newObj;
       })
        this.loadComponent();
      }
    );
  }
  loadComponent() {
    const boardingItem = this.iothubItem[this.currentIndex];
    boardingItem.iothub = this.iothub;

    const componentFactory = this.componentFactoryResolver
      .resolveComponentFactory(boardingItem.component);

    const viewContainerRef = this.iothubHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<{ iothub: any }>componentRef.instance).iothub = boardingItem.iothub;

  }

  changeComponent(index) {
    console.log(index);
    if (index <= 1) {
      this.wizardItems[this.currentIndex].active = false;
      this.currentIndex = index;
      this.wizardItems[this.currentIndex].active = true;
      this.loadComponent();
    }

  }

}
