import { Type } from '@angular/core';
import { IotHub } from './iothub';
export class IotHubItem {
    constructor(public component: Type<any>, public iothub: IotHub[]) { }
};
