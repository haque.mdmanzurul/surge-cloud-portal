import { Injectable } from '@angular/core';
import { HttpService } from '../../../shared-module';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class IotHubService {

  constructor(public httpService: HttpService) { }

  getIotHub(data?: string): Observable<any> {

    return this.httpService.get(`device/api/iot/GetDeviceCountByCompany/${data}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getDevice(companyId: string, iothubname: string): Observable<any> {

    return this.httpService.get(`device/api/iot/getdevicebycompany/${companyId}?iothubname=${iothubname}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }
}
