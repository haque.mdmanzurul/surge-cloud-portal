import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Organization, RoleGroup } from '../../app-common';
import { HttpService } from '../../shared-module';
import { CustomerOnboardingStatus } from './customer-onboarding';


@Injectable()
export class CustomerOnboardingService {

  public changeStatusSource: Subject<any> = new Subject();
  public changeStatus = this.changeStatusSource.asObservable();
  private _organization: Organization;
  private _roleGroupSource: Subject<RoleGroup> = new Subject();
  public roleGroup = this._roleGroupSource.asObservable();
  private _roleGroup: RoleGroup;
  private _onboardEdit: any = { organizationId: null, userId: null, for: null, roleGroup: null };

  private _onboardNew: any = { organizationId: null, userId: null, for: null, roleGroup: null };

  private _onboardingData: CustomerOnboardingStatus = new CustomerOnboardingStatus();

  constructor(public httpService: HttpService) { }


  changeWizardStatus(status: Boolean, data: any, index: number) {
    if (status) {
      this.changeStatusSource.next({ data, index });
    } else {
      data.index = index;
      this.changeStatusSource.error(data);
    }
  }

  set onboardingData(data: CustomerOnboardingStatus) {
    this._onboardingData = data;
  }

  get onboardingData(): CustomerOnboardingStatus {
    return this._onboardingData;
  }

  setRoleGroup(data: RoleGroup) {
    this._roleGroup = data;
    this._roleGroupSource.next(data);
    // this._roleGroup = data;
  }

  getSavedRoleGroup(): RoleGroup {
    return this._roleGroup;
  }

  set onboardEdit(data: any) {
    this._onboardEdit = data;
    this._onboardNew = { organizationId: null, userId: null, for: null, roleGroup: null };
  }

  get onboardEdit(): any {
    return this._onboardEdit;
  }

  set onboardNew(data: any) {
    this._onboardNew = data;
    this._onboardEdit = { organizationId: null, userId: null, for: null, roleGroup: null };
  }

  get onboardNew(): any {
    return this._onboardNew;
  }

  getOrganization(): Observable<any> {
    return this.httpService.get('company/api/company/getbasic')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getCurrency(): Observable<any> {
    return this.httpService.get('common/api/currency')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  saveOrganization(data: any, forUpate: Boolean): Observable<any> {
    let url = forUpate ? 'company/api/Company/Update' : 'company/api/Company/Post';
    return this.httpService.post(url, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  uploadLogo(logo: File, fileName: string): Observable<any> {
    let formData = new FormData();
    formData.append('', logo);
    formData.append('fname', fileName);
    return this.httpService.saveFile('common/api/image', formData)
      // .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  createRoleGroup(displayName: string) {
    return this.httpService.post('users/api/roles/creategroup', { displayName: displayName })
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  saveRoleGroup(data: any, isEdit?: Boolean): Observable<any> {
    let url = isEdit ? 'users/api/Roles/UpdateRole' : 'users/api/Roles/CreateRole';
    return this.httpService.post(url, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  MapRolesFeatures(data: any): Observable<any> {
    return this.httpService.post('users/api/Roles/MapRolesFeatures', data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  saveUSer(data: any, forUpdate): Observable<any> {
    let url = forUpdate ? 'users/api/Users/UpdateUser' : 'users/api/Users/CreateUser';
    return this.httpService.post(url, data)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getRoleGroup(data?: string): Observable<any> {
    return this.httpService.get(`users/api/roles/getroles/${data}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  addUserToGroup(userId: string, groupId: string): Observable<any> {
    return this.httpService.post('users/api/Roles/AddUserToGroup', { userId: userId, groupId: groupId })
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  updateRoleUserMap(userId: string, roleId: string): Observable<any> {
    return this.httpService.post('users/api/roles/updateroleusermap', { UserId: userId, RoleId: roleId })
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  uploadLogoForUser(logo: File, fileName: string, UserId: string): Observable<any> {
    let formData = new FormData();
    formData.append('Id', UserId);
    formData.append('Photo', fileName);
    return this.httpService.saveFile('users/api/users/uploadphoto', formData)
      // .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getGetMembers(groupId: string): Observable<any> {
    return this.httpService.get(`users/api/Roles/GetMembers/${groupId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  GetRoleUserMapping(userId: string): Observable<any> {
    return this.httpService.get(`users/api/Roles/GetRoleUserMapping/${userId}`)
      .map(res => res.json() as { RoleId: string, UserId: string }[])
      .catch(err => Observable.throw(err));
  }

  GetRoleFeature(roleId: string) {
    return this.httpService.get(`users/api/Roles/GetRoleFeatures/${roleId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }


}
