import { TestBed, inject } from '@angular/core/testing';

import { CustomerOnboardingService } from './customer-onboarding.service';

describe('CustomerOnboardingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerOnboardingService]
    });
  });

  it('should ...', inject([CustomerOnboardingService], (service: CustomerOnboardingService) => {
    expect(service).toBeTruthy();
  }));
});
