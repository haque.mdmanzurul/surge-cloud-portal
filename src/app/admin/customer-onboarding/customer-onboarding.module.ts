import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { OrganizationModule } from './organization';
// import { RoleGroupsModule } from './role-groups';
import { CoreModule } from '../../core';
import { SharedModule } from '../../shared-module';

import { CustomerOnboardingComponent } from './customer-onboarding.component';
// import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CustomerOnboardingDirective } from './customer-onboarding.directive';
import { CustomerOnboardingService } from './customer-onboarding.service';
import { MyDatePickerModule } from 'mydatepicker';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CreateOrganizationComponent } from './create-organization';
import { CreateRoleGroupComponent } from './create-role-group';
import { CreateUserComponent } from './create-user';


@NgModule({
  imports: [
    CommonModule,
    // MaterialModule,
    FormsModule,
    SharedModule,
    MyDatePickerModule,
    SimpleNotificationsModule,
    NgbModule,
    CoreModule
  ],
  declarations: [
    CustomerOnboardingComponent, 
    CustomerOnboardingDirective,
    CreateOrganizationComponent,
    CreateRoleGroupComponent,
    CreateUserComponent
  ],
  exports: [CustomerOnboardingComponent],
  providers: [CustomerOnboardingService]
})
export class CustomerOnboardingModule { }
