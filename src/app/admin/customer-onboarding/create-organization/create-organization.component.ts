import { Component, OnInit, ViewChild, AfterViewChecked, ElementRef, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Organization, IAddress, Currency, RoleGroup } from '../../../app-common';
import { Company } from '../../../app';
import { CustomerOnboardingStatus } from '../customer-onboarding';
import { CustomerOnboardingService } from '../customer-onboarding.service';
import { MapsAPILoader } from '@agm/core';
import { Subject } from 'rxjs/Subject';
import { AppService } from '../../../app.service';
import * as _ from 'underscore';

declare const google: any;
@Component({
  selector: 'app-create-organization',
  templateUrl: './create-organization.component.html',
  styleUrls: ['./create-organization.component.scss']
})
export class CreateOrganizationComponent implements OnInit {
  public organization: Organization = new Organization();
  public selectedOrganization: Organization;
  public availableOrganization: Array<{ companyId: string, name: string }> = [];
  public address: IAddress;
  public availableCurrencies: any[] = [];
  public phoneNumber: string[] = [''];
  public logoFile: File;
  public isEditMode: Boolean = false;
  public addressname: string = '';

  @Output() public status: EventEmitter<CustomerOnboardingStatus> = new EventEmitter();
  public notifiyStatus: CustomerOnboardingStatus;
  public organizationIndex: number;



  constructor(private mapsAPILoader: MapsAPILoader,
    private elementRef: ElementRef, private router: Router,
    private onboardingService: CustomerOnboardingService, private appService: AppService) {
  }

  ngOnInit() {
    this.availableOrganization = this.appService.companies;
    this.availableOrganization = _.sortBy(this.availableOrganization, 'name');

    if (this.onboardingService.onboardingData && this.onboardingService.onboardingData.organization) {
      this.isEditMode = true;
      const selectIndex = _.indexOf(this.availableOrganization, _.findWhere(this.availableOrganization, { companyId: this.onboardingService.onboardingData.organization.companyId }));
      this.organizationIndex = selectIndex;
      this.getCompanySelected(selectIndex);
    }

    this.getCurrency();
    this.mapsAPILoader.load().then(() => {
      const autocomplete: any = new google.maps.places.Autocomplete(this.elementRef.nativeElement.querySelector('input[name=addressName]'), {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        const place: any = autocomplete.getPlace();
        if (place.geometry === undefined || place.geometry === null) {
          return;
        }
        this.setAddress(place);
      });
    });

    if (this.onboardingService.onboardEdit && this.onboardingService.onboardEdit.organizationId) {
      const selectIndex = _.indexOf(this.availableOrganization, _.findWhere(this.availableOrganization, { companyId: this.onboardingService.onboardEdit.organizationId }));
      this.organizationIndex = selectIndex;
      this.getCompanySelected(selectIndex);
      this.isEditMode = true;
    }
  }

  changeImage(image: { file, data }) {
    const pattern = new RegExp("image/");
    if (image.file && pattern.test(image.file.type)) {
      this.organization.logo = image.data;
      this.logoFile = image.file;
    }
  }

  getCompany() {
    this.onboardingService.getOrganization().subscribe(
      (data) => {
        this.availableOrganization = [];
        for (let org of data) {
          this.availableOrganization.push({ companyId: org.companyId, name: org.name });
        }
        this.availableOrganization = _.sortBy(this.availableOrganization, 'name');
      }
    );
  }

  getCurrency() {
    this.onboardingService.getCurrency().subscribe(
      (data) => {
        for (let curr of data) {
          this.availableCurrencies.push(new Currency(curr));
        }
      }
    )
  }

  setAddress(place: any) {
    let street = this.getAddrComponent(place, { street_number: 'short_name' });
    if (street) {
      street = `, ${this.getAddrComponent(place, { route: 'long_name' })}`;
    } else {
      street = this.getAddrComponent(place, { route: 'long_name' });
    }

    this.address = {
      name: place.name,
      street1: street,
      street2: this.getAddrComponent(place, { neighborhood: "long_name" }),
      city: this.getAddrComponent(place, { locality: 'long_name' }),
      stateCode: this.getAddrComponent(place, { administrative_area_level_1: 'long_name' }),
      postcode: this.getAddrComponent(place, { postal_code: 'long_name' }),
      countryCode: this.getAddrComponent(place, { country: 'short_name' }),
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng()
    };
  }

  getAddrComponent(place: any, componentTemplate: any) {
    for (let i = 0; i < place.address_components.length; i++) {
      const addressType = place.address_components[i].types[0];
      if (componentTemplate[addressType]) {
        return place.address_components[i][componentTemplate[addressType]];
      }
    }
    return '';
  }

  saveOrganization(isValid: Boolean) {
    if (isValid && this.address) {
      let logoName = '';
      let splittedName = (this.organization.name.replace(/[^\w\s]/gi, '')).split(' ');
      let randomNumber: any = Math.random().toFixed(2);
      splittedName.push((randomNumber * 100).toString());

      if (this.logoFile) {
        logoName = `${splittedName.join('_')}.${this.logoFile.name.split('.').pop()}`;
      }

      if (this.organization.website) {
        if (!/^(f|ht)tps?:\/\//i.test(this.organization.website)) {
          this.organization.website = `http://${this.organization.website}`;
        }
      }

      let organization = {
        CompanyId: this.organization.companyId,
        Name: this.organization.name,
        ParentCompany: this.organization.parentCompany,
        Address: this.address,
        Contact: {
          ContactName: this.organization.contact.contactName,
          ContactPhoneNumber: this.organization.contact.contactPhoneNumber,
          ContactMobileNumber: this.organization.contact.contactMobileNumber,
          ContactEmailId: this.organization.contact.contactEmailId,
        },
        EmailId: this.organization.emailId,
        Website: this.organization.website,
        Phones: this.organization.phones,
        CurrencyCode: this.organization.currencyCode,
        Status: this.organization.status ? 'active' : 'Inactive',
        Logo: this.isEditMode ? this.organization.logo : logoName,
        groupId: this.organization.groupId
      };
      if (!this.isEditMode) {
        this.onboardingService.createRoleGroup(this.organization.name)
          .subscribe(
          (data) => {
            console.log('New Group created');
            this.onboardingService.setRoleGroup(new RoleGroup(
              data.Description,
              data.DisplayName,
              data.Id,
              data.status)
            );
            organization.groupId = data.Id;
            if (this.logoFile) {
              this.uploadLogo(organization, logoName);
            } else {
              this.createOrganization(organization);
            }
          },
          (error) => {
            this.notifiyStatus = new CustomerOnboardingStatus();
            this.notifiyStatus.index = 0;
            this.notifiyStatus.success = false;
            this.notifiyStatus.message = error.json();
            this.notifyErrorMessage('Error', error.json());
            this.status.next(this.notifiyStatus);
          }
          );
      } else if (this.logoFile) {
        this.uploadLogo(organization, logoName);
      } else {
        this.createOrganization(organization);
      }

    }

  }

  cancel() {
    this.router.navigate(['admin/feature']);
  }

  getCompanySelected(index: any) {
    if (index != -1) {
      const selectedOrg: any = this.availableOrganization[index];
      this.organization = selectedOrg;
      this.address = selectedOrg.address;
      this.addressname = `${this.address.name}, ${this.address.city},  ${this.address.stateCode},  ${this.address.countryCode} - ${this.address.postcode}`;
      this.selectedOrganization = selectedOrg;
      this.phoneNumber = this.organization.phones;
    }
  }

  uploadLogo(organization, logoName) {
    if (this.logoFile) {
      this.onboardingService.uploadLogo(this.logoFile, logoName)
        .subscribe(
        (data) => {
          organization.Logo = data._body;
          this.createOrganization(organization)
        },
        (error) => {
          this.notifiyStatus = new CustomerOnboardingStatus();
          this.notifiyStatus.index = 0;
          this.notifiyStatus.success = false;
          this.notifiyStatus.message = error.json();
          this.notifyErrorMessage('Error', error.json());
          this.status.next(this.notifiyStatus);
        },
        () => {
          console.log('image uploaded');
        }
        );
    }
  }

  createOrganization(organization) {
    this.onboardingService.saveOrganization(organization, this.isEditMode).subscribe(
      (response: any) => {
        const data = new Company(response);
        if (!this.isEditMode) {
          this.appService.addCompany(data);
        } else {
          this.appService.updateCompany(data, response.companyId);
        }
        this.notifiyStatus = new CustomerOnboardingStatus();
        this.notifiyStatus.index = 0;
        this.notifiyStatus.success = true;
        this.notifiyStatus.organization = new Organization(response);
        if (this.onboardingService.onboardEdit && this.onboardingService.onboardEdit.organizationId) {
          this.router.navigate(['admin/tenant-management']);
        } else {
          this.status.next(this.notifiyStatus);
        }
      },
      (error) => {
        this.notifiyStatus = new CustomerOnboardingStatus();
        this.notifiyStatus.index = 0;
        this.notifiyStatus.success = false;
        this.notifiyStatus.message = error.json();
        this.notifyErrorMessage('Error', error.json());
        this.status.next(this.notifiyStatus);
      }
    );
  }

  resetForm() {
    this.organization = new Organization();
    this.address = null;
    this.logoFile = null;
    this.addressname = '';
  }

  notifyErrorMessage(title, error) {
    if (error && error.isError) {
      this.appService.showErrorMessage(title, error.message);
    }
  }

  trackByFn(index: any, item: any) {
    return index;
  }
}
