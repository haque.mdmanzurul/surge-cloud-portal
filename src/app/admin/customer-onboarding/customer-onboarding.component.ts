import { Component, OnInit, Input, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { CustomerOnboardingItem } from './customer-onboarding-item';
import { CustomerOnboardingDirective } from './customer-onboarding.directive';
import { CreateOrganizationComponent } from './create-organization';
import { CreateRoleGroupComponent } from './create-role-group';
import { CreateUserComponent } from './create-user';
import { Users } from '../../app-common';
import { IWizardItem, TabsComponent } from '../../shared-module';
import * as _ from 'underscore';
import { CustomerOnboardingService } from './customer-onboarding.service';
import { NotificationsService } from 'angular2-notifications';
import { ICustomerOnboardingStatus, CustomerOnboardingStatus } from './customer-onboarding';
import { NgbTabset } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-customer-onboarding',
  templateUrl: './customer-onboarding.component.html',
  styleUrls: ['./customer-onboarding.component.scss']
})
export class CustomerOnboardingComponent {
  private creatingGroup: Boolean = false;

  public wizardItems: IWizardItem[] = [
    {
      name: 'Create Organization',
      icon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateOrg.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateOrgWhite.svg#Vector_Smart_Object',
      success: false,
      active: true,
      statusIcon: '',
      tabId: 'organization'
    },
    {
      name: 'Create Users',
      icon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateUser.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateUserWhite.svg#Vector_Smart_Object',
      success: false,
      active: false,
      statusIcon: '',
      tabId: 'user'
    },
    {
      name: 'Create Role Groups',
      icon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateRoleGroup.svg#Vector_Smart_Object',
      activeIcon: 'assets/icons/CustomerOnboarding/icon_CustomerOnboarding_CreateRoleGroupWhite.svg#Vector_Smart_Object',
      success: false,
      active: false,
      statusIcon: '',
      tabId: 'group'
    }
  ];

  public options = {
    timeOut: 5000,
    position: ['right']
  };

  private statusIcons = [
    'assets/icons/common/TabStatus_Error.svg#_',
    'assets/icons/common/TabStatus_Success.svg#Vector_Smart_Object_copy_13'
  ];

  @ViewChild(NgbTabset) tab: NgbTabset;
  private data: CustomerOnboardingStatus = new CustomerOnboardingStatus();
  private isForCancel: boolean = false;

  constructor(private onboardingService: CustomerOnboardingService,
    private _notificationsService: NotificationsService) { }

  ngOnInit() {
    this.onboardingService.onboardingData = new CustomerOnboardingStatus();
  }

  ngAfterViewInit() {

    setTimeout(() => {
      if (this.onboardingService.onboardEdit.userId || this.onboardingService.onboardNew.for == 2) {
        this.tab.select('user');
      } else if (this.onboardingService.onboardEdit.roleGroup || this.onboardingService.onboardNew.for == 3) {
        this.creatingGroup = true;
        this.tab.select('group');
      }
    }, 1);
  }

  beforeChange($event) {
    switch ($event.nextId) {
      case 'organization':
        if (!this.isForCancel) {
          $event.preventDefault();
        } else {
          this.wizardItems[0].active = true;
          this.wizardItems[1].active = false;
        }
        break;
      case 'user':
        if (!this.onboardingService.onboardEdit.userId && !this.onboardingService.onboardNew.for) {
          if (!this.wizardItems[0].success) {
            $event.preventDefault();
          } else {
            this.makeActiveTab($event.nextId);
          }
        } else {
          this.wizardItems[0].active = false;
          this.wizardItems[1].active = true;
        }

        break;
      case 'group':
        if (!this.creatingGroup) {
          $event.preventDefault();
        } else {
          this.makeActiveTab($event.nextId);
        }
        break;
    }
    this.isForCancel = false;

  }

  moveNextTab($event: CustomerOnboardingStatus) {
    if ($event.success) {
      this.wizardItems[$event.index].success = true;
      this.wizardItems[$event.index].statusIcon = this.statusIcons[1];

      if ($event.index == 0) {
        this.data.organization = $event.organization;
      } else if ($event.index == 2) {
        this.data.roleGroup = $event.roleGroup;
      }

      this.tab.select('user');

    } else {
      this.wizardItems[$event.index].success = false;
      this.wizardItems[$event.index].statusIcon = this.statusIcons[0];
    }

    this.onboardingService.onboardingData = this.data;

  }

  gotoRoleGroup($event) {
    this.creatingGroup = true;
    this.data.User = $event.user;
    this.data.isEditMode = $event.isEditMode;
    this.tab.select('group');
  }

  gotoPreviousTab(tabId: string) {
    this.isForCancel = true;
    this.tab.select(tabId);
  }

  makeActiveTab(tabId: string) {
    this.wizardItems.forEach(
      (wizard) => {
        if (wizard.tabId == tabId) {
          wizard.active = true;
        } else {
          wizard.active = false;
        }
      }
    )
  }

}
