import { Type } from '@angular/core';
export class CustomerOnboardingItem {
    constructor(public component: Type<any>, public data: any) { }
};
