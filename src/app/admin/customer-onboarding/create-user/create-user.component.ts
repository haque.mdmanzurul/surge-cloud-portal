import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Users } from '../../../app-common';
import { CustomerOnboardingService } from '../customer-onboarding.service'
import { Subject } from 'rxjs/Subject';
import { Organization, RoleGroup } from '../../../app-common';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { CustomerOnboardingStatus } from '../customer-onboarding';
import * as _ from 'underscore';
import { AppService } from '../../../app.service';


@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  public user: Users = new Users();
  public roleGroup: any;
  public changeStatus: Subject<Boolean> = new Subject();
  public drivingDetails: Boolean = false;
  public saveUserData: Subject<Users> = new Subject();
  private _organization: any;
  private _roleGroup: RoleGroup;
  private logoFile: File;
  private formatedExpiredDate: any;
  private isEditMode: Boolean = false;
  private availableUsers: Users[] = [];
  private modelDate: Object;
  public userIndex: any;


  public data: CustomerOnboardingStatus;
  @Output() public status: EventEmitter<CustomerOnboardingStatus> = new EventEmitter();
  @Output() public back: EventEmitter<string> = new EventEmitter();
  @Output() public createRole: EventEmitter<any> = new EventEmitter();
  public notifiyStatus: CustomerOnboardingStatus;

  todayDate: Date = new Date();
  private myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'mm/dd/yyyy',
    disableUntil: {
      year: this.todayDate.getFullYear(),
      month: this.todayDate.getMonth() + 1,
      day: this.todayDate.getDate()
    }
  };

  constructor(private router: Router,
    private onboardingservice: CustomerOnboardingService,
    public appService: AppService) {

  }

  ngOnInit() {
    this.data = this.onboardingservice.onboardingData;

    const existingUserId = this.onboardingservice.onboardEdit.userId;
    this._organization = this.data.organization;

    if (existingUserId) {
      this._organization = this.appService.getActiveCompany();
      this.getGetMembers();
    } else if (this.onboardingservice.onboardNew.for == 2) {
      this._organization = this.appService.getActiveCompany();
    }


    if (this.data && this.data.User) {
      this.user = this.data.User;
    }

    if (this.data && this.data.roleGroup) {
      this.user.roleGroup = this.data.roleGroup.id;
    }

    if (this.data.isEditMode) {
      this.isEditMode = true;
      this.getGetMembers();
    }

    // this.onboardingservice.roleGroup.subscribe(
    //   (data) => {
    //     this._roleGroup = data;
    //   }
    // );
    this._roleGroup = this.data.roleGroup;

    this.onboardingservice.getRoleGroup(this._organization.companyId).subscribe(
      (data) => {
        this.roleGroup = data;
        if (this.data && this.data.roleGroup) {
          this.user.roleGroup = this.data.roleGroup.id;
        }
      }
    );

  }

  onDateChanged(event: IMyDateModel) {
    if (event.jsdate) {
      this.formatedExpiredDate = event.jsdate.toISOString();
    }
  }

  createRoleGroup() {
    this.createRole.next({ user: this.user, isEditMode: this.isEditMode });
  }

  changeImage(image: { file, data }) {
    const pattern = new RegExp("image/");
    if (image.file && pattern.test(image.file.type)) {
      this.user.logo = image.data;
      this.logoFile = image.file;
    }
  }

  saveUser(form) {
    if (form.valid) {

      let logoName = '';
      let splittedName = (this.user.displayName.replace(/[^\w\s]/gi, '')).split(' ');
      let randomNumber: any = Math.random().toFixed(2);
      splittedName.push((randomNumber * 100).toString());

      if (this.logoFile) {
        logoName = `${splittedName.join('_')}.${this.logoFile.name.split('.').pop()}`;
      }

      let user: any = {
        UserId: this.user.Id,
        EmailId: this.user.emailId,
        Password: this.user.password,
        DisplayName: this.user.displayName,
        GivenName: this.user.givenName,
        Surname: this.user.surname,
        JobTitle: this.user.jobTitle,
        CompanyName: this._organization.name,
        CompanyId: this._organization.companyId,
        PhoneNumber: this.user.phoneNumber,
        RoleGroup: this.user.roleGroup,
        IsActive: this.user.status ? 1 : 0
      };
      if (this.drivingDetails) {

        user.DrivingLicenseNumber = this.user.drivingLicenseNumber;
        user.ExpirationDate = this.formatedExpiredDate;
        user.DriverId = this.user.driverId;

      }
      this.onboardingservice.saveUSer(user, this.isEditMode).subscribe(
        (data) => {
          this.notifySuccessMessage(
            'Success',
            `User is ${!this.isEditMode ? 'created' : 'updated'}  and redirecting to Dashboard`,
          );
          if (this.logoFile) {
            this.onboardingservice.uploadLogoForUser(this.logoFile, logoName, data.UserId).subscribe(
              (data) => {
                console.log('user image uploaded');
              },
              (error) => {
                this.showError(error);
              }
            );
          }
          if (!this.isEditMode || (this._organization.groupId == this.user.roleGroup)) {
            this.onboardingservice.addUserToGroup(data.UserId, this._organization.groupId).subscribe(
              (data) => {
                console.log('user added to role group')
              },
              (error) => {
                this.showError(error);
              }
            );
          }

          this.onboardingservice.updateRoleUserMap(data.UserId, this.user.roleGroup).subscribe(
            (data) => {
              console.log('user added to role')
            },
            (error) => {
              this.showError(error);
            }
          );
          if (this.onboardingservice.onboardNew.for == 2 || this.onboardingservice.onboardEdit.userId) {
            this.router.navigate(['admin/subscription-management']);
          } else {
            this.router.navigate([this.appService.getLandingPage()]);
          }
        },
        (error) => {
          this.showError(error);
        }
      );
    }

  }

  clearDriving(clear) {
    if (clear) {
      this.user.drivingLicenseNumber = '';
      this.user.expirationDate = '';
      this.user.driverId = '';
      this.modelDate = null;
    }
  }

  cancel() {
    this.back.next('organization');
  }

  getGetMembers() {
    this.onboardingservice.getGetMembers(this._organization.groupId).subscribe(
      (data) => {
        this.availableUsers = [];
        data.forEach(user => {
          
          this.availableUsers.push(new Users(user));
        });
        this.availableUsers = _.sortBy(this.availableUsers, 'displayName');
        if (this.onboardingservice.onboardEdit.userId) {
          this.isEditMode = true;
          const selectIndex = _.indexOf(this.availableUsers, _.findWhere(this.availableUsers, { Id: this.onboardingservice.onboardEdit.userId }));
          this.userIndex = selectIndex;
          this.setUserSelected(selectIndex);
        } else if (this.data && this.data.User && this.data.isEditMode) {
          this.isEditMode = true;
          const selectIndex = _.indexOf(this.availableUsers, _.findWhere(this.availableUsers, { Id: this.data.User.Id }));
          this.userIndex = selectIndex;
          this.setUserSelected(selectIndex);
        }
      }
    );
  }

  showError(error) {
    this.notifiyStatus = new CustomerOnboardingStatus();
    this.notifiyStatus.index = 1;
    this.notifiyStatus.success = false;
    this.notifiyStatus.message = error.json();
    this.notifyErrorMessage('Error', error.json());
    this.status.next(this.notifiyStatus);
  }


  setUserSelected(index: any) {
    if (index != "" || index != -1) {
      let selectedUser = this.availableUsers[index];
      console.log(selectedUser);
      let n = selectedUser.emailId.indexOf('@');
      selectedUser.emailId= selectedUser.emailId.substring(0, n != -1 ? n : selectedUser.emailId.length);
      this.user = selectedUser;
      if (!this.data.isEditMode) {
        this.setRoleGroup();
      }
      if (selectedUser.expirationDate) {
        this.drivingDetails = true;
        let expDate = new Date(selectedUser.expirationDate);
        this.formatedExpiredDate = selectedUser.expirationDate;
        this.modelDate = { date: { year: expDate.getFullYear(), month: expDate.getMonth(), day: expDate.getDay() } };
      }
    }

  }

  setRoleGroup() {
    this.onboardingservice.GetRoleUserMapping(this.user.Id).subscribe(
      (data) => {
        if (data[0]) {
          this.user.roleGroup = data[0].RoleId;
        }
      },
      (error) => {
        this.showError(error);
      }
    )
  }

  resetForm(form: any) {
    this.user = new Users();
  }

  notifyErrorMessage(title, error) {
    if (error && error.ErrorMessage) {
      this.appService.showErrorMessage(title, error.ErrorMessage);
    }
  }

  notifySuccessMessage(title, message) {
    this.appService.showSuccessMessage(title, message);
  }


}
