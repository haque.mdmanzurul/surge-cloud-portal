import { Organization, RoleGroup, Users } from '../../app-common';

export interface ICustomerOnboardingStatus {
    index: number;
    success: Boolean;
    organization?: Organization;
    roleGroup?: RoleGroup;
    User?: Users;
    message?: any;
}

export class CustomerOnboardingStatus {
    public index: number;
    public success: Boolean = false;
    public organization: Organization;
    public roleGroup: RoleGroup;
    public User: Users;
    public message: any;
    public isEditMode: Boolean = false;
}
