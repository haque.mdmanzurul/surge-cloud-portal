import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { CustomerOnboardingService } from '../customer-onboarding.service';
import { Organization, RoleGroup } from '../../../app-common';
import { AppStartService } from '../../../app-start.service';
import { INavigation } from '../../../../app';
import { CustomerOnboardingStatus } from '../customer-onboarding';
import * as _ from 'underscore';
import { AppService } from '../../..//app.service';

@Component({
  selector: 'app-create-role-group',
  templateUrl: './create-role-group.component.html',
  styleUrls: ['./create-role-group.component.scss']
})
export class CreateRoleGroupComponent implements OnInit {
  private _organization: any;
  private _features: any[];
  private _roleGroup: RoleGroup;
  public roleGroup = {
    displalyName: '',
    users: [],
    description: '',
    logo: '',
    status: true
  };
  public isEditMode: boolean = false;

  public selected: boolean = false;
  public savedRoleData: any;
  public isFeatureSaved: Boolean = false;
  public featureSelected: string = '2';
  public availableFeatures: any[] = [];

  public roleEdit: Boolean = false;


  public data: CustomerOnboardingStatus;
  @Output() public status: EventEmitter<CustomerOnboardingStatus> = new EventEmitter();
  @Output() public back: EventEmitter<string> = new EventEmitter();
  public notifiyStatus: CustomerOnboardingStatus;


  constructor(private router: Router,
    private onboardingService: CustomerOnboardingService, private appService: AppStartService, public apService: AppService) { }

  ngOnInit() {
    this.availableFeatures = this.appService.features;
    this.availableFeatures.map((feature) => {
      feature.enabled = false;
    });
    this._features = _.sortBy(_.where(this.availableFeatures, { ControlLevel: 2 }), 'ControlLevel');
    this.data = this.onboardingService.onboardingData;
    this._organization = this.data.organization || this.apService.getActiveCompany();
    this.changeFeature();
    this._roleGroup = this.onboardingService.getSavedRoleGroup();
    if (this.onboardingService.onboardEdit.roleGroup) {
      this.roleEdit = true;
      this.getRoleGroup();
      // this.roleGroup.displalyName = this.onboardingService.onboardEdit.roleGroup.Name;
    }
  }

  saveRoleGroup(isValid: Boolean) {
    let roleGroup = {
      DisplayName: this.roleGroup.displalyName,
      Description: this.roleGroup.description,
      Status: this.roleGroup.status ? 1 : 0,
      CompanyId: this._organization.companyId,
      CompanyName: this._organization.name,
      Id: this.onboardingService.onboardEdit.roleGroup ? this.onboardingService.onboardEdit.roleGroup.RoleId : null
    };
    this.onboardingService.saveRoleGroup(roleGroup, this.roleEdit).subscribe(
      (data) => {
        this.savedRoleData = new RoleGroup(data.Description, data.DisplayName, data.Id, data.Status);
        this.isEditMode = true;
        // if (this.isFeatureSaved) {
        //   this.notifiyStatus = new CustomerOnboardingStatus();
        //   this.notifiyStatus.index = 2;
        //   this.notifiyStatus.success = true;
        //   this.notifiyStatus.roleGroup = this.savedRoleData;
        //   this.router.navigate(['admin/subscription-management']);
        //   this.status.next(this.notifiyStatus);
        // } else {
        //   this.isEditMode = true;
        // }
      },
      (error) => {
        this.notifiyStatus = new CustomerOnboardingStatus();
        this.notifiyStatus.index = 2;
        this.notifiyStatus.success = false;
        this.notifiyStatus.message = error.json();
        this.notifyErrorMessage('Error', error.json().ErrorMessage);
        this.status.next(this.notifiyStatus);
      }
    )

  }

  saveFeature() {
    let availableFeature = _.where(this.availableFeatures, { enabled: true });
    let enabledFeature: any = [];

    availableFeature.forEach(feature => {
      let model = {
        RoleId: this.savedRoleData.id,
        FeatureId: feature.FeatureId,
        CanAccess: 1
      }
      enabledFeature.push(model);
    });

    this.onboardingService.MapRolesFeatures(enabledFeature).subscribe(
      (data) => {
        // if (this.savedRoleData && this.savedRoleData.id) {
        //   if (this.selected) {
            this.notifiyStatus = new CustomerOnboardingStatus();
            this.notifiyStatus.index = 2;
            this.notifiyStatus.success = true;
            this.notifiyStatus.roleGroup = this.savedRoleData;
            if (!this.data.organization) {
              this.router.navigate(['admin/subscription-management']);
            } else {
              this.status.next(this.notifiyStatus);
            }
          // }
        // } else {
        //   this.isEditMode = false;
        //   this.isFeatureSaved = true;
        // }
      },
      (error) => {
        this.notifiyStatus = new CustomerOnboardingStatus();
        this.notifiyStatus.index = 2;
        this.notifiyStatus.success = false;
        this.notifiyStatus.message = error.json();
        this.notifyErrorMessage('Error', error.json().ErrorMessage);
        this.status.next(this.notifiyStatus);
      }
    )
  }

  cancel() {
    this.back.next('user');
  }

  checkForm() {
    this.selected = (_.where(this.availableFeatures, { enabled: true })).length > 0 ? true : false;
  }

  changeFeature() {
    if (this._features) {
      this._features.forEach((feature) => {
        const selectedFeature = _.findWhere(this.availableFeatures, { FeatureId: feature.FeatureId });
        selectedFeature.enabled = feature.enabled;
      });
    }

    if (parseInt(this.featureSelected) == 1) {
      this._features = _.sortBy(_.where(this.availableFeatures, { ControlLevel: 1 }), 'ControlLevel');
    } else {
      this._features = _.sortBy(_.where(this.availableFeatures, { ControlLevel: 2 }), 'ControlLevel');
    }

  }

  notifyErrorMessage(title, message = '') {
    this.apService.showErrorMessage(title, message);
  }

  getRoleGroup() {
    this.onboardingService.getRoleGroup(this.apService.getActiveCompany().companyId).subscribe(
      (res) => {
        let role = _.findWhere(res, { Id: this.onboardingService.onboardEdit.roleGroup.RoleId });
        this.roleGroup.displalyName = role.DisplayName;
        this.roleGroup.description = role.Description;
        this.roleGroup.status = role.Status ? true : false;
        this.getRoleFeature(role.Id);
      }
    );
  }

  getRoleFeature(roleId: string) {
    this.onboardingService.GetRoleFeature(roleId).subscribe(
      (res) => {
        let availFeature = _.pluck(this.availableFeatures, 'FeatureId');
        if (res) {
          res.forEach(feature => {
            if (availFeature.indexOf(feature.FeatureId) > -1) {
              this.availableFeatures[availFeature.indexOf(feature.FeatureId)].enabled = true;
            }
          });
          this.selected = true;
        }
      });
  }
}
