import { TestBed, async } from '@angular/core/testing';
import {APP_INITIALIZER} from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { NgProgressModule } from 'ng2-progressbar';
import { Http, Response } from '@angular/http';
import { HttpService } from './shared-module';
import { Navigation, INavigation } from './app';
import { AppStartService, startupServiceFactory } from './app-start.service';



import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NgProgressModule
      ],
      declarations: [
        AppComponent
      ],
      providers:[
        AppStartService,
        HttpService,
        {
          provide: APP_INITIALIZER,
          useFactory: startupServiceFactory,
          deps: [AppStartService],
          multi: true
        }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app works!'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app works!');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('app works!');
  }));
});
