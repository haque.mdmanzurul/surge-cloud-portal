import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeoFencingMapComponent } from './geo-fencing-map.component';

describe('GeoFencingMapComponent', () => {
  let component: GeoFencingMapComponent;
  let fixture: ComponentFixture<GeoFencingMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeoFencingMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeoFencingMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
