import { Component, OnInit, Output, EventEmitter, Input, SimpleChange, ElementRef, NgZone, ViewChild, QueryList, ViewChildren, OnChanges } from '@angular/core';
import { GeoFencingService } from '../geo-fencing.service';
import { Facility, GeoFenceAsset } from '../../../app-common';
import { AppService } from '../../../app.service';
import { GeofenceFilter } from '../../operation';
import { MapsAPILoader, GoogleMapsAPIWrapper, CircleManager, AgmMarker, AgmMap, AgmCircle } from '@agm/core';
// import { AgmCoreModule } from '@agm/core';

import * as _ from 'underscore';
import { Router } from '@angular/router';

declare const google: any;

@Component({
  selector: 'app-geo-fencing-map',
  templateUrl: './geo-fencing-map.component.html',
  styleUrls: ['./geo-fencing-map.component.scss']
})
export class GeoFencingMapComponent implements OnInit {
  public lat: number = 51.678418;
  public lng: number = 7.809007;

  private filterBy: number = 1;
  private availableFacility: Facility[] = [];
  private geoFilter: GeofenceFilter = new GeofenceFilter();
  private availableGeoData: any;
  @Input('data') public mapData: GeoFenceAsset[] = [];
  public fitBounds: [{ lat: number, lng: number }] = null;
  @Input() set geoData(data) {
    this.availableGeoData = data;
    if (this.availableGeoData.length > 0) {
      // this.lat = this.availableGeoData[0].lat;
      // this.lng = this.availableGeoData[0].lng;
      this.drawLines();
      const bounds = new google.maps.LatLngBounds();
      data.forEach((ast) => {
        bounds.extend(ast);
      });
      this.map.setCenter(bounds.getCenter());
      this.map.fitBounds(bounds);
    }
  }

  @Input() set realtime(data) {
    this.updateAssetPosition(data);
  }

  public map: any;
  public polyline: any;
  public zoom: number = 2;
  @ViewChild(AgmCircle) circle: AgmCircle;
  @ViewChildren(AgmMarker) markers: QueryList<AgmMarker>;

  public drawingManager: any;
  public selectedShape: any;
  public avilableaAssetType: any[] = [];
  public newShape: any = {};

  private statusIcons: string[] = [
    'assets/icons/Geofencing/Status_Stopped.svg#Ellipse_661_copy_3',
    'assets/icons/Geofencing/Status_Inactive.svg#Ellipse_661_copy_6',
    'assets/icons/Geofencing/Status_Idle.svg#Ellipse_661_copy',
    'assets/icons/Geofencing/Status_Moving.svg#Ellipse_661_copy_15',
    'assets/icons/Geofencing/Status_Alarm.svg#Ellipse_661_copy_4'
  ];

  public sliderDate: { startDate: string, endDate: string } = { startDate: '', endDate: '' };
  public sliderTime: { startTime: string, endTime: string } = { startTime: '', endTime: '' };


  public availStatus: string[] = ['Inactive', "Active", "Moving", "Idle", "Alarm"];
  public alarmData: any[] = [];

  public fullscreen: any = {
    map: false,
    list: false,
  };
  public minimize: any = {
    map: false,
    list: false
  };

  public mapInterval = null;

  @Output() filter = new EventEmitter<GeofenceFilter>();
  @Output() action = new EventEmitter<GeoFenceAsset>();

  private circlesManager: any = {};

  label = {
    text: 'test',
    color: "#eb3a44",
    fontSize: "16px",
    fontWeight: "bold"
  };

  constructor(private geoFencingService: GeoFencingService, private elementRef: ElementRef,
    private appService: AppService, private mapsAPILoader: MapsAPILoader, private _zone: NgZone, private router: Router
  ) { }

  ngOnInit() {
    this.getAvailableFacility();
    this.getAssetType();

    this.mapsAPILoader.load().then(() => {
      const autocomplete: any = new google.maps.places.Autocomplete(this.elementRef.nativeElement.querySelector('input[name=addressName]'), {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this._zone.run(() => {
          const place: any = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
        });
      });

    });

    this.geoFencingService.filterApplied.subscribe(
      (data: GeoFenceAsset[]) => {
        if (this.mapInterval) {
          clearInterval(this.mapInterval);
        }
        this.mapData = data;
        if (this.mapData[0]) {
          let bounds = new google.maps.LatLngBounds();
          this.mapData.forEach((ast) => {
            bounds.extend({ lat: ast.latitude, lng: ast.longitude });
          });
          this.map.setCenter(bounds.getCenter());
          this.map.fitBounds(bounds);
        }
      }
    );

    this.geoFencingService.minMaxApplied.subscribe(
      (data) => {
        this.fullscreen = data.fullscreen;
        this.minimize = data.minimize;
      }
    );
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {

  }



  changeFilter() {
    setTimeout(
      () => {
        this.filter.emit(this.geoFilter);
      }, 2);
  }

  getAvailableFacility() {
    this.geoFencingService.getFacility(this.appService.getActiveCompany().companyId).subscribe(
      (data) => {
        data.forEach(facility => {
          let model = new Facility(facility.logo, facility.name, facility.status, facility.address, facility.timezone, facility.facilityType, facility.facilityId);
          this.availableFacility.push(model);
        });
        //this.geoFilter.facility = this.availableFacility[0].facilityId;
      }
    );
  }


  setLocation(lat: number, lng: number) {
    this.lat = lat;
    this.lng = lng;

    this.circle.getBounds().then(
      (data) => {
        this.getMarkersInsideCircle(data, this.markers);
      }
    );

  }

  getMarkersInsideCircle(circle, markers) {
    let result: AgmMarker[] = [];
    markers.forEach(marker => {
      if (circle.contains({ lat: marker.latitude, lng: marker.longitude })) {
        result.push(marker);
      }
    });

  }

  initDrawingTools() {
    const polyOptions = {
      strokeWeight: 0,
      fillOpacity: 0.45,
      editable: true,
      draggable: true,
      fillColor: 'red'
    };

    this.drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: null,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER,
        drawingModes: ['circle', 'polygon', 'polyline', 'rectangle']
      },
      markerOptions: {
        draggable: true
      },
      polylineOptions: {
        editable: true,
        draggable: true
      },
      rectangleOptions: polyOptions,
      circleOptions: polyOptions,
      polygonOptions: polyOptions,
      map: this.map
    });

    this.drawingManager.setMap(this.map);

    this.drawingEventListen();

  }

  mapRendered($event) {
    this.map = $event;
    this.initDrawingTools();
  }

  zoomChange(level: number) {
    this.zoom = level;
  }

  drawLines() {
    const lineSymbol = {
      path: google.maps.SymbolPath.CIRCLE,
      scale: 8,
      strokeColor: '#393'
    };

    this.polyline ? this.polyline.setMap(null) : null;

    this.polyline = new google.maps.Polyline({
      path: [], //this.availableGeoData,//[{ lat: 22.291, lng: 153.027 }, { lat: 18.291, lng: 153.027 }],
      icons: [{
        icon: lineSymbol,
        offset: '100%'
      }],
      map: this.map
    });

    this.animateCircle(this.polyline);

  }

  animateCircle(line) {
    let count = 0;
    let marker = null;
    const bounds = new google.maps.LatLngBounds();
    this.mapInterval = window.setInterval(() => {
      let path = line.getPath();
      const latlng = new google.maps.LatLng(this.availableGeoData[count].lat, this.availableGeoData[count].lng);
      if (this.availableGeoData[count].alarm) {
        this.alarmData.push({
          lat: this.availableGeoData[count].lat,
          lng: this.availableGeoData[count].lng
        });
      }
      path.push(latlng);
      this.map.panTo(latlng);
      bounds.extend(latlng);
      count += 1;
      if (count == this.availableGeoData.length) {
        clearInterval(this.mapInterval);
      }
    }, 350);
  }

  drawingEventListen() {

    /* http://bl.ocks.org/knownasilya/89a32e572989f0aff1f8 */
    google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (e) => {

      var newShape = e.overlay;

      newShape.type = e.type;

      if (e.type !== google.maps.drawing.OverlayType.MARKER) {
        // Switch back to non-drawing mode after drawing a shape.
        this.drawingManager.setDrawingMode(null);

        // Add an event listener that selects the newly-drawn shape when the user
        // mouses down on it.
        google.maps.event.addListener(newShape, 'click', (e) => {
          if (e.vertex !== undefined) {
            if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
              var path = newShape.getPaths().getAt(e.path);
              path.removeAt(e.vertex);
              if (path.length < 3) {
                newShape.setMap(null);
              }
            }
            if (newShape.type === google.maps.drawing.OverlayType.POLYLINE) {
              var path = newShape.getPath();
              path.removeAt(e.vertex);
              if (path.length < 2) {
                newShape.setMap(null);
              }
            }
          }
          this.setSelection(newShape);
        });
        this.setSelection(newShape);
      }
      else {
        google.maps.event.addListener(newShape, 'click', (e) => {
          this.setSelection(newShape);
        });
        this.setSelection(newShape);
      }
    });

    google.maps.event.addListener(this.drawingManager, 'drawingmode_changed', () => {
      if (this.selectedShape) {
        if (this.selectedShape.type !== 'marker') {
          // this.selectedShape.setEditable(false);
          this.selectedShape.setMap(null);
        }


        this.selectedShape = null;
      }
    });

    google.maps.event.addListener(this.map, 'click', () => {
      if (this.selectedShape) {
        if (this.selectedShape.type !== 'marker') {
          this.selectedShape.setEditable(false);
        }

        this.selectedShape = null;
      }
    });


  }

  setSelection(shape) {
    if (shape.type !== 'marker') {
      this.clearSelection();
      shape.setEditable(true);
      //this.selectColor(shape.get('fillColor') || shape.get('strokeColor'));
    }

    this.selectedShape = shape;
  }

  clearSelection() {
    if (this.selectedShape) {
      if (this.selectedShape.type !== 'marker') {
        this.selectedShape.setEditable(false);
      }

      this.selectedShape = null;
    }
  }

  deleteSelectedShape() {
    if (this.selectedShape) {
      this.selectedShape.setMap(null);
    }
  }

  getAssetType() {
    this.geoFencingService.getAssetType().subscribe(
      (data) => {
        if (data) {
          data.forEach(assettype => {
            let model = { name: assettype.Name, status: assettype.Status };
            this.avilableaAssetType.push(model);
          });
        }
      }
    );
  }

  saveSelectedShape() {
    if (this.selectedShape) {
      switch (this.selectedShape.type) {
        case 'circle':
          this.newShape = {
            type: this.selectedShape.type,
            radius: this.selectedShape.getRadius(),
            path: [{ lat: this.selectedShape.getCenter().lat(), lng: this.selectedShape.getCenter().lng() }]
          }
          break;
        case 'polygon':
          this.newShape = {
            type: this.selectedShape.type,
            radius: null,
            path: this.getLatLngFromPath(this.selectedShape.getPaths().getArray()[0])
          }
          break;
        case 'polyline':
          this.newShape = {
            type: this.selectedShape.type,
            radius: null,
            path: this.getLatLngFromPath(this.selectedShape.getPath())
          }
          break;
        case 'rectangle':
          this.newShape = {
            type: this.selectedShape.type,
            radius: null,
            path: this.selectedShape.getBounds().toJSON()
          }
          break;
      }
    }
  }

  getLatLngFromPath(paths: any) {
    let latlng: any = [];
    paths.forEach((path, key) => {
      latlng.push({ lat: path.lat(), lng: path.lng() });
    });
    return latlng;
  }

  triggerResize() {
    google.maps.event.trigger(this.map, 'resize');
  }

  dateChanged(data: any[]) {
    this.sliderDate.startDate = data[0];
    this.sliderDate.endDate = data[1];
  }

  timeChanged(time: any) {
    this.sliderTime.startTime = time[0] > 10 ? time[0] + ':00' : `0${time[0]}:00`;
    this.sliderTime.endTime = time[1] > 10 ? time[1] + ':00' : `0${time[1]}:00`;
  }

  navigateDashboard(asset) {
    this.router.navigate([`operation/asset-dashboard/${asset.assetId}`]);
  }

  changeMinMax() {
    this.geoFencingService.applyFullscreen({ fullscreen: this.fullscreen, minimize: this.minimize });
  }

  alarmClicked(marker) {
    this.router.navigate([`operation/alarm/${marker.assetId}/${marker.gwSNO}`]);
  }

  updateAssetPosition(data) {
    if (data) {
      let ind = _.indexOf(this.mapData, _.findWhere(this.mapData, { assetId: data.assetID }));
      if (ind > -1) {
        if (this.circlesManager[data.assetID]) {
          this.circlesManager[data.assetID].setMap(null);
        }
        if (data.type == 'geo') {
          this.mapData[ind].latitude = parseFloat(data.latitude);
          this.mapData[ind].longitude = parseFloat(data.longitude);
        }
        let lat = data.type == 'alarm' ? this.mapData[ind].latitude : parseFloat(data.latitude);
        let lng = data.type == 'alarm' ? this.mapData[ind].longitude : parseFloat(data.longitude);
        var cityCircle = new google.maps.Circle({
          strokeColor: data.type == 'alarm' ? '#FF0000' : '#96c33b',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: data.type == 'alarm' ? '' : '',
          fillOpacity: 0.35,
          map: this.map,
          center: { lat: lat, lng: lng },
          radius: 1000
        });
        this.circlesManager[data.assetID] = cityCircle;
        let bounds = new google.maps.LatLngBounds();
        bounds.extend({ lat: lat, lng: lng });
        const status = (data.type == 'alarm') ? 6 : 3;
        this.mapData[ind].icon = this.getIcon(status);
        console.log(lat, lng, data.assetID);
      }
    }

  }


  getIcon(status: number) {
    return new google.maps.MarkerImage('assets/icons/Geofencing/CargoTruck.svg#CargoTruck', new google.maps.Size(40, 40), new google.maps.Point(0, 0));
  }
}