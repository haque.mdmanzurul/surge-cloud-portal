import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../../core';
import { FormsModule } from '@angular/forms';
// import { AgmCoreModule, GoogleMapsAPIWrapper } from 'angular2-google-maps/core';
import { AgmCoreModule } from '@agm/core';

import { DataTableModule } from 'angular2-datatable';
import { GeoFencingComponent } from './geo-fencing.component';
import { GeoFencingMapComponent } from './geo-fencing-map/geo-fencing-map.component';
import { GeoFencingListComponent } from './geo-fencing-list/geo-fencing-list.component';
import { GeoFencingService } from './geo-fencing.service';
import { GmapComponent } from './gmap/gmap.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CoreModule,
    AgmCoreModule,
    DataTableModule
  ],
  declarations: [
    GeoFencingComponent,
    GeoFencingMapComponent,
    GeoFencingListComponent,
    GmapComponent
  ],
  providers: [GeoFencingService]
})
export class GeoFencingModule { }
