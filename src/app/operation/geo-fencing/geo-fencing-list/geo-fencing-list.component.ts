import { Component, OnInit, Input, ViewChild, TemplateRef, SimpleChange, EventEmitter, Output } from '@angular/core';
import { GeoFenceAsset } from '../../../app-common';
import { AppService } from '../../../../app';
import { OperationService } from '../../operation.service';
import * as _ from 'underscore';
import { GeofenceFilter, GeoData } from '../../operation';
import { GeoFencingService } from '../geo-fencing.service';
import { Router } from '@angular/router';

import { Socket } from 'ng-socket-io';

declare const google: any;


@Component({
  selector: 'app-geo-fencing-list',
  templateUrl: './geo-fencing-list.component.html',
  styleUrls: ['./geo-fencing-list.component.scss']
})
export class GeoFencingListComponent implements OnInit {

  public data: GeoFenceAsset[] = [];
  public availableAsset: GeoFenceAsset[] = [];
  public filterQuery = "";
  public rowsOnPage = 10;
  public sortBy = "assetId";
  public sortOrder = "asc";
  public columns = [
    {
      name: '',
      cssClass: 'col-lg-2',
    },
    {
      name: 'Asset Id',
      cssClass: 'col-lg-4',
      sort: 'assetId'
    },
    {
      name: 'Last Updated',
      cssClass: 'col-lg-4',
      sort: 'assetLastUpdateDate'
    },
    {
      name: 'Actions',
      cssClass: '',
      sort: 'status'
    }
  ];

  public availableColumns: any[] = [
    {
      name: 'Asset Id',
      cssClass: 'col-lg-4',
      sort: 'assetId'
    },
    {
      name: 'VIN',
      cssClass: 'col-lg-4',
      sort: 'vin'
    },
    {
      name: 'Make',
      cssClass: 'col-lg-4',
      sort: 'make'
    },
    {
      name: 'Model',
      cssClass: 'col-lg-4',
      sort: 'model'
    },
    {
      name: 'Serial Number',
      cssClass: 'col-lg-4',
      sort: 'serialNumber'
    }
  ];

  private statusIcons: string[] = [
    'assets/icons/Geofencing/Status_Stopped.svg#Ellipse_661_copy_3',
    'assets/icons/Geofencing/Status_Inactive.svg#Ellipse_661_copy_6',
    'assets/icons/Geofencing/Status_Idle.svg#Ellipse_661_copy',
    'assets/icons/Geofencing/Status_Moving.svg#Ellipse_661_copy_15',
    'assets/icons/Geofencing/Status_Alarm.svg#Ellipse_661_copy_4'
  ];

  public event: any = {
    idle: 0,
    inactive: 0,
    active: 0,
    alarm: 0,
    moving: 0
  }

  public geoHistoryData: Array<{ lat: number, lng: number, alarm: boolean }> = [];
  public alarmData: Array<{ lat: number, lng: number }> = [];
  public availableAlarmData: GeoData[] = [];
  public sliderDate: { startDate: string, endDate: string } = { startDate: '', endDate: '' };
  public sliderTime: { startTime: string, endTime: string } = { startTime: '', endTime: '' };

  private keysForSubscription = [];

  public fullscreen: any = {
    map: false,
    list: false,
  };
  public minimize: any = {
    map: false,
    list: false
  };

  @Input() filterBy: any;
  @Output() public dataChange: EventEmitter<GeoFenceAsset[]> = new EventEmitter();
  @Output() public geoData: EventEmitter<Array<{ lat: number, lng: number, alarm: boolean }>> = new EventEmitter();
  @Output() public realTime: EventEmitter<any> = new EventEmitter();

  constructor(private operationService: OperationService, private appService: AppService,
    private geoFencingService: GeoFencingService, private router: Router, private socket: Socket) { }

  ngOnInit() {
    this.getAssets(this.appService.getActiveCompany().companyId);
    this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.availableAsset = [];
        this.data = [];
        this.event = {
          idle: 0,
          inactive: 0,
          active: 0,
          alarm: 0,
          moving: 0
        };
        this.getAssets(company.companyId);
      }
    );

    this.geoFencingService.minMaxApplied.subscribe(
      (data) => {
        this.fullscreen = data.fullscreen;
        this.minimize = data.minimize;
      }
    );

    this.socket.on('data', (data) => {
      if (data.status == 'Geo') {
        data.type = 'geo';
        this.realTime.next(data);
      } else if (data.status != 'Realtime') {
        data.type = 'alarm';
        this.realTime.next(data);
      }
    });
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    let filter = changes.filterBy;
    if (filter) {
      this.applyFileter(filter.currentValue);
    }

  }

  getAssets(companyId: string) {
    this.operationService.getAssets(companyId).subscribe(
      (res) => {
        const company = this.appService.getActiveCompany();
        this.keysForSubscription = [];
        res.forEach((asset: any) => {
          if (asset.Status == 2) {
            this.event.idle += 1;
          } else if (asset.Status == 0) {
            this.event.inactive += 1;
          } else if (asset.Status == 1) {
            this.event.active += 1;
          } else if (asset.Status == 3) {
            this.event.moving += 1;
          } else {
            this.event.alarm += 1;
          }
          let model = new GeoFenceAsset(
            asset.AId,
            asset.Altitude,
            asset.AssetId,
            asset.AssetLastUpdateDate,
            asset.AssetLogo,
            asset.AssetStatus,
            asset.AssetType,
            asset.CalibrationId,
            asset.CompanyId,
            asset.CompanyName,
            asset.ControllerId,
            asset.CustomerEquipmentGroup,
            asset.CustomerEquipmentId,
            asset.FacilityId,
            asset.FacilityName,
            asset.IotHubName,
            asset.LatestSyncDate,
            asset.Latitude,
            asset.Logo,
            asset.Longitude,
            asset.Make,
            asset.Model,
            asset.SerialNumber,
            asset.SoftwareId,
            asset.Status,
            asset.UnitNumber,
            asset.Vin,
            asset.gwAppBuild,
            asset.gwDCAver,
            asset.gwDescription,
            asset.gwIMEI,
            asset.gwOSBuild,
            asset.gwRSSI,
            asset.gwSIMICCID,
            asset.gwSNO,
            asset.gwTimezone,
            asset.gwWANIP,
            asset.pVer,
            asset.param
          );

          model.icon = this.getIcon(asset.Status);
          this.data.push(model);

          this.keysForSubscription.push(`realtime_${company.name.split(' ').join('')}_${asset.gwSNO}_${asset.AssetId}`);
          this.keysForSubscription.push(`geo_${company.name.split(' ').join('')}_${asset.gwSNO}_${asset.AssetId}`);
          this.keysForSubscription.push(`alarm_${company.name.split(' ').join('')}_${asset.gwSNO}_${asset.AssetId}`);
        });
        this.availableAsset = this.data;
        this.subscribeForAlarm();
        this.dataChange.next(this.availableAsset);
      }
    )
  }

  applyFileter(filter: GeofenceFilter) {
    let query: any = {};
    if (filter.utilities) {
      this.columns[1] = this.availableColumns[Number(this.filterBy.utilities) - 1];
    }

    filter.facility ? query.facilityId = filter.facility : null;
    filter.event ? query.assetStatus = parseInt(filter.event) : null;
    filter.assetType ? query.assetType = filter.assetType : null;
    this.data = _.where(this.availableAsset, query);
    if (filter.vin) {
      filter.vin ? query.vin = filter.vin : null;
      const vinList = _.where(this.availableAsset, query);
      delete query.vin;
      filter.vin ? query.serialNumber = filter.vin : null;
      const serialList = _.where(this.availableAsset, query);
      this.data = _.uniq([...vinList, ...serialList]);
    }
    this.dataChange.next(this.data);
  }

  getGeoData(asset: GeoFenceAsset, startDate = this.sliderDate.startDate, endDate = this.sliderDate.endDate, startTime = this.sliderTime.startTime, endTime = this.sliderTime.endTime) {
    this.geoFencingService.getGeoData(this.appService.getActiveCompany().companyId, asset.gwSNO, asset.assetId, startDate + ' ' + startTime, endDate + ' ' + endTime).subscribe(
      (res) => {
        this.geoHistoryData = [];
        const availableAlarm = _.pluck(this.availableAlarmData, "OccurrenceDate");
        res.forEach(
          (geo) => {
            const latlng = geo.Param[0].Value.split(',');
            if (latlng[0]) {
              let alarm = this.checkClosestAlarm(availableAlarm, geo.OccurrenceDate);
              this.geoHistoryData.push({ lat: parseFloat(latlng[0]), lng: parseFloat(latlng[1]), alarm: alarm });
            }
          }
        );
        this.geoData.next(this.geoHistoryData);
      }
    );
  }

  timeChanged(time: any) {
    this.sliderTime.startTime = time[0] > 10 ? time[0] + ':00' : `0${time[0]}:00`;
    this.sliderTime.endTime = time[1] > 10 ? time[1] + ':00' : `0${time[1]}:00`;
  }

  dateChanged(data: any[]) {
    this.sliderDate.startDate = data[0];
    this.sliderDate.endDate = data[1];
  }

  navigateDashboard(asset) {
    this.router.navigate([`operation/asset-dashboard/${asset.assetId}`]);
  }

  toggleExpand(item: GeoFenceAsset) {
    this.data.forEach((itm) => {
      if (item.assetId == itm.assetId) {
        itm.expanded = !itm.expanded;
      } else {
        itm.expanded = false;
      }
    });
  }

  getAlarmData(asset: GeoFenceAsset, startDate = this.sliderDate.startDate, endDate = this.sliderDate.endDate, startTime = this.sliderTime.startTime, endTime = this.sliderTime.endTime) {
    this.geoFencingService.getAlarmData(this.appService.getActiveCompany().companyId, asset.gwSNO, asset.assetId, startDate + ' ' + startTime, endDate + ' ' + endTime).subscribe(
      (res) => {
        this.availableAlarmData = [];
        res.forEach(
          (geo) => {
            let model = new GeoData(
              geo.gwSno,
              geo.AssetId,
              geo.OccurrenceDate,
              geo.Param
            );
            this.availableAlarmData.push(model);
          }
        );
        this.getGeoData(asset, startDate, endDate, startTime, endTime);
      }
    );
  }

  checkClosestAlarm(available, current) {
    let closest = false;
    available.forEach(element => {
      let alarmDate = Date.parse(element);
      let geoDate = Date.parse(current);

      if (Math.abs(alarmDate - geoDate) <= 5000 || Math.abs(geoDate - alarmDate) <= 5000) {
        closest = true;
      }
    });
    return closest;
  }

  changeMinMax() {
    this.geoFencingService.applyFullscreen({ fullscreen: this.fullscreen, minimize: this.minimize });
  }

  subscribeForAlarm() {
    this.socket.emit("subscribe", this.keysForSubscription);
  }

  getIcon(status: string) {
    return new google.maps.MarkerImage(this.statusIcons[status], new google.maps.Size(40, 40), new google.maps.Point(0, 0));
  }
}
