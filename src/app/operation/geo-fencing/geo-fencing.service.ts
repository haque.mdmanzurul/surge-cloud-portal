import { Injectable } from '@angular/core';
import { HttpService } from '../../shared-module';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { GeoFenceAsset } from '../../app-common';


@Injectable()
export class GeoFencingService {

  public filterApplied: Subject<Array<GeoFenceAsset>> = new Subject();
  public minMaxApplied: Subject<{ fullscreen, minimize }> = new Subject();

  constructor(private httpService: HttpService) { }

  getFacility(companyId): Observable<any> {
    return this.httpService.get(`facility/api/facility/get/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getGeoData(companyId, gwSno, assetId, startDate, endDate) {
    return this.httpService.get(`device/api/iot/GetGeoData?companyId=${companyId}&gwSno=${gwSno}&assetId=${assetId}&startDate=${startDate}&endDate=${endDate}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getAssetType(): Observable<any> {
    return this.httpService.get('device/api/iot/getassettype')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  setMapData(data: GeoFenceAsset[]) {
    this.filterApplied.next(data);
  }

  getAlarmData(companyId: string, gwSno: string, assetId: string, startDate: string, endDate: string) {
    return this.httpService.get(`device/api/iot/GetAlarmData?companyId=${companyId}&gwSno=${gwSno}&assetId=${assetId}&startDate=${startDate}&endDate=${endDate}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  applyFullscreen(data) {
    this.minMaxApplied.next(data);
  }


}
