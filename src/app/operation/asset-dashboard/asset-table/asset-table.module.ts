import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetTableComponent } from './asset-table.component';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    FormsModule
  ],
  declarations: [AssetTableComponent],
  exports : [AssetTableComponent]
})
export class AssetTableModule { }
