import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { AssetDashboardTable, AssetTableShrinkView } from '../asset-dashboard';
import { OperationService } from '../../operation.service';
import * as _ from 'underscore';
@Component({
  selector: 'app-asset-table',
  templateUrl: './asset-table.component.html',
  styleUrls: ['./asset-table.component.scss']
})
export class AssetTableComponent implements OnInit {

  @Input() assetTableInfo: AssetDashboardTable[] = [];
  private paginationInfo = {
    sortOrder: 'asc', sortBy: 'Name',
    numberRowPerPage: 999
  };
  @Output() private selectedRTData: EventEmitter<string[]> = new EventEmitter();
  private selectedData: string[] = [];
  @Output() private RTtableView: EventEmitter<boolean> = new EventEmitter();
  private selectedView: boolean = true;  // shrink
  @Input() tableviewChange: AssetTableShrinkView;
  private maxmizeView: boolean = false; //max
  @Output() RTmaxmizeView: EventEmitter<boolean> = new EventEmitter();

  public realtimeData = [];

  constructor(private operationService: OperationService) {
    // this.paginationInfo.numberRowPerPage = this.operationService.numberOfRecordPerPage;
    this.RTtableView.emit(this.selectedView);
    // console.log(this.RTtableView);
  }

  ngOnInit() {
  }

  realTimeDataChange($event) {
    let x = $event.target || $event.srcElement;
    if (x.checked) {
      this.selectedData.push(x.value);
    }
    else {
      this.selectedData = _.without(this.selectedData, x.value);
    }
    this.selectedRTData.emit(this.selectedData);
  }
  changeTableView() {
    this.selectedView = this.selectedView ? false : true;

    if (!this.selectedView && !this.tableviewChange.selAlarmView) {
      this.selectedView = true;
    }
    this.RTtableView.emit(this.selectedView);
  }
  changeTableMaxView() {
    this.maxmizeView = this.maxmizeView ? false : true;
    this.RTmaxmizeView.emit(this.maxmizeView);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!Array.isArray(changes.assetTableInfo.currentValue)) {
      let data = changes.assetTableInfo.currentValue;
      data.event_param = JSON.parse(data.event_param);
      this.realtimeData.push(data);
    }
  }

}
