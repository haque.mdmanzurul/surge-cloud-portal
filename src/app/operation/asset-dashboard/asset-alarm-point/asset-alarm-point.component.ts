import { Component, OnInit, Input, Output, EventEmitter,SimpleChanges } from '@angular/core';
import { OperationService } from '../../operation.service';
import { AssetTableShrinkView } from '../asset-dashboard';
@Component({
  selector: 'app-asset-alarm-point',
  templateUrl: './asset-alarm-point.component.html',
  styleUrls: ['./asset-alarm-point.component.scss']
})
export class AssetAlarmPointComponent implements OnInit {

  @Input() assetAlarmInfo: any[];

  @Input() selectedRealTimeData: string[];

  private selectedView : boolean = true;
  @Output() private ACtableView : EventEmitter<boolean> = new EventEmitter();
  
  private maxmizeView : boolean = false; //max
  @Output() ACmaxmizeView : EventEmitter<boolean> = new EventEmitter();
  
  private paginationInfo = {
    sortOrder: 'asc', sortBy: 'dataoccured',
    numberRowPerPage: 25
  };

  @Input() tableviewChange : AssetTableShrinkView;

  public liveData = [];
  
  constructor(private operationService: OperationService) {
    this.paginationInfo.numberRowPerPage = this.operationService.numberOfRecordPerPage;
  }

  ngOnInit() {
  }
  changeTableView()
  {
    this.selectedView = this.selectedView ? false:true;
    if(!this.selectedView && !this.tableviewChange.selRTtableView) {
      this.selectedView = true;
    }
    this.ACtableView.emit(this.selectedView);
  }
  changeTableMaxView(){
    this.maxmizeView = this.maxmizeView ? false:true;
    this.ACmaxmizeView.emit(this.maxmizeView);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!Array.isArray(changes.assetAlarmInfo.currentValue)) {
      let data = changes.assetAlarmInfo.currentValue;
      // data.event_param = JSON.parse(data.event_param);
      this.liveData.push(data);
    }
  }
  

}
