import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetAlarmPointComponent } from './asset-alarm-point.component';
import { DataTableModule } from 'angular2-datatable';
@NgModule({
  imports: [
    CommonModule,
    DataTableModule
  ],
  declarations: [AssetAlarmPointComponent],
  exports: [AssetAlarmPointComponent]
})
export class AssetAlarmPointModule { }
