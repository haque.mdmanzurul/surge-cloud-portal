import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AssetDashboardComponent } from './';
import { AssetDescModule } from './asset-desc/asset-desc.module';
import  { AssetLocationModule } from './asset-location/asset-location.module';
import { AssetTableModule } from './asset-table/asset-table.module';
import { AssetAlarmPointModule } from './asset-alarm-point/asset-alarm-point.module';
import { AssetDashboardService } from './asset-dashboard.service';
import { CoreModule } from '../../core';

@NgModule({
  imports: [
    CommonModule,
    AssetDescModule,
    AssetLocationModule,
    AssetAlarmPointModule,
    AssetTableModule,
    CoreModule,
    FormsModule
  ],
  declarations: [
  AssetDashboardComponent,
  
  ],
  exports: [
  	AssetDashboardComponent
  ],
  providers: [
  AssetDashboardService
  ]
})
export class AssetDashboardModule { }
