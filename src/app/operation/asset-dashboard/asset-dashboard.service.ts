import { Injectable } from '@angular/core';
import { HttpService } from './../../shared-module';
import { Observable } from 'rxjs/Observable';
import { Company } from '../../../app';

@Injectable()
export class AssetDashboardService {

  constructor(public httpService: HttpService) { }

  getAssetDesc(companyId: string): Observable<any> {

    return this.httpService.get(`device/api/iot/get/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getAssetMapInfo(companyId: string): Observable<any> {

    return this.httpService.get(`device/api/iot/getdevicebycompany/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getAssetTableInfo(obj: { companyId: string, gwSno: string, assetId: string }): Observable<any> {

    return this.httpService.get(encodeURI(`device/api/iot/getrealtimedata?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getAssetAlaramInfo(obj: { companyId: string, gwSno: string, assetId: string }): Observable<any> {

    return this.httpService.get(encodeURI(`device/api/iot/getalarmdata?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }
  getHistoricalData(obj: { companyId: string, gwSno: string, assetId: string, datapointname: string, startDate: string, endDate: string }): Observable<any>{
    console.log(encodeURI(`device/api/iot/GetHistoryData?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}&datapointname=${obj.datapointname}&startDate=${obj.startDate}&endDate=${obj.endDate}`));
    return this.httpService.get(encodeURI(`device/api/iot/GetHistoryData?companyId=${obj.companyId}&gwSno=${obj.gwSno}&assetId=${obj.assetId}&datapointname=${obj.datapointname}&startDate=${obj.startDate}&endDate=${obj.endDate}`))
    .map(res => res.json())
    .catch(err => Observable.throw(err));
}

}
