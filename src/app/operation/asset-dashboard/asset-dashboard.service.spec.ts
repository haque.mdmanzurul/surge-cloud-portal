import { TestBed, inject } from '@angular/core/testing';

import { AssetDashboardService } from './asset-dashboard.service';

describe('AssetDashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssetDashboardService]
    });
  });

  it('should ...', inject([AssetDashboardService], (service: AssetDashboardService) => {
    expect(service).toBeTruthy();
  }));
});
