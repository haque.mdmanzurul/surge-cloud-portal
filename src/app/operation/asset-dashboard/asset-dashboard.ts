export interface AssetDashboardDesc {
	aId: string,
	assetId: string,
	companyId: string,
	companyName: string,
	assetType: string,
	logo: string,
	make: string,
	model: string,
	serialNumber: string,
	unitNumber: string,
	vin: string,
	softwareId: string,
	calibrationId: string,
	customerReference: string,
	customerFacilityGroup: string,
	customerEquipmentGroup: string,
	customerEquipmentId: string,
	tags: string[],
	status: number,
	createdBy: string,
	updatedBy: string,
	lastUpdatedDate: Date
}

export interface AssetDashboardMap {
	ControllerId: string,
	CompanyId: string,
	CompanyName: string,
	FacilityId: string,
	FacilityName: string,
	AId: string,
	AssetId: string,
	IotHubName: string,
	MsgType: string,
	pVer: string,
	gwSNO: string,
	gwDescription: string,
	gwOSBuild: string,
	gwAppBuild: string,
	gwDCAver: string,
	gwSIMICCID: string,
	gwIMEI: string,
	gwRSSI: string,
	gwTimezone: string,
	gwWANIP: string,
	Latitude: number,
	Longitude: number,
	Altitude: string,
	LatestSyncDate: Date,
	tags: string[],
	Status: number,
	param: string[]
}

export interface iotRealTimeData {
	TS: Date,
	Name: string,
	Value: string,
	Status: string
}

export interface AssetDashboardTable {
	gwSno: string,
	AssetId: string,
	OccurrenceDate: Date,
	Param: iotRealTimeData[]
}

export interface AssetTableShrinkView{
	selRTtableView : boolean, 
	selAlarmView : boolean,
	rtClass : string, 
    rtInnerClass : string, 
    alarmClass:string, 
    alarmInnerClass:string,
    RTtableMaxView : boolean,
    AlarmMaxView : boolean
}