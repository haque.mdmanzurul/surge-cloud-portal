import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetDescComponent } from './asset-desc.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AssetDescComponent],
  exports: [AssetDescComponent
  ]
})
export class AssetDescModule { }
