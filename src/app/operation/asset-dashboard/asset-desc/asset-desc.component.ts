import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { AssetDashboardDesc } from './../asset-dashboard';
import { AppService } from './../../../app.service';
@Component({
  selector: 'app-asset-desc',
  templateUrl: './asset-desc.component.html',
  styleUrls: ['./asset-desc.component.scss']
})
export class AssetDescComponent implements OnInit {

  @Input() assetDescInfo: any;

  defaultImg: string;

  constructor(private appSerive: AppService) {
    this.defaultImg = this.appSerive.getDefaultImage();
  }

  ngOnInit() {
  }


  ngOnChanges(changes: SimpleChanges) {
    this.assetDescInfo = changes.assetDescInfo.currentValue;
  }

}
