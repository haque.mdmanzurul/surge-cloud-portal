import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { AssetDashboardMap } from './../asset-dashboard';

@Component({
  selector: 'app-asset-location',
  templateUrl: './asset-location.component.html',
  styleUrls: ['./asset-location.component.scss']
})
export class AssetLocationComponent implements OnInit {

  @Input() assetMapInfo: any;

  constructor() {

  }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    this.assetMapInfo = changes.assetMapInfo.currentValue;
    if (changes.assetMapInfo.currentValue) {
      this.assetMapInfo.latitude = parseFloat(this.assetMapInfo.latitude);
      this.assetMapInfo.longitude = parseFloat(this.assetMapInfo.longitude);
    }
  }

}
