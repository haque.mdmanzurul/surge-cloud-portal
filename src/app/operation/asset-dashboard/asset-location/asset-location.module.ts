import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetLocationComponent } from './asset-location.component';
import { AgmCoreModule } from '@agm/core';
import { environment } from '../../../../environments/environment';
@NgModule({
	imports: [
		CommonModule,
		AgmCoreModule.forRoot({
        apiKey: environment.googleApiKey
    })
	],
	declarations: [
		AssetLocationComponent
	],
	exports: [
		AssetLocationComponent
	]


})
export class AssetLocationModule { }
