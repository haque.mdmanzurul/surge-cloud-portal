import { Component, OnInit } from '@angular/core';
import { AppService } from './../../app.service';
import { Subscription } from 'rxjs/Rx';
import { Router, ActivatedRoute } from '@angular/router';
import { AssetDashboardService } from './asset-dashboard.service';
import { AssetDashboardDesc, AssetDashboardMap, AssetDashboardTable, AssetTableShrinkView } from './asset-dashboard';
import { SingleChartData, MultiChartData } from '../../core';

import { Socket } from 'ng-socket-io';
import * as _ from 'underscore';


@Component({
  selector: 'app-asset-dashboard',
  templateUrl: './asset-dashboard.component.html',
  styleUrls: ['./asset-dashboard.component.scss']
})
export class AssetDashboardComponent implements OnInit {

  private routerParam: Subscription;
  companyId: string;
  assetId: string;
  assetDescInfo: AssetDashboardDesc;
  assetMapInfo: AssetDashboardMap;
  assetTableInfo: AssetDashboardTable[] = [];
  assetAlarmInfo: any[] = [];
  linechartData: MultiChartData[] = [
    {
      'name': 'Germany',
      'series': [
        {
          'name': '2010',
          'value': 7300000
        },
        {
          'name': '2011',
          'value': 8940000
        }
      ]
    },

    {
      'name': 'USA',
      'series': [
        {
          'name': '2010',
          'value': 7870000
        },
        {
          'name': '2011',
          'value': 8270000
        }
      ]
    },

    {
      'name': 'France',
      'series': [
        {
          'name': '2010',
          'value': 5000002
        },
        {
          'name': '2011',
          'value': 5800000
        }
      ]
    }
  ];
  selectedRealTimeData: string[] = [];
  dateInfo: string[] = [];
  timeInfo: number[] = [];
  lineChartInfo: any = { showLegend: false, height: 255 };
  private tableviewChange: AssetTableShrinkView = {
    selRTtableView: true, selAlarmView: true,
    rtClass: 'col-lg-6 col-md-6', rtInnerClass: '',
    alarmClass: 'col-lg-3 col-md-3 asset-right-align', alarmInnerClass: '',
    RTtableMaxView: false, AlarmMaxView: false
  };

  public selectedIndex = 0;
  public selectedAsset: any = null;
  private keysForSubscription = [];


  constructor(private appService: AppService,
    private route: ActivatedRoute,
    private router: Router,
    private socket: Socket,
    private assetDashboardService: AssetDashboardService) {
    // this.companyId = '84ca6adb-67c1-49b5-a89e-bd644c9f0aa7';
    this.companyId = this.appService.getActiveCompany().companyId;

  }

  ngOnInit() {
    this.routerParam = this.route.params.subscribe(params => {
      this.assetId = params['assetId']; // get the project id from parent class URl
      this.getAssetMapInfo();
    });


    this.getAssetDesc();

    this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.companyId = this.appService.getActiveCompany().companyId;
        this.getAssetDesc();
      }
    );

    this.socket.on('data', (data) => {
      if (data.status === 'Geo') {
        this.assetMapInfo = data;
      }else if (data.status === 'Realtime') {
        this.assetTableInfo = data;
      }else {
        console.log(data);
        this.assetAlarmInfo = data;
      }

    });


  }


  getAssetDesc() {
    this.assetDashboardService.getAssetDesc(this.companyId)
      .subscribe(response => {
        // this.assetDescInfo = response ? response.find(desc => desc.assetId === this.assetId) : null;
        // console.log(this.assetDescInfo);
        this.assetDescInfo = response ? response : null;
        if (this.assetId) {
          this.selectedIndex = _.indexOf(this.assetAlarmInfo, _.findWhere(this.assetAlarmInfo, { 'aId': this.assetId }));
        }
        this.selectedAsset = this.assetDescInfo[this.selectedIndex];
        this.subscribeForAsset();
      })
  }

  getAssetMapInfo() {
    this.assetDashboardService.getAssetMapInfo(this.companyId)
      .subscribe(response => {
        response;
        this.assetMapInfo = response ? response.find(mapInfo => mapInfo.AssetId === this.assetId) : null;
        // this.assetMapInfo = response[0];
        if (this.assetAlarmInfo && this.assetAlarmInfo.length > 0) {
          this.getAssetTableInfo();
          this.getAssetAlaramInfo();
        }
      });
  }

  getAssetTableInfo() {
    this.assetDashboardService.getAssetTableInfo({
      companyId: this.companyId,
      gwSno: this.assetMapInfo.gwSNO,
      assetId: this.assetId
    })
      .subscribe(response => {
        this.assetTableInfo = response;
      });

  }
  getAssetAlaramInfo() {
    this.assetDashboardService.getAssetAlaramInfo({
      companyId: this.companyId,
      gwSno: this.assetMapInfo.gwSNO, assetId: this.assetId
    })
      .subscribe(response => {
        this.assetAlarmInfo = response ? response : [];
        if (this.assetId) {
          this.selectedIndex = _.indexOf(this.assetAlarmInfo, _.findWhere(this.assetAlarmInfo, { 'aId': this.assetId }));
        }
        this.selectedAsset = this.assetAlarmInfo[this.selectedIndex];
      });

  }
  changeRTtableView(tableView: boolean) {

    this.tableviewChange.selRTtableView = tableView;
    this.tableViewClass();
  }
  changeACtableView(tableView: boolean) {
    this.tableviewChange.selAlarmView = tableView;
    this.tableViewClass();
  }

  changeRTMaxView(view: boolean) {
    this.tableviewChange.RTtableMaxView = view;

    if (this.tableviewChange.RTtableMaxView) {
      this.tableviewChange.rtClass = 'col-lg-12 col-md-12';
      this.tableviewChange.rtInnerClass = '';
    } else {
      this.tableviewChange.selRTtableView = true;
      this.tableviewChange.selAlarmView = true;
      this.tableViewClass();
    }
  }
  changeACMaxView(view: boolean) {
    this.tableviewChange.AlarmMaxView = view;
    if (this.tableviewChange.AlarmMaxView) {
      this.tableviewChange.alarmClass = 'col-lg-12 col-md-12';
      this.tableviewChange.alarmInnerClass = '';
    } else {
      this.tableviewChange.selRTtableView = true;
      this.tableviewChange.selAlarmView = true;
      this.tableViewClass();
    }
  }

  tableViewClass() {
    if (!this.tableviewChange.selRTtableView && !this.tableviewChange.selAlarmView) {
      this.tableviewChange.selRTtableView = true;
      this.tableviewChange.selAlarmView = false;
    }

    if (this.tableviewChange.selRTtableView && this.tableviewChange.selAlarmView) {
      this.tableviewChange.rtClass = 'col-lg-6 col-md-6';
      this.tableviewChange.rtInnerClass = '';
      this.tableviewChange.alarmClass = 'col-lg-3 col-md-3 asset-right-align';
      this.tableviewChange.alarmInnerClass = '';
    } else if (!this.tableviewChange.selRTtableView && this.tableviewChange.selAlarmView) {
      this.tableviewChange.rtClass = 'col-lg-1 col-md-1 width-0-4';
      this.tableviewChange.rtInnerClass = '';
      this.tableviewChange.alarmClass = 'col-lg-8 col-md-8 pl-10';
      this.tableviewChange.alarmInnerClass = 'row mr-8';
    } else if (this.tableviewChange.selRTtableView && !this.tableviewChange.selAlarmView) {
      this.tableviewChange.rtClass = 'col-lg-8 col-md-8 pl-10';
      this.tableviewChange.rtInnerClass = 'mr-7';
      this.tableviewChange.alarmClass = 'col-lg-1 col-md-1 width-0-4 alarm-mp';
      this.tableviewChange.alarmInnerClass = '';
    }
  }

  changeDate(data: string[]) {
    this.dateInfo = data;
    // console.log(this.dateInfo);
  }
  changeTime(data: number[]) {
    this.timeInfo = data;
    // console.log(this.timeInfo);

  }
  changeRTData(data: string[]) {
    this.selectedRealTimeData = data;
    this.getHistoricalData();

  }
  getHistoricalData() {
    if (this.selectedRealTimeData.length > 0) {
      this.changeRTMaxView(false);

      let startdate = `${this.dateInfo[0]} ${this.timeInfo[0]}:00:00`;
      let enddate = `${this.dateInfo[1]} ${this.timeInfo[1]}:00:00`;
      // console.log(startdate);
      this.assetDashboardService.getHistoricalData({
        companyId: this.companyId,
        gwSno: this.assetMapInfo.gwSNO, assetId: this.assetId,
        datapointname: this.selectedRealTimeData[0],
        startDate: startdate,
        endDate: enddate
      }).subscribe(response => {

      });
    }
  }

  ngOnDestroy() {
    this.routerParam.unsubscribe();
  }

  getAssetSelected(selectedIndex) {
    this.selectedAsset = this.assetDescInfo[selectedIndex];
    this.subscribeForAsset();
  }

  subscribeForAsset() {
    let company = this.appService.getActiveCompany();
    this.keysForSubscription = [];
    this.keysForSubscription.push(`realtime_${company.name.split(' ').join('')}_${this.selectedAsset.gwSNO}_${this.selectedAsset.AssetId}`);
    this.keysForSubscription.push(`geo_${company.name.split(' ').join('')}_${this.selectedAsset.gwSNO}_${this.selectedAsset.AssetId}`);
    this.keysForSubscription.push(`alarm_${company.name.split(' ').join('')}_${this.selectedAsset.gwSNO}_${this.selectedAsset.AssetId}`);
    this.socket.emit('subscribe', this.keysForSubscription);
  }

}
