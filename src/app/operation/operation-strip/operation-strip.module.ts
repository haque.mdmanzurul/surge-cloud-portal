import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OperationStripComponent } from './operation-strip.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [OperationStripComponent],
  exports : [OperationStripComponent]
})
export class OperationStripModule { }
