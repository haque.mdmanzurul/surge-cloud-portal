import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationStripComponent } from './operation-strip.component';

describe('OperationStripComponent', () => {
  let component: OperationStripComponent;
  let fixture: ComponentFixture<OperationStripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationStripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationStripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
