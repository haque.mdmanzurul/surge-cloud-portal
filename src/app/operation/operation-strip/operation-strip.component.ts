import { Component, OnInit } from '@angular/core';
import { INavigation } from '../../';
import { AppStartService } from '../../';
import * as _ from 'underscore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-operation-strip',
  templateUrl: './operation-strip.component.html',
  styleUrls: ['./operation-strip.component.scss']
})
export class OperationStripComponent implements OnInit {
  private svgIcons: string[] = [
		// 'icon_AdvancedAanalytics.svg#Vector_Smart_Object_copy',
    'icon_BasicAanalytics',
    'icon_GeofenceTracking',
    'icon_EventsAlarms',
    'icon_AssetDashboard',
    'icon_RoutingDispatch',
    'icon_Reports',
    'icon_ConnectedDiagnostics',
    'icon_ConnectedCallibration',
    'icon_SafetyCompliance',
    'icon_ServiceManagement',
  ];
  private currentRoute: string ;
  private features: INavigation[] = [];

  constructor(private appStartService: AppStartService,
    private router: Router) {
    this.currentRoute = (this.router.url).substr(1);
    console.log(this.currentRoute);
  }

  ngOnInit() {
    let i = 0;
    const tempData = _.sortBy(this.appStartService.feature, 'sortOrder');
    for (const feature of tempData) {

      feature.imageIcon = `${feature.imageIcon}${this.svgIcons[i]}.svg#Vector_Smart_Object`;
      // feature.imageHoverIcon = `${feature.imageHoverIcon}${this.svgIcons[i]}_mouseover.svg#Vector_Smart_Object`;
      // feature.color = this.cardColors[i++];
      feature.enabled = feature.url === this.currentRoute ? true : false; // active
      this.features.push(feature);
      i++;
    }
  }

  goto(feature: INavigation) {
    console.log(feature.url);
    this.router.navigate([feature.url]);
  }

}
