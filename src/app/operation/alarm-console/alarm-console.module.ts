import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { AlarmConsoleComponent } from './alarm-console.component';
import { AlarmConsoleService } from './alarm-console.service';
import { OperationStripModule } from '../';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../../core';
import { SqueezeBoxModule } from 'squeezebox';
import { ModalModule } from 'ngx-bootstrap';
import { Ng2FileInputModule } from 'ng2-file-input'; // <-- import the module

import { ApplicationAttachmentComponent } from './application-attachment/application-attachment.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    CoreModule,
    OperationStripModule,
    SqueezeBoxModule,
    ModalModule.forRoot(),
    Ng2FileInputModule.forRoot()
  ],
  declarations: [AlarmConsoleComponent, ApplicationAttachmentComponent],
  exports: [AlarmConsoleComponent],
  providers: [AlarmConsoleService]
})
export class AlarmConsoleModule { }
