import { Injectable } from '@angular/core';
import { HttpService } from '../../shared-module';
import { Observable } from 'rxjs/Observable';
import { IValues } from './alarm-console';
import { AppService } from '../../app.service';
import * as moment from 'moment';
@Injectable()
export class AlarmConsoleService {

  errorType: IValues[] = [
    { key: 'Accident', value: 'Accident', checked: false },
    { key: 'Over speeding', value: 'Over speeding', checked: false },
    { key: 'Geofence crossing', value: 'Geofence crossing', checked: false }
  ];

  alarmConsole: IValues[] = [
    { key: 'Alarms', value: 'Alarms', checked: false },
    { key: 'Acknowledged', value: 'Acknowledged', checked: false },
    { key: 'Notified', value: 'Notified', checked: false }
  ];

  attachments : any = []

  constructor(private httpService: HttpService, private appService: AppService) { }

  getFacility(data?: string): Observable<any[]> {
    return this.httpService.get(`facility/api/facility/get/${data}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getAssetAlaramInfo(obj: { companyId: string, gwSno: string, assetId: string }): Observable<any> {
    let url = '';
    if (obj.gwSno && obj.assetId) {
      url = `device/api/iot/getalarmdata?companyId=${obj.companyId}${'&gwSno=' + obj.gwSno}&assetId=${obj.assetId}`;
    } else {
      url = `device/api/iot/getalarmdata?companyId=${obj.companyId}`;
    }
    return this.httpService.get(encodeURI(url))
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

  getAlarmNoteInfo(obj: { companyId: string, gwSno: string, assetId: string }): Observable<any> {
    let url = '';
    if (obj.gwSno && obj.assetId) {
      url = `device/api/iot/getalarmnotes?companyId=${obj.companyId}${'&gwSno=' + obj.gwSno}&assetId=${obj.assetId}`;
    } else {
      url = `device/api/iot/getalarmdata?companyId=${obj.companyId}`;
    }
    return this.httpService.get(encodeURI(url))
        .map(res => res.json())
        .catch(err => Observable.throw(err));
  }

  getAssetInfo(): Observable<any> {
    return this.httpService.get(encodeURI(`device/api/iot/getassettype`))
      .map(res => res.json())
      .catch(err => Observable.throw(err));

  }

  getAlarmNotes(gwSno: string, assetId: string ) {
    let companyId = this.appService.getActiveCompany().companyId;
    let url  = `device/api/iot/getalarmnotes?companyId=${companyId}${'&gwSno=' + gwSno}&assetId=${assetId}`;
    return this.httpService.get(encodeURI(url))
        .map(res => res.json())
        .catch(err => Observable.throw(err));
  }

  uploadFileReference(fileToUpload:any, gwSno, assetId) {

    let input = new FormData();
    input.append("SelectedFile", fileToUpload);
    input.append('gwSNO', gwSno);
    input.append('AssetId', assetId);
    input.append('OccurrenceDate', moment().format('YYYY/DD/MM HH:mm:ss'));

    return this.httpService.post("device/api/iot/addalarmfileref", input)
        .map(res =>res.json())
        .catch(err => Observable.throw(err));
  }



}
