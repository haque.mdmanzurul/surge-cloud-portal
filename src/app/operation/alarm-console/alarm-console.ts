export class Address {
    public name: string;
    public street1: string;
    public street2: string;
    public city: string;
    public stateCode?: string;
    public postcode: string;
    public countryCode: string;
    public latitude: number;
    public longitude: number;
    constructor(private address: any = {}) {
        this.name = address.name || '';
        this.street1 = address.street1 || '';
        this.street2 = address.street2 || '';
        this.city = address.city || '';
        this.stateCode = address.stateCode || '';
        this.postcode = address.postCode || '';
        this.countryCode = address.countryCode || '';
        this.latitude = address.latitude || 0;
        this.longitude = address.longitude || 0;
    }
}

export class Facility {
    public facilityId: string;
    public name: string;
    public companyId: string;
    public companyName: string;
    public logo: string;
    public address: IAddress;
    public timezone: string;
    public status: boolean | number;
    public tags: string;
    public checked: boolean = false;
    constructor(public data: any = {}) {
        this.facilityId = data.facilityId || '';
        this.name = data.name || '';
        this.companyId = data.companyId || '';
        this.companyName = data.companyName || '';
        this.logo = data.logo || '';
        this.address = new Address(data.address);
        this.timezone = data.timezone || '';
        this.status = data.status || 0;
        this.tags = data.tags || [];
    }
}

export interface IAddress {
    name: string;
    street1: string;
    street2?: string;
    city: string;
    stateCode?: string;
    countryCode: string;
    postcode: string;
    latitude?: number;
    longitude?: number;
}

export interface IValues {
    key: string,
    value: string,
    checked: boolean
}

export interface IAsset {
    Name: string,
    Status: string
}

export class Asset implements IAsset {
    public Name: string;
    public Status: string
    public checked: boolean = false;
    constructor(d: any = {}) {
        this.Name = d.Name || null;
        this.Status = d.Status || null;
    }
}

export class AlarmNotes {
    constructor(
        public notes: string,
        public status: string
    ) {
        this.notes = notes;
        this.status = status;
    }
}