import { TestBed, inject } from '@angular/core/testing';

import { AlarmConsoleService } from './alarm-console.service';

describe('AlarmConsoleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AlarmConsoleService]
    });
  });

  it('should ...', inject([AlarmConsoleService], (service: AlarmConsoleService) => {
    expect(service).toBeTruthy();
  }));
});
