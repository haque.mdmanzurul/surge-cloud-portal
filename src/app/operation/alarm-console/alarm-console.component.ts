import { Component, OnInit, ElementRef, Renderer, ViewChild } from '@angular/core';
import { AlarmConsoleService } from './alarm-console.service';
import { AppService } from '../../app.service';
import { Facility, IValues, Asset, AlarmNotes } from './alarm-console';
import { AssetDashboardTable } from '../asset-dashboard/asset-dashboard';
import { OperationStripComponent } from '../';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import * as _ from 'underscore';

import { Socket } from 'ng-socket-io';

//import { ModalDirective } from 'ngx-bootstrap/modal';
import {PopupComponent} from "../../core/popup/popup.component";


@Component({
  selector: 'app-alarm-console',
  templateUrl: './alarm-console.component.html',
  styleUrls: ['./alarm-console.component.scss', './accordian.css'],

})
export class AlarmConsoleComponent implements OnInit {
  @ViewChild('body') bodyEl: ElementRef;
  //@ViewChild('childModal') public childModal:ModalDirective;
  @ViewChild("fileInput") fileInput;

  public facility: Facility[] = [];
  public companyId: string;
  public errorType: IValues[];
  public alarmConsole: IValues[];
  public gwSno: string;
  public assetAlarmInfo: AssetDashboardTable[] = [];
  public assetAlarmAvailable: AssetDashboardTable[] = [];
  public asset: Asset[] = [];
  public assetId: string = null;
  private svgIcons: string[] = [
    'assets/icons/AlarmConsole/Alarms.svg#Alarms',
    'assets/icons/AlarmConsole/Acknowledged.svg#Acknowledged',
    'assets/icons/AlarmConsole/Notified.svg#Notified'
  ];

  public filter: any = {
    facility: {
      enabled: false,
      keyword: '',
      selected: [],
      filtered: []
    },
    error: {
      enabled: false,
      selected: [],
      filtered: []
    },
    asset: {
      enabled: false,
      selected: [],
      filtered: []
    },
    alarm: {
      enabled: false,
      selected: [],
      filtered: []
    }
  };

  public alarmModal:PopupComponent;
  public alarmNotes: AlarmNotes = new AlarmNotes('', 'Alarm');
  private keysForSubscription = [];

  selectedAlarmInfo : any = null;
  constructor(private alarmConsoleService: AlarmConsoleService, private socket: Socket,
              private appService: AppService, public router: Router, private route: ActivatedRoute,
              private renderer: Renderer) {

    this.errorType = this.alarmConsoleService.errorType;
    this.alarmConsole = this.alarmConsoleService.alarmConsole;
  }

  ngOnInit() {
    console.log('Test function is running')
    this.route.paramMap.subscribe(
      (mapped: any) => {
        if (mapped.params) {
          this.gwSno = mapped.params['gwSNO'] ? mapped.params['gwSNO'] : this.gwSno;
          this.assetId = mapped.params['assetId'] ? mapped.params['assetId'] : this.assetId;
        }
        this.getAssetAlaramInfo();
      }
    );

    this.appService.activeCompanyInfo.subscribe(
      (company) => {
        this.companyChanged();
      }
    );

    this.getAssetInfo();
    this.companyChanged();

    this.socket.on('data', (data) => {
      //console.log(data);
      // this.assetAlarmInfo.push(data);
      if (data.status != 'Geo' && data.status != "Realtime") {
        // this.realTime.next(data);
        //let ind = _.indexOf(this.assetAlarmInfo, _.findWhere(this.assetAlarmInfo, { assetId: data.assetID }));
        // this.assetAlarmInfo
      }
    });

  }

  getFacility() {
    this.alarmConsoleService.getFacility(this.appService.getActiveCompany().companyId).subscribe(
      (data) => {
        data.forEach((fac) => {
          fac.address = fac.address ? fac.address : {};
          this.facility.push(new Facility(fac));
        });
      });
  }

  getAssetAlaramInfo() {
    this.alarmConsoleService.getAssetAlaramInfo({
      companyId: this.appService.getActiveCompany().companyId,
      gwSno: this.gwSno,
      assetId: this.assetId
    }).subscribe(response => {
      this.assetAlarmAvailable = response;
      this.assetAlarmInfo = response ? response : [];

      if(this.assetAlarmInfo.length > 0) {
        this.assetAlarmInfo.forEach(
            (alarm) => {

              this.alarmConsoleService.getAlarmNotes(alarm.gwSno, alarm.AssetId).subscribe(response => {
                alarm['notes'] = response ? response : [];
              });
            })
      }

      if (!this.assetId && !this.gwSno) {
        let company = this.appService.getActiveCompany();
        this.assetAlarmAvailable.forEach(
          (value) => {
            console.log(value);
            this.keysForSubscription.push(`geo_${company.name.split(' ').join('')}_${value.gwSno}_${value.AssetId}`);
            this.keysForSubscription.push(`realtime_${company.name.split(' ').join('')}_${value.gwSno}_${value.AssetId}`);
            this.keysForSubscription.push(`alarm_${company.name.split(' ').join('')}_${value.gwSno}_${value.AssetId}`);
          });
        this.subscribeForAlarm();
      }
    });
  }

  getAssetInfo() {
    this.alarmConsoleService.getAssetInfo()
      .subscribe(response => {
        this.asset = [];
        response.forEach(asset => {
          this.asset.push(new Asset(asset))
        })
      })
  }

  companyChanged() {
    this.getFacility();
    this.getAssetAlaramInfo();

    if (this.assetId && this.gwSno) {
      let company = this.appService.getActiveCompany();
      this.keysForSubscription = [];
      this.keysForSubscription.push(`geo_${company.name.split(' ').join('')}_${this.gwSno}_${this.assetId}`);
      this.keysForSubscription.push(`realtime_${company.name.split(' ').join('')}_${this.gwSno}_${this.assetId}`);
      this.keysForSubscription.push(`alarm_${company.name.split(' ').join('')}_${this.gwSno}_${this.assetId}`);
      this.subscribeForAlarm();

    }

  }

  displaySearch(){

  }

  applyFilter() {

     /*this.filter.facility.selected = this.filter.facility.enabled ? this.filter.facility.selected = _.pluck(_.where(this.filter.facility, { enabled: true }), 'name') : this.filter.facility.selected = [];
     this.filter.error.selected = this.filter.error.enabled ? this.filter.error.selected = _.pluck(_.where(this.errorType, { checked: true }), 'value') : this.filter.error.selected = [];
     this.filter.asset.selected = this.filter.asset.enabled ? this.filter.asset.selected = _.pluck(_.where(this.asset, { checked: true }), 'Name') : this.filter.asset.selected = [];
     this.filter.alarm.selected = this.filter.asset.enabled ? this.filter.alarm.selected = _.pluck(_.where(this.alarmConsole, { checked: true }), 'value') : this.filter.alarm.selected = [];


    if (this.filter.facility.enabled) {
       this.filter.facility.filtered = _.filter(this.assetAlarmAvailable, (asset) => this.filter.facility.selected.indexOf(asset.name) > -1);
     }

     if (this.filter.error.enabled) {
       this.filter.error.filtered = _.filter(this.assetAlarmAvailable, (asset) => this.filter.error.selected.indexOf(asset.value) > -1);
     }

     if (this.filter.asset.enabled) {
       this.filter.asset.filtered = _.filter(this.assetAlarmAvailable, (asset) => this.filter.asset.selected.indexOf(asset.Name) > -1);
     }

     if (this.filter.alarm.enabled) {
       this.filter.alarm.filtered = _.filter(this.assetAlarmAvailable, (asset) => this.filter.alarm.selected.indexOf(asset.value) > -1);
     }

    this.assetAlarmInfo = _.uniq([...this.filter.facility.filtered, ...this.filter.error.filtered, ...this.filter.asset.filtered, ...this.filter.alarm.filtered])
  */}

  saveAlarmNotes(alarm) {
    let fi = this.fileInput.nativeElement;
    if (fi.files && fi.files[0]) {
      let fileToUpload = fi.files[0];
      this.alarmConsoleService.uploadFileReference(fileToUpload, alarm.gwSno, alarm.assetId)
          .subscribe(res => {
            console.log(res);
          });
    }
  }

  subscribeForAlarm() {
    this.socket.emit("subscribe", this.keysForSubscription);
  }
  test(){
    console.log('Test function is running')
  }

  openAlarmNoteForm(alarm) {
      console.log('Clicked Alarm Data', alarm);
      this.alarmConsoleService.getAlarmNoteInfo({
          companyId: this.appService.getActiveCompany().companyId,
          gwSno: alarm.gwSno,
          assetId: alarm.AssetId
      }).subscribe(response => {
          console.log('Response', response);
          this.selectedAlarmInfo = response ? response : [];;
          //this.childModal.show();
      });
  }

  hideAlarmNoteForm() {
        this.selectedAlarmInfo = null;
        //this.childModal.hide();
  }

  onFileChange(file) {
    console.log(file.files);
  }

}
