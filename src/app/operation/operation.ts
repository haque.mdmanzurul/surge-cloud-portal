export class GeofenceFilter {
    public event: string = '';
    public facility: string = '';
    public geofence: string = '';
    public utilities: number = 1;
    public vin: string = '';
    public location: string = '';
    public showZones: Boolean = false;
    public assetType: string = '';
}

export class GeoData {
    constructor(
        public gwSno: string,
        public AssetId: string,
        public OccurrenceDate: string,
        public params: any[]
    ) {
        this.gwSno = gwSno;
        this.AssetId = AssetId;
        this.OccurrenceDate = OccurrenceDate;
        this.params = params;
    }
}