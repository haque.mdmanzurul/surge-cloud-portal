import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OperationComponent } from './operation.component';
import { GeoFencingComponent } from './geo-fencing';
import { HomeComponent, CompanyResolverService } from '../app-common';
import { AssetDashboardComponent } from './asset-dashboard';
import { AlarmConsoleComponent } from './alarm-console';
import { OperationStripComponent } from './operation-strip/operation-strip.component';
import { AuthenticationGuard } from '../authentication.guard';

const routes: Routes = [];
// const routes: Routes = [
//   {
//     path: 'operation',
//     component: HomeComponent,
//     resolve: {
//       company: CompanyResolverService
//     },
//     canActivate: [AuthenticationGuard],
//     children: [
//       {
//         path: 'geo-fencing',
//         component: GeoFencingComponent,
//       },
//       {
//         path: 'asset-dashboard/:assetId',
//         component : AssetDashboardComponent
//       },
//       {
//         path : 'alarm',
//         component : AlarmConsoleComponent
        
//       }
//     ]
//   }
// ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OperationRoutingModule { }
