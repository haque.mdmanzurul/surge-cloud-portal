import { Injectable } from '@angular/core';
import { HttpService } from '../shared-module';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class OperationService {

	public numberOfRecordPerPage : number =3;	
	
  constructor(private httpService: HttpService) { }

  getAssets(companyId: string): Observable<any> {
    return this.httpService.get(`device/api/iot/GetGeofenceData/${companyId}`)
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

}
