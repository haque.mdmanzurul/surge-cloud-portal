import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { OperationRoutingModule } from './operation-routing.module';
import { OperationComponent } from './operation.component';
import { GeoFencingModule } from './geo-fencing';
import { OperationService } from './operation.service';
import { AssetDashboardModule } from './asset-dashboard/asset-dashboard.module';
import { AlarmConsoleModule } from './alarm-console/alarm-console.module';
import { OperationStripModule } from './operation-strip/operation-strip.module';

import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { environment } from '../../environments/environment';

const config: SocketIoConfig = { url: environment.socket.alarm, options: {} };

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    OperationRoutingModule,
    GeoFencingModule,
    AssetDashboardModule,
    AlarmConsoleModule,
    OperationStripModule,
    SocketIoModule.forRoot(config)
  ],
  declarations: [OperationComponent],
  providers: [OperationService],
  exports: [OperationComponent],
  bootstrap: [OperationComponent]
})
export class OperationModule { }
