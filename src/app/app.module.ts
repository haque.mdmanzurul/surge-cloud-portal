import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';
import { CoreModule } from './core/core.module';
import { AppCommonModule } from './app-common/app-common.module';
import { AdminModule } from './admin/admin.module';
import { OperationModule } from './operation/operation.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { environment } from '../environments/environment';
import { AppStartService, startupServiceFactory } from './app-start.service';
import { AppService } from './app.service';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgProgressModule } from 'ng2-progressbar';
import { DataTableModule } from "angular2-datatable";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AuthenticationGuard } from './authentication.guard';
//import { NouisliderModule } from 'ng2-nouislider';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    NgProgressModule,
    CoreModule,
    AppCommonModule,
    AdminModule,
    OperationModule,
    //NouisliderModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleApiKey,
      libraries: ["places", "drawing"]
    }),
    SimpleNotificationsModule.forRoot(),
    AppRoutingModule,
    DataTableModule,
    NgbModule.forRoot()
  ],
  providers: [
    AppStartService,
    {
      provide: APP_INITIALIZER,
      useFactory: startupServiceFactory,
      deps: [AppStartService],
      multi: true
    },
    AppService,
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    AuthenticationGuard,
    GoogleMapsAPIWrapper
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
