import { TestBed, inject } from '@angular/core/testing';

import { AppStartService } from './app-start.service';

describe('AppStartService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppStartService]
    });
  });

  it('should ...', inject([AppStartService], (service: AppStartService) => {
    expect(service).toBeTruthy();
  }));
});
