import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpService } from './shared-module';
import { Navigation, INavigation } from './app';
/**
 * @class
 * @description
 * This service injected to root module and will call before starting our application
 * call load() method to get the details from backend
 * @var _startupData is the variable where you can get the data from server
 * @author kesavan.r
 */

@Injectable()
export class AppStartService {

  private _features: INavigation[] = [];
  private _sidebar: INavigation[] = [];
  private _feature: INavigation[] = [];
  constructor(public httpservice: HttpService) { }
  /**
   * @method
   * @description
   * The following load() method will call from provider factory
   * It's should be promise
   */
  load(): Promise<any> {
    return this.httpservice
      .get('users/api/roles/getfeatures')
      .retry(3)
      .map((res: Response) => res.json())
      .toPromise()
      .then((features: INavigation[]) => {
        this._features = features;
      });
  }

  get feature(): INavigation[] {
    this._feature = [];
    this._features.forEach((feature) => {
      let model = new Navigation(feature);
      if (model.controlLevel === 2) {
        this._feature.push(model);
      }
    });
    return this._feature;
  }

  get sidebar(): INavigation[] {
    this._sidebar = [];
    this._features.forEach((feature) => {
      let model = new Navigation(feature);
      if (model.controlLevel === 1) {
        this._sidebar.push(model);
      }
    });
    return this._sidebar
  }

  get features(): INavigation[] {
    return this._features;
  }

}
/**
 * @function
 * @description
 * This is the factory is used to inject APP_INITILIZED provider for root module
 * @param {StartupService}
 * @author kesavan.r
 */
export function startupServiceFactory(startupService: AppStartService): Function {
  return (): Promise<any> => startupService.load().then(() => Promise.resolve());
}
