import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
import { HttpService } from './services/http/http.service';
import { WebsocketService } from './services/websocket/websocket.service';
import { SortByPipe } from './pipes/sort-by.pipe';
import { WizardComponent } from './components';
// import { MaterialModule } from '@angular/material';
import { TabsComponent } from './components/tabs/tabs.component';
import { TabHeadingDirective } from './components/tabs/tab-heading.directive';
import { TabDirective } from './components/tabs/tab.directive';
import { NgTransclude } from './components/tabs/utils.directive';
import { NgProgressService } from 'ng2-progressbar';
import { AdalService } from './services/adal/adal.service';


export function httpServiceFactory(backend: XHRBackend, defaultOptions: RequestOptions, NgProgressService: NgProgressService) {
  return new HttpService(backend, defaultOptions, NgProgressService);
}

@NgModule({
  imports: [
    CommonModule,
    // MaterialModule
  ],
  declarations: [
    SortByPipe,
    WizardComponent,
    TabsComponent,
    TabHeadingDirective,
    TabDirective,
    NgTransclude
  ],
  exports: [
    SortByPipe,
    WizardComponent,
    NgTransclude,
    TabHeadingDirective,
    TabsComponent,
    TabDirective
  ],
  providers: [
    HttpService,
    WebsocketService,
    NgProgressService,
    {
      provide: HttpService,
      useFactory: httpServiceFactory,
      deps: [XHRBackend, RequestOptions, NgProgressService]
    },
    AdalService
  ]
})
export class SharedModule { }
