import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
// https://blogs.msdn.microsoft.com/premier_developer/2017/04/26/using-adal-with-angular2/
declare const adal: any;
declare const AuthenticationContext: any;
let createAuthContextFn:any = AuthenticationContext; 


@Injectable()
export class AdalService {

  private context: any;
  public adalConfig: any = {
    tenant: environment.adal.tenant,
    clientId: environment.adal.clientId,
    expireOffsetSeconds: 600,
    redirectUri: window.location.origin + '/',
    postLogoutRedirectUri: window.location.origin + '/',
    popUp: false,
    cacheLocation: "localStorage"
  };

  constructor() { 
    this.context = new createAuthContextFn(this.adalConfig);
  }

  login() {
    this.context.login();
  }

  logout() {
    this.context.logOut();
  }

  handleCallback() {
    this.context.handleWindowCallback();
  }

  public get userInfo() {
    return this.context.getCachedUser();
  }

  public get accessToken() {
    return this.context.getCachedToken(this.adalConfig.clientId);
  }

  public get isAuthenticated() {
    return this.userInfo && this.accessToken;
  }

}
