import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IWizardItem } from './wizard';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss']
})
export class WizardComponent implements OnInit {
  private _items: IWizardItem[] = [];
  @Output() onselect = new EventEmitter<{ name: string, icon: string, index: number }>();
  private _index: number = 0;
  private _clickable: Boolean = true;

  @Input() set clickable(clickable: Boolean) {
    this._clickable = clickable;
  }

  @Input('items') set availableItem(items: IWizardItem[]) {
    items.forEach((item) => {
      this._items.push({ ...item, active: false, success: false, error: false });
    });
  }

  constructor() { }

  ngOnInit() {
  }

  itemClicked(item: IWizardItem, index: number, fromInteral?:Boolean) {
    if (this._clickable || fromInteral) {
      this._index = index;
      this.onselect.emit({ name: item.name, icon: item.icon, index: index });
    }
  }

  get index(): number {
    return this._index;
  }

  setComplete(completed: Boolean) {
    if (completed) {
      this._items[this._index].success = completed;
      this._items[this._index].error = false;
      this.itemClicked(this._items[this._index + 1], this._index + 1, true);
      // this._index = this._index + 1;
      // let item = this._items[this._index + 1];
      // this.onselect.emit({ name: item.name, icon: item.icon, index: this._index });
    } else {
      this._items[this._index].success = false;
      this._items[this._index].error = !completed;
    }
  }

  gotoPrevious() {
    this._items[this._index].success = false;
    this._items[this._index].error = false;
    this.itemClicked(this._items[this._index - 1], this._index - 1);
  }
}
