export interface IWizardItem {
    name: string,
    icon: string,
    activeIcon?: string,
    active?: Boolean,
    success?: Boolean,
    error?: Boolean,
    clickable?: Boolean,
    index?:number,
    disabled?:Boolean,
    statusIcon?:string,
    tabId?:string
}