import { Directive, TemplateRef } from '@angular/core';
import { TabDirective } from './tab.directive';

@Directive({
  selector: '[tab-heading]'
})
export class TabHeadingDirective {

  constructor(public templateRef: TemplateRef<any>, public tab: TabDirective) {
    tab.headingRef = templateRef;
  }

}
