import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'arrayFilter'
})
/**
	Filter the Array of object based on the multiple key-value pair

*/
export class ArrayFilterPipe implements PipeTransform {

	transform(items: Array<any>, filter?: any, equalStatusParam?: boolean): any {

		// default it will check the object's all key ,value matching

		let equalStatus = equalStatusParam === undefined ? true : equalStatusParam;

		if (filter && items) {

			if (equalStatus) { // it will check everything (key,value) is same in array
				
				return items.filter(item => {

					let notMatchingField = Object.keys(filter)
						.find(key => {
							return item[key] !== filter[key];
						});

					return !notMatchingField; // true if matches all fields
				});
			} else { // it will check everything(key,value) is not same

				return items.filter(item => {

					let notMatchingField = Object.keys(filter)
						.find(key => {
							return item[key] == filter[key];
						});

					return !notMatchingField; // true if matches all fields
				});

			}


		}
		else {
			return items;
		}

	}
}

