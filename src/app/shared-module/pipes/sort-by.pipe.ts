import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortBy'
})
export class SortByPipe implements PipeTransform {

  transform(array: any, filterFrom: any): any {
    array.sort((a, b) => {
      let fromValues = filterFrom.map(f => f.value);
      return fromValues.indexOf(a.value) < fromValues.indexOf(b.value) ? -1 : 1;
    });
  }

}
