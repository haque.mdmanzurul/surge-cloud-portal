
export const defaultImageCompany = {
    defaultImage :'/assets/images/default-org.png'
}
export interface INavigation {
    controlLevel: number;
    featureId: number;
    name: string;
    sortOrder: number;
    status: number;
    imageIcon?: string;
    imageHoverIcon?: string
    color?: string;
    url?: string;
    enabled?: Boolean;
}

export class Navigation implements INavigation {
    public controlLevel: number;
    public featureId: number;
    public name: string;
    public sortOrder: number;
    public status: number;
    public imageIcon?: string;
    public imageHoverIcon?: string;
    public color?: string;
    public url?= '';
    constructor(public data: any) {
        this.controlLevel = data.ControlLevel || 0;
        this.featureId = data.FeatureId || 0;
        this.name = data.Name || '';
        this.sortOrder = data.SortOrder || 0;
        this.status = data.Status || 0;
        this.imageIcon = 'assets/icons/' // data.ImageIcon;
        this.imageHoverIcon = 'assets/icons/' // data.ImageIcon;
        this.color = data.color || '';
        this.url = data.Url || '';
    }
}

export interface ICompany {
    companyId: string;
    address: IAddress;
    contact: IContact;
    createdBy: Date;
    currencyCode: string;
    emailId: string;
    lastUpdatedDate: Date;
    logo: string;
    name: string;
    parentCompany: string;
    phones: string[];
    status: number | boolean;
    tags: any[];
    updatedBy: Date;
    website: string;
    groupId: string;
}

export class Company {
    public companyId: string;
    public address: IAddress;
    public contact: IContact;
    public createdBy: Date;
    public currencyCode: string;
    public emailId: string;
    public lastUpdatedDate: Date;
    public logo: string;
    public name: string;
    public parentCompany: string;
    public phones: string[];
    public status: number | boolean;
    public tags: any[];
    public updatedBy: Date;
    public website: string;
    public groupId: string;
    constructor(data: any) {
        this.companyId = data.companyId || '';
        this.address = data.address ? new Address(data.address) : null;
        this.contact = data.contact ? new Contact(data.contact) : null;
        this.createdBy = data.createdBy || '';
        this.currencyCode = data.currencyCode || '';
        this.emailId = data.emailId || '';
        this.lastUpdatedDate = data.lastUpdatedDate ? new Date(data.lastUpdatedDate):null;
        this.logo = data.logo || defaultImageCompany.defaultImage;
        this.name = data.name || '';
        this.parentCompany = data.parentCompany || '';
        this.phones = data.phones || '';
        this.status = data.status || 0;
        this.tags = data.tags || [];
        this.updatedBy = data.updatedBy || '';
        this.website = data.website || '';
        this.groupId = data.groupId || '';
    }
}

export interface IAddress {
    city: string;
    countryCode: string;
    latitude: number;
    longitude: number;
    name: string;
    postcode: string;
    stateCode: string;
    street1: string;
    street2: string;
}

export class Address {
    public city: string;
    public countryCode: string;
    public latitude: number;
    public longitude: number;
    public name: string;
    public postcode: string;
    public stateCode: string;
    public street1: string;
    public street2: string;
    constructor(address: any) {
        this.city = address.city || '';
        this.countryCode = address.countryCode || '';
        this.latitude = address.latitude || 0;
        this.longitude = address.longitude || 0;
        this.name = address.name || '';
        this.postcode = address.postcode || '';
        this.stateCode = address.stateCode || '';
        this.street1 = address.street1 || '';
        this.street2 = address.street2 || '';
    }
}

export interface IContact {
    contactEmailId: string;
    contactMobileNumber: string;
    contactName: string;
    contactPhoneNumber: string;
    contactStatus: number;
}

export class Contact {
    public contactEmailId: string;
    public contactMobileNumber: string;
    public contactName: string;
    public contactPhoneNumber: string;
    public contactStatus: number;
    constructor(contact: any) {
        if (contact) {
            this.contactEmailId = contact.contactEmailId || '';
            this.contactMobileNumber = contact.contactMobileNumber || '';
            this.contactName = contact.contactName || '';
            this.contactPhoneNumber = contact.contactPhoneNumber || '';
            this.contactStatus = contact.contactStatus || '';
        }
    }
}

export class Features {
    constructor(public canAccess: number,
        public canAdd: number,
        public canDelete: number,
        public canUpdate: number,
        public featureId: string,
        public roleId: string) {
        this.canAccess = canAccess || 0;
        this.canAdd = canAdd || 0;
        this.canUpdate = canUpdate || 0;
        this.featureId = featureId || '';
        this.roleId = roleId || '';

    }
}