import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SingleChartData, PieChart } from '../ichart';
import * as _ from 'underscore';
@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {
  private pieChartInfo : PieChart = new PieChart();
  private activeEntries : any[] = [];
  private currentActiveEntry : SingleChartData;
  @Output() public selectedData : EventEmitter<SingleChartData> = new EventEmitter<SingleChartData>();
  @Input() public set clearActiveData(v : boolean) {
    if(!v){
      this.activeEntries = [];
    }
  }
  @Input() public set chartInfo (chart: any){
    
    chart = chart === undefined ? {} : chart;
    this.pieChartInfo = new PieChart(chart);
  };
  @Input() public data: SingleChartData[] = this.data ? this.data : [];
  constructor() { }

  ngOnInit() {
    // var self : PieChart= this.pieChartInfo;
  }
  onSelect(event) {
    
    if(typeof event === 'string'){
       let i = _.findIndex(this.data, function(i){ return i.name === event});
      this.currentActiveEntry = {name : event, value : this.data[i].value};
    }else{
      this.currentActiveEntry = event;
    }
     this.selectedData.emit(this.currentActiveEntry);
   // this.activeEntries.push(event);
  }
  onActivate(item) {
    console.log(item);
  }

  pieDeactivate($event){
    if(this.currentActiveEntry){
      this.activeEntries = [];
      this.activeEntries.push(this.currentActiveEntry);
    }
  }
   setLabelFormatting(c){
     // let value = this.pieChartInfo.valueAppend ? this.pieChartInfo.valueAppend : ' ';
    return `${c.data.name}<br/><span class="custom-label-text">${c.data.value}%</span>`;
  }

}
