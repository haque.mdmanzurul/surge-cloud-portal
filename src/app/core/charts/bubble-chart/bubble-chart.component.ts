import { Component, OnInit,Input } from '@angular/core';
import { BubbleChartData, MultiChartData, BubbleChart } from '../ichart';
@Component({
  selector: 'app-bubble-chart',
  templateUrl: './bubble-chart.component.html',
  styleUrls: ['./bubble-chart.component.scss']
})
export class BubbleChartComponent implements OnInit {

	single: BubbleChartData[] = [ // make it empty, testing data
  {
    "name": "Germany",
    "series": [
      {
        "name": "2010",
        "x": "2009",
        "y": 80.3,
        "r": 80.4
      },
      {
        "name": "2000",
        "x": "1999",
        "y": 80.3,
        "r": 78
      },
      {
        "name": "1990",
        "x": "1989",
        "y": 75.4,
        "r": 79
      }
    ]
  },
  {
    "name": "United States",
    "series": [
      {
        "name": "2010",
        "x": "2009",
        "y": 78.8,
        "r": 310
      },
      {
        "name": "2000",
        "x": "1999",
        "y": 76.9,
        "r": 283
      },
      {
        "name": "1990",
        "x": "1989",
        "y": 75.4,
        "r": 253
      }
    ]
  },
  {
    "name": "France",
    "series": [
      {
        "name": "2010",
        "x": "2009",
        "y": 81.4,
        "r": 63
      },
      {
        "name": "2000",
        "x": "1999",
        "y": 79.1,
        "r": 59.4
      },
      {
        "name": "1990",
        "x": "1989",
        "y": 77.2,
        "r": 56.9
      }
    ]
  },
  {
    "name": "United Kingdom",
    "series": [
      {
        "name": "2010",
        "x": "2009",
        "y": 80.2,
        "r": 62.7
      },
      {
        "name": "2000",
        "x": "1999",
        "y": 77.8,
        "r": 58.9
      },
      {
        "name": "1990",
        "x": "1989",
        "y": 75.7,
        "r": 57.1
      }
    ]
  }];
  @Input() data : BubbleChartData[] = this.data ? this.data : this.single;  
  @Input() chartInfo: any;

  bubbleChartInfo : BubbleChart;

  constructor() { }

  ngOnInit() {
  	this.chartInfo = this.chartInfo === undefined ? {} : this.chartInfo;
    this.bubbleChartInfo = new BubbleChart(this.chartInfo);
  }
  onSelect(event) {
    
  }

}
