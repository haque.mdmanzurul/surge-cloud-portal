import { Component, OnInit, Input } from '@angular/core';
import { HeatMapChart, MultiChartData } from '../ichart';
@Component({
  selector: 'app-heat-map-chart',
  templateUrl: './heat-map-chart.component.html',
  styleUrls: ['./heat-map-chart.component.scss']
})
export class HeatMapChartComponent implements OnInit {
single: MultiChartData[] = [ // make it empty, testing data
  {
    "name": "Germany",
    "series": [
      {
        "name": "2010",
        "value": 7300000
      },
      {
        "name": "2011",
        "value": 8940000
      }
    ]
  },

  {
    "name": "USA",
    "series": [
      {
        "name": "2010",
        "value": 7870000
      },
      {
        "name": "2011",
        "value": 8270000
      }
    ]
  },

  {
    "name": "France",
    "series": [
      {
        "name": "2010",
        "value": 5000002
      },
      {
        "name": "2011",
        "value": 5800000
      }
    ]
  }
];
  @Input() data : MultiChartData[] = this.data ? this.data : this.single;  
  @Input() chartInfo: any;

  heatMapChartInfo : HeatMapChart;


  constructor() { }

  ngOnInit() {
    this.chartInfo = this.chartInfo === undefined ? {} : this.chartInfo;
    this.heatMapChartInfo = new HeatMapChart(this.chartInfo);
    
  }

  onSelect(event) {
    
  }

}
