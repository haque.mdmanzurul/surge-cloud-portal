import { Component, OnInit, Input } from '@angular/core';
import { SingleChartData, MultiChartData, BarChart } from '../ichart';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {
  single: SingleChartData[] = [ // make it empty, testing data
    {
      "name": "Germany",
      "value": 8940000
    },
    {
      "name": "USA",
      "value": 5000000
    },
    {
      "name": "France",
      "value": 7200000
    }
  ];
  @Input() data : SingleChartData[] = this.data ? this.data : this.single;  
  @Input() chartInfo: any;

  barChartInfo : BarChart;


  constructor() { }

  ngOnInit() {
    this.chartInfo = this.chartInfo === undefined ? {} : this.chartInfo;
    this.barChartInfo = new BarChart(this.chartInfo);
    
  }
  onSelect(event) {
    
  }

}
