import { Component, OnInit, Input } from '@angular/core';
import { SingleChartData, MultiChartData, LineChart } from '../ichart';
@Component({
	selector: 'app-line-chart',
	templateUrl: './line-chart.component.html',
	styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {

	@Input() result: MultiChartData[];

	@Input() chartInfo: any;

	lineChartInfo: LineChart;
	constructor() {

	}

	ngOnInit() {
		// console.log(this.chartInfo);
		this.chartInfo = this.chartInfo === undefined ? {} : this.chartInfo;
		this.lineChartInfo = new LineChart(this.chartInfo);
		// console.log(this.lineChartInfo);
	}

	onSelect($event) {

	}

}
