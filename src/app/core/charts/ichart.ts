export interface SingleChartData {
	name: string,
	value: number
}

export interface MultiChartData {
	name: string,
	series: SingleChartData[]
}

export interface BubbleChartData {
	name: string,
	series: BubbleChartSeries[]
}
export interface BubbleChartSeries {
	name: string,
	x: string,
	y: number,
	r: number
}

export interface ILineChart {
	width: number;
	height: number;
	// options
	showXAxis: boolean;
	showYAxis: boolean;
	gradient: boolean;
	showXAxisLabel: boolean;
	xAxisLabel: string;
	showYAxisLabel: boolean;
	yAxisLabel: string;
	showLegend: boolean;
	colorScheme: any;
	// line area
	autoScale: boolean;
}

export class LineChart implements ILineChart {
	public width: number;
	public height: number;
	// options
	public showXAxis: boolean;
	public showYAxis: boolean;
	public gradient: boolean;
	public showXAxisLabel: boolean;
	public xAxisLabel: string;
	public showYAxisLabel: boolean;
	public yAxisLabel: string;
	public showLegend: boolean;
	public colorScheme: IColorScheme;
	public autoScale: boolean;

	constructor(public data: any) {
		this.width = this.data.width || 100;
		this.height = this.data.height || 300;
		this.showXAxis = this.data.showXAxis === undefined ? true : this.data.showXAxis;
		this.showYAxis = this.data.showYAxis === undefined ? true : this.data.showYAxis;
		this.gradient = this.data.gradient === undefined ? true : this.data.gradient;
		this.showXAxisLabel = this.data.showXAxisLabel === undefined ? true : this.data.showXAxisLabel;
		this.xAxisLabel = this.data.xAxisLabel || 'x';
		this.showYAxisLabel = this.data.showYAxisLabel === undefined ? true : this.data.showYAxisLabel;
		this.yAxisLabel = this.data.yAxisLabel || 'y';
		this.showLegend = this.data.showLegend === undefined ? true : this.data.showLegend;
		this.colorScheme = {
			domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
		};
		this.autoScale = this.data.autoScale === undefined ? true : this.data.autoScale;
	}
}


export interface IColorScheme {
	domain: string[]
}

export interface IPieChart {
	view: any[];
	// options
	showLegend: boolean;
	colorScheme: IColorScheme;
	// pie
	showLabels: boolean;
	explodeSlices: boolean;
	doughnut: boolean;
	valueAppend : string
}

export class PieChart implements IPieChart {
	public view: any[];
	// options
	public showLegend: boolean;
	public colorScheme: IColorScheme;
	// pie
	public showLabels: boolean;
	public explodeSlices: boolean;
	public doughnut: boolean;
	public valueAppend : string
	constructor(public data: any={}) {
		this.view = this.data.view || [230, 230];
		this.showLegend = this.data.showLegend == undefined ? true : this.data.showLegend;
		this.colorScheme = this.data.colorScheme || { domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA','#5DA5DA','#FAA43A','#F17CB0','#B2912F','#B276B2','#DECF3F'] };
		this.showLabels = this.data.showLabels === undefined ? false : this.data.showLabels;
		this.explodeSlices = this.data.explodeSlices === undefined ? false : this.data.explodeSlices;
		this.doughnut = this.data.doughnut === undefined ? false : this.data.doughnut;
		this.valueAppend = this.data.valueAppend || ' ';
	}
}

export interface IBarChart {
	width: number;
	height: number;
	// options
	showXAxis: boolean;
	showYAxis: boolean;
	gradient: boolean;
	showLegend: boolean;
	showXAxisLabel: boolean;
	xAxisLabel: string;
	showYAxisLabel: boolean;
	yAxisLabel: string;
	colorScheme: IColorScheme
}

export class BarChart implements IBarChart {
	public width: number;
	public height: number;
	// options
	public showXAxis: boolean;
	public showYAxis: boolean;
	public gradient: boolean;
	public showLegend: boolean;
	public showXAxisLabel: boolean;
	public xAxisLabel: string;
	public showYAxisLabel: boolean;
	public yAxisLabel: string;
	public colorScheme: IColorScheme
	constructor(public data: any) {
		this.width = this.data.width || 100;
		this.height = this.data.height || 300;
		this.showXAxis = this.data.showXAxis === undefined ? true : this.data.showXAxis;
		this.showYAxis = this.data.showYAxis === undefined ? true : this.data.showYAxis;
		this.gradient = this.data.gradient === undefined ? false : this.data.gradient;
		this.showXAxisLabel = this.data.showXAxisLabel === undefined ? true : this.data.showXAxisLabel;
		this.xAxisLabel = this.data.xAxisLabel || 'x';
		this.showYAxisLabel = this.data.showYAxisLabel === undefined ? true : this.data.showYAxisLabel;
		this.yAxisLabel = this.data.yAxisLabel || 'y';
		this.showLegend = this.data.showLegend === undefined ? true : this.data.showLegend;
		this.colorScheme = {
			domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
		};
	}
}

export interface IBubbleChart {
	width: number;
	height: number;
	// options
	showXAxis: boolean;
	showYAxis: boolean;
	showLegend: boolean;
	showXAxisLabel: boolean;
	xAxisLabel: string;
	showYAxisLabel: boolean;
	yAxisLabel: string;
	colorScheme: IColorScheme;
	autoScale: boolean;
}

export class BubbleChart implements IBubbleChart {
	public width: number;
	public height: number;
	// options
	public showXAxis: boolean;
	public showYAxis: boolean;
	
	public showLegend: boolean;
	public showXAxisLabel: boolean;
	public xAxisLabel: string;
	public showYAxisLabel: boolean;
	public yAxisLabel: string;
	public colorScheme: IColorScheme;
	public autoScale : boolean;
	constructor(public data: any) {
		this.width = this.data.width || 100;
		this.height = this.data.height || 300;
		this.showXAxis = this.data.showXAxis === undefined ? true : this.data.showXAxis;
		this.showYAxis = this.data.showYAxis === undefined ? true : this.data.showYAxis;
		this.showXAxisLabel = this.data.showXAxisLabel === undefined ? true : this.data.showXAxisLabel;
		this.xAxisLabel = this.data.xAxisLabel || 'x';
		this.showYAxisLabel = this.data.showYAxisLabel === undefined ? true : this.data.showYAxisLabel;
		this.yAxisLabel = this.data.yAxisLabel || 'y';
		this.showLegend = this.data.showLegend === undefined ? true : this.data.showLegend;
		this.colorScheme = {
			domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
		};
		this.autoScale = this.data.autoScale === undefined ? true : this.data.autoScale;
	}
}

export interface IHeatMapChart {
	width: number;
	height: number;
	// options
	showXAxis: boolean;
	showYAxis: boolean;
	gradient: boolean;
	showLegend: boolean;
	showXAxisLabel: boolean;
	xAxisLabel: string;
	showYAxisLabel: boolean;
	yAxisLabel: string;
	colorScheme: IColorScheme
}

export class HeatMapChart implements IHeatMapChart {
	public width: number;
	public height: number;
	// options
	public showXAxis: boolean;
	public showYAxis: boolean;
	public gradient: boolean;
	public showLegend: boolean;
	public showXAxisLabel: boolean;
	public xAxisLabel: string;
	public showYAxisLabel: boolean;
	public yAxisLabel: string;
	public colorScheme: IColorScheme
	constructor(public data: any) {
		this.width = this.data.width || 100;
		this.height = this.data.height || 300;
		this.showXAxis = this.data.showXAxis === undefined ? true : this.data.showXAxis;
		this.showYAxis = this.data.showYAxis === undefined ? true : this.data.showYAxis;
		this.gradient = this.data.gradient === undefined ? false : this.data.gradient;
		this.showXAxisLabel = this.data.showXAxisLabel === undefined ? true : this.data.showXAxisLabel;
		this.xAxisLabel = this.data.xAxisLabel || 'x';
		this.showYAxisLabel = this.data.showYAxisLabel === undefined ? true : this.data.showYAxisLabel;
		this.yAxisLabel = this.data.yAxisLabel || 'y';
		this.showLegend = this.data.showLegend === undefined ? true : this.data.showLegend;
		this.colorScheme = {
			domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
		};
	}
}