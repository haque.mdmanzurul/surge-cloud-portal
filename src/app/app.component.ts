import { Component } from '@angular/core';
import { AppStartService } from './app-start.service';
// import { AdalService } from 'ng2-adal/services/adal.service';
// import { OAuthData } from 'ng2-adal/services/oauthdata.model';
import { environment } from '../environments/environment';
import { Router } from "@angular/router";
import { AdalService } from './shared-module';
import { AppService } from './app.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public userInfo: any;
  public options = {
    position: ["bottom", "right"],
    timeOut: 5000,
    lastOnBottom: true
  };
  constructor(public appStartService: AppStartService, private adalService: AdalService, public appService: AppService) {

  }

}
