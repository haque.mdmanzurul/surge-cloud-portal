import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AdalService } from '../../shared-module';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(public adalService: AdalService, private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.adalService.handleCallback();

    // if (this.adalService.userInfo) {
    //   let returnUrl = next.queryParams['returnUrl'];
    //   if (!returnUrl) {
    //     this.router.navigate(['/admin/feature']);
    //   } else {
    //     this.router.navigate([returnUrl], { queryParams: next.queryParams });
    //   }
    // }
    // else {
    //   // this.router.navigate(['login']);
    //   return true;
    // }
    return true;
  }
}
