import { Component, OnInit } from '@angular/core';
import { AdalService } from '../../shared-module';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public adalService: AdalService, private router: Router) { }

  ngOnInit() {
    // this.adalService.handleCallback();

    if (!this.adalService.userInfo) {
      this.router.navigate(['login']);
    } else {
      console.log(this.adalService.userInfo);
      this.router.navigate(['/admin/feature']);
    }
  }

  signIn() {
    this.adalService.login();
  }
}
