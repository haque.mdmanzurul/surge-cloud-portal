import { Component, OnInit } from '@angular/core';
import { AdalService } from '../../shared-module';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../../app.service';

@Component({
  selector: 'app-oauthcallback',
  templateUrl: './oauthcallback.component.html',
  styleUrls: ['./oauthcallback.component.scss']
})
export class OauthcallbackComponent implements OnInit {

  constructor(private adalService: AdalService,public appService:AppService,
    private router: Router, public route:ActivatedRoute) { }

  ngOnInit() {
    if (!this.adalService.userInfo) {
      this.router.navigate(['login']);
    } else {
      this.router.navigate(['/admin/feature']);
    }
  }

}
