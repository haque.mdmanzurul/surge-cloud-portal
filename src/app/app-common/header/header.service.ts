import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../../shared-module';


@Injectable()
export class HeaderService {

  constructor(private httpsService: HttpService) { }

  getcompanies(): Observable<any> {
    return this.httpsService.get('company/api/company/get')
      .map(res => res.json())
      .catch(err => Observable.throw(err));
  }

}
