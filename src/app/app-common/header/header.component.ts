import { Component, OnInit } from '@angular/core';
import { HeaderService } from './header.service';
import { Company, ICompany } from '../../app';
import { AppService } from '../../app.service';
import { AdalService } from '../../shared-module';
import * as _ from 'underscore';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public companies: ICompany[] = [];
  public currentCompany: ICompany;
  public user: any;
  public Icon: any = {
    alarm: {
      normal: 'assets/icons/TopBar/Alarm01.svg#Vector_Smart_Object',
      hover: 'assets/icons/TopBar/Alarm02.svg#Vector_Smart_Object'
    },
    message: {
      normal: 'assets/icons/TopBar/Message01.svg#Vector_Smart_Object',
      hover: 'assets/icons/TopBar/Message02.svg#Vector_Smart_Object'
    },
    search: {
      normal: 'assets/icons/TopBar/Search02.svg#Search_Icon',
      hover: 'assets/icons/TopBar/Search01.svg#Search_Icon'
    },
    help: {
      normal: 'assets/icons/TopBar/Help01.svg#Vector_Smart_Object',
      hover: 'assets/icons/TopBar/Help02.svg#Vector_Smart_Object'
    }
  };
  constructor(private headerService: HeaderService, private appService: AppService, public adalService: AdalService) { }

  ngOnInit() {
    this.companies = [];
    this.appService.companies.forEach(company => {
      if (company.status === 1 || company.status === true) {
        this.companies.push(company);
      }
    });
    this.currentCompany = this.companies[0];
    this.user = this.adalService.userInfo;
    this.appService.companyList.subscribe(companies => {
      this.companies = [];
      companies.forEach(company => {
        if (company.status === 1 || company.status === true) {
          this.companies.push(company);
        }
        });
        // debugger;
        if(this.currentCompany){
          let index = _.findIndex(this.companies,{companyId : this.currentCompany.companyId});
          // console.log(index);
          if(index === -1){
            this.setActiveCompany(this.companies[0]);
          }
        }
      
    });
  }

  getcompanies() {
    this.headerService.getcompanies().subscribe(
      (companies) => {
        companies.forEach(company => {
          this.companies.push(new Company(company));
        });
        this.currentCompany = this.companies[0];
      }
    );
  }

  setActiveCompany(company: Company) {
    this.currentCompany = company;
    this.appService.activeCompany = company;
  }

  logout() {
    this.adalService.logout();
  }

}
