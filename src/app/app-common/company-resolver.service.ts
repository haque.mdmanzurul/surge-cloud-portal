import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../shared-module';
import { AppService } from '../app.service';

@Injectable()
export class CompanyResolverService {

  constructor(private httpsService: HttpService, public appService: AppService) { }
  resolve() {
    return this.httpsService.get(`users/api/users/getuser/${this.appService.getUserId()}`)
      .map((res) => res.json())
      .flatMap(
      (data) => {
        return Observable.forkJoin(
          this.httpsService.get(`company/api/company/get?name=${data.CompanyName}`)
            .map(response => response.json())
        )
      });
  }
}
