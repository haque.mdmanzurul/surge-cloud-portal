import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AppService } from '../app.service';

@Injectable()
export class AccessGuard implements CanActivate {

  constructor(public appService: AppService) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.appService.isAccessible(state.url) || next.params.assetId) {
      return true;
    } else {
      this.appService.showErrorMessage(
        'Permission Denied',
        'You do not have access to this features in the portal. Please contact the Administrator to enable feature for you'
      )
      return false;
    }
  }

}
