import { Component, OnInit } from '@angular/core';
import { AppStartService } from '../../';
import * as _ from 'underscore';
import { INavigation } from '../../app';
import { AppService } from '../../app.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public isOpened = false;
  private sidebar: INavigation[] = [];
  private iconAvailable: string[] = [
    "icon_CustomerOnboarding.svg#CustomerOnboarding",
    "icon_AssetDeviceOnboarding.svg#AssetDeviceOnboarding",
    "icon_TenantManagement.svg#TenantManagement",
    "icon_SubscriptionManagement.svg#SubscriptionManagement",
    "icon_DeviceCloudManagement.svg#DeviceCloudManagement"
  ];
  constructor(public appStartService: AppStartService, private appService: AppService,
    private route: ActivatedRoute) {
    this.appService.getPermissions();
    this.appService.companies = this.route.snapshot.data['company'][0];
    this.appService.activeCompany = this.appService.companies[0];
  }

  ngOnInit() {
    this.appService.feature.subscribe(
      () => {
        let availableSidebar = _.sortBy(this.appStartService.sidebar, 'sortOrder');
        availableSidebar.forEach((side, key) => {
          side.imageIcon = `${side.imageIcon}${this.iconAvailable[key]}`;
        });
        this.sidebar = this.appService.getAccessibleSideNavigation(availableSidebar);
      }
    );
  }

  toggleMenu() {
    this.isOpened = !this.isOpened;
  }
}
