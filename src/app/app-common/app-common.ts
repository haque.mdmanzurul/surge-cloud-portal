export class Facility {
    constructor(
        public logo: string,
        public facilityName: string,
        public status: boolean,
        public address: any,
        public timezone: string,
        public facilityType: string,
        public facilityId?: string
    ) {
        this.logo = logo;
        this.facilityName = facilityName;
        this.status = status;
        this.timezone = timezone;
        this.facilityType = facilityType;
        this.address = new Address(address || {});
        this.facilityId = facilityId;
    }
}

export class Address {
    public name: string;
    public street1: string;
    public street2: string;
    public city: string;
    public stateCode?: string;
    public postcode: string;
    public countryCode: string;
    public latitude: number;
    public longitude: number;
    constructor(private address: any = {}) {
        this.name = address.name || '';
        this.street1 = address.street1 || '';
        this.street2 = address.street2 || '';
        this.city = address.city || '';
        this.stateCode = address.stateCode || '';
        this.postcode = address.postCode || '';
        this.countryCode = address.countryCode || '';
        this.latitude = address.latitude || 0;
        this.longitude = address.longitude || 0;
    }
}


export class Asset {
    constructor(
        public aId: string,
        public assetId: string,
        public companyId: string,
        public companyName: string,
        public assetType: string,
        public logo: string,
        public make: string,
        public model: string,
        public serialNumber: string,
        public unitNumber: string,
        public vin: string,
        public softwareId: string,
        public calibrationId: string,
        public customerReference: string,
        public customerFacilityGroup: string,
        public customerEquipmentGroup: string,
        public customerEquipmentId: string,
        public tags: string[],
        public status: string,
        public lastUpdatedDate: string
    ) {
        this.aId = aId;
        this.assetId = assetId;
        this.companyId = companyId;
        this.companyName = companyName;
        this.assetType = assetType;
        this.logo = logo || 'assets/images/logo.png';
        this.make = make;
        this.model = model;
        this.serialNumber = serialNumber;
        this.unitNumber = unitNumber;
        this.vin = vin;
        this.softwareId = softwareId;
        this.calibrationId = calibrationId;
        this.customerReference = customerReference;
        this.customerFacilityGroup = customerFacilityGroup;
        this.customerEquipmentGroup = customerEquipmentGroup;
        this.customerEquipmentId = customerEquipmentId;
        this.tags = tags;
        this.status = status;
        this.lastUpdatedDate = new Date().toLocaleString();
    }
}


export class GeoFenceAsset {
    public expanded : Boolean = false;
    public icon: string = '';
    constructor(
        public aId: string,
        public altitude: number,
        public assetId: string,
        public assetLastUpdateDate: string,
        public assetLogo: string,
        public assetStatus: string,
        public assetType: string,
        public calibrationId: string,
        public companyId: string,
        public companyName: string,
        public controllerId: string,
        public customerEquipmentGroup: string,
        public customerEquipmentId: string,
        public facilityId: string,
        public facilityName: string,
        public iotHubName: string,
        public latestSyncDate: string,
        public latitude: number,
        public logo: string,
        public longitude: number,
        public make: string,
        public model: string,
        public serialNumber: string,
        public softwareId: string,
        public status: number,
        public unitNumber: string,
        public vin: string,
        public gwAppBuild:string,
        public gwDCAver: string,
        public gwDescription: string,
        public gwIMEI: string,
        public gwOSBuild: string,
        public gwRSSI: string,
        public gwSIMICCID: string,
        public gwSNO: string,
        public gwTimezone:string,
        public gwWANIP: string,
        public pVer: string,
        public param: any[],
    ) {
        this.aId = aId;
        this.altitude = altitude;
        this.assetId = assetId;
        this.assetLastUpdateDate = assetLastUpdateDate;
        this.assetLogo =assetLogo;
        this.assetStatus = assetStatus;
        this.assetType = assetType;
        this.calibrationId = calibrationId;
        this.companyId = companyId;
        this.companyName = companyName;
        this.controllerId = controllerId;
        this.customerEquipmentGroup = customerEquipmentGroup;
        this.customerEquipmentId = customerEquipmentId;
        this.facilityId = facilityId;
        this.facilityName = facilityName;
        this.iotHubName = iotHubName;
        this.latestSyncDate = latestSyncDate;
        this.latitude = latitude || 0;
        this.logo = logo || 'assets/images/logo.png';
        this.longitude = longitude || 0;
        this.make = make;
        this.model = model;
        this.serialNumber = serialNumber;
        this.softwareId = softwareId;
        this.status = status;
        this.unitNumber = unitNumber;
        this.vin = vin;
        this.gwAppBuild = gwAppBuild;
        this.gwDCAver = gwDCAver;
        this.gwDescription = gwDescription;
        this.gwIMEI = gwIMEI;
        this.gwOSBuild = gwOSBuild;
        this.gwRSSI = gwRSSI;
        this.gwSIMICCID = gwSIMICCID;
        this.gwSNO = gwSNO;
        this.gwTimezone = gwTimezone;
        this.gwWANIP = gwWANIP;
        this.pVer = pVer;
        this.param = param;
    }
}

// export class Address {
//     public name: string;
//     public street1: string;
//     public street2: string;
//     public city: string;
//     public stateCode?: string;
//     public postcode: string;
//     public countryCode: string;
//     public latitude: number;
//     public longitude: number;
//     constructor(private address: any = {}) {
//         this.name = address.name || '';
//         this.street1 = address.street1 || '';
//         this.street2 = address.street2 || '';
//         this.city = address.city || '';
//         this.stateCode = address.stateCode || '';
//         this.postcode = address.postCode || '';
//         this.countryCode = address.countryCode || '';
//         this.latitude = address.latitude || 0;
//         this.longitude = address.longitude || 0;
//     }
// }

export class Contact {
    public contactName: string;
    public contactPhoneNumber: string;
    public contactMobileNumber: string;
    public contactEmailId: string;
    public contactStatus: number;
    constructor(private contact: any = {}) {
        this.contactName = contact.contactName || '';
        this.contactPhoneNumber = contact.contactPhoneNumber || '';
        this.contactMobileNumber = contact.contactMobileNumber || '';
        this.contactEmailId = contact.contactEmailId || '';
        this.contactStatus = contact.stateCode || 0;
    }
}

export class Organization {
    public companyId: string;
    public name: string;
    public parentCompany: string;
    public address: IAddress;
    public contact: Contact;
    public emailId: string;
    public website: string;
    public phones: string[];
    public currencyCode: string;
    public createdBy: string;
    public updatedBy: string;
    public lastUpdatedDate: string;
    public status: Boolean;
    public logo: string;
    public tags: string[];
    public groupId: string;
    constructor(private organization: any = {}) {
        this.companyId = organization.companyId || null;
        this.name = organization.name || '';
        this.parentCompany = organization.parentCompany || '';
        this.address = new Address(organization.address);
        this.contact = new Contact(organization.contact);
        this.emailId = organization.emailId || '';
        this.website = organization.website || '';
        this.phones = organization.phones || [''];
        this.currencyCode = organization.currencyCode || '';
        this.createdBy = organization.createdBy || '';
        this.updatedBy = organization.updatedBy || '';
        this.lastUpdatedDate = organization.lastUpdatedDate || '';
        this.status = organization.status || true;
        this.logo = organization.logo || '';
        this.tags = organization.tags || [];
        this.groupId = organization.groupId || '';
    }
}

export interface IAddress {
    name: string;
    street1: string;
    street2?: string;
    city: string;
    stateCode?: string;
    countryCode: string;
    postcode: string;
    latitude?: number;
    longitude?: number;
}

export class Currency {
    public code: string;
    public name: string;
    public symbol: string
    constructor(public data: any) {
        this.code = data.code;
        this.name = data.name;
        this.symbol = data.symbol;
    }
}

export class RoleGroup {
    public description: string;
    public displayName: string;
    public id: string;
    public status: number;
    constructor(
        description: string,
        displayName: string,
        id: string,
        status: number
    ) {
        this.description = description;
        this.displayName = displayName;
        this.id = id;
        this.status = status;
    }
}

export class Users {
    public Id: string;
    public emailId: string;
    public password: string;
    public displayName: string;
    public givenName: string;
    public surname: string;
    public jobTitle: string;
    public companyId: string;
    public companyName: string;
    public facilityId: string;
    public facilityName: string;
    public drivingLicenseNumber: string;
    public driverId: string;
    public expirationDate: any;
    public landingPage: string;
    public logo: string;
    public status: Boolean = true;
    public roleGroup: string;
    public phoneNumber: string;
    constructor(public data: any = {}) {
        this.Id = data.UserId || null;
        this.emailId = data.EmailId || '';
        this.password = data.Password || '';
        this.displayName = data.DisplayName || '';
        this.givenName = data.GivenName || '';
        this.surname = data.Surname || '';
        this.jobTitle = data.JobTitle || '';
        this.companyId = data.CompanyId || null;
        this.companyName = data.CompanyName || '';
        this.facilityId = data.FacilityId || null;
        this.facilityName = data.FacilityName || '';
        this.drivingLicenseNumber = data.DrivingLicenceNumber || '';
        this.driverId = data.DriverId || null;
        this.expirationDate = data.ExpirationDate || '';
        this.landingPage = data.LandingPage || '';
        this.logo = data.logo || '';
        this.status = data.status || true;
        this.roleGroup = data.roleGroup || '';
        this.phoneNumber = data.PhoneNumber || '';
    }
}

/*export enum Images {
    ORGANIZATION = 'assets/images/org_default.svg',
    USER = 'assets/images/logo.png'
}
*/
