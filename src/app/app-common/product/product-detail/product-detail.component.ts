import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  @Input() product: any = {
    "id": 1,
    "name": "Apna Bazar Pure Cow Ghee",
    "image": "http://www.surgecloud.com/images/T/Apna%20Bazar%20Pure%20Cow%20ghee%20%28Copy%29.JPG",
    "new": false,
    "price": "50",
    "discount": "5 %",
    "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis."
  };

  constructor() { }

  ngOnInit() {
  }

}
