import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { MaterialModule } from '@angular/material';
import { SharedModule } from '../shared-module';
import { CoreModule } from '../core/core.module';
import { AppCommonRoutingModule } from './app-common-routing.module';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SidenavComponent } from '../core/sidenav/sidenav.component';
import { HeaderService } from './header/header.service';
import { CompanyResolverService } from './company-resolver.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent, LoginGuard } from './login';
import { OauthcallbackComponent, OauthGuard } from './oauthcallback';
import { AccessGuard } from './access.guard';

@NgModule({
  imports: [
    CommonModule,
    AppCommonRoutingModule,
    // MaterialModule,
    SharedModule,
    CoreModule,
    NgbModule
  ],
  providers: [
    HeaderService,
    CompanyResolverService,
    LoginGuard,
    OauthGuard,
    AccessGuard
  ],
  declarations: [HomeComponent, HeaderComponent, FooterComponent, LoginComponent, OauthcallbackComponent],
  exports: [HomeComponent, HeaderComponent, FooterComponent]
})
export class AppCommonModule { }
